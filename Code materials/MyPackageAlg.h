#ifndef  MYPACKAGE_MYPACKAGEALG_H
#define  MYPACKAGE_MYPACKAGEALG_H

#include "GaudiKernel/ToolHandle.h"

#include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"

// #include "TTree.h"
#include "TROOT.h"
#include "TChain.h"
#include "TFile.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TMVA/Reader.h"

#include "AsgTools/AnaToolHandle.h"

#include "AsgAnalysisInterfaces/IGoodRunsListSelectionTool.h"
#include "AsgAnalysisInterfaces/IPileupReweightingTool.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
#include "MuonAnalysisInterfaces/IMuonSelectionTool.h"
#include "JetInterface/IJetSelector.h"
#include "JetResolution/IJERTool.h"

#include "xAODJet/JetContainer.h"

#include "xAODTruth/TruthEvent.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODTruth/TruthEventAuxContainer.h"

class MyPackageAlg: public ::AthAnalysisAlgorithm
{ 
public: 
   MyPackageAlg( const std::string& name, ISvcLocator* pSvcLocator );
   virtual ~MyPackageAlg(); 

   ///uncomment and implement methods as required

   virtual StatusCode  initialize();     //once, before any input is loaded
   virtual StatusCode  beginInputFile(); //start of each input file, only metadata loaded
   //virtual StatusCode  firstExecute();   //once, after first eventdata is loaded (not per file)
   virtual StatusCode  execute();        //per event
   //virtual StatusCode  endInputFile();   //end of each input file
   //virtual StatusCode  metaDataStop();   //when outputMetaStore is populated by MetaDataTools
   virtual StatusCode  finalize();       //once, after all events processed
  
   ///Other useful methods provided by base class are:
   ///evtStore()        : ServiceHandle to main event data storegate
   ///inputMetaStore()  : ServiceHandle to input metadata storegate
   ///outputMetaStore() : ServiceHandle to output metadata storegate
   ///histSvc()         : ServiceHandle to output ROOT service (writing TObjects)
   ///currentFile()     : TFile* to the currently open input file
   ///retrieveMetadata(...): See twiki.cern.ch/twiki/bin/view/AtlasProtected/AthAnalysisBase#ReadingMetaDataInCpp
   
   // -------------------------------
   // utils
   // -------------------------------

   StatusCode getTruthChargino(const xAOD::TruthEventContainer*,
			       std::vector<TVector3>&, std::vector<TVector3>&, std::vector<TVector3>&);
   bool  is_target_chg(float, const TVector3&);

   void  calc_dr_min(const TVector3&, const std::vector<TVector3>&, float&, int&); 

   float trk_z0_wrt_pvtx(float, float);

   bool is_seed_allpix(const xAOD::TrigComposite*);
   bool is_seed_allpix_allbarrel(const xAOD::TrigComposite*);
   int  get_seed_npix(const xAOD::TrigComposite*);

   TVector3 get_track_p3(const xAOD::TrigComposite*);
   void get_track(const xAOD::TrigComposite*, TVector3&, float&, float&);
   void get_track(const xAOD::TrigComposite*, TVector3&, float&, float&, float&, float&, int&, int&, int&);

   void calc_jet_isolation(const TVector3&,const xAOD::JetContainer*,float&, float&, float&);
   void calc_isolation(const TVector3&,const std::vector<TVector3>&,float&, float&, float&);

   // -------------------------------
   // BDT
   // -------------------------------

   bool  is_bdt_passed(int, const std::string&, const xAOD::TrigComposite*, float);
   float bdt_eval_pix4l_sct0( float, float, float, float, float, float, float, float, int, float, float, int);
   float bdt_eval_pix4l_sct1p(float, float, float, float, int, float, float, float, float, int, float, int);
   float bdt_eval_pix3l_sct0( float, float, float, float, float, float, float, float, float, float, int, int);
   float bdt_eval_pix3l_sct1p(float, float, float, float, float, float, int, int, float, int, int, float, float, float, float);

   // -------------------------------
   // disFitTrk utils
   // -------------------------------

   float get_fittrk_n_hist(int); 

   void get_fittrk_brhits(const std::string&, const xAOD::TrigComposite*, std::vector<int>&, std::vector<float>&, std::vector<int>&, std::vector<int>&);
   void get_fittrk_vtx(const std::string&, const xAOD::TrigComposite*, float&, float&, std::vector<float>&);
   bool presel_fittrk(const std::string&, const xAOD::TrigComposite*);
   void get_fittrk(const std::string&, const xAOD::TrigComposite*,std::map<std::string, float>&, std::map<std::string, int>&);

   void print_fittrk(const std::string&, std::map<std::string, float>&, std::map<std::string, int>&);
   bool is_fittrk_good_for_isolation(const std::string&, const xAOD::TrigComposite*);
   void fittrk_calc_isolation(const std::string&, const xAOD::TrigComposite*,
			      std::map<unsigned int, std::map<std::string, float> >&,
			      std::map<std::string, float>&);
   
   int  get_fittrk_category(const std::string&, const xAOD::TrigComposite*);

   void get_fittrk_p3(const std::string&, const xAOD::TrigComposite*, TVector3&);
   void get_fittrk_trk(const std::string&, const xAOD::TrigComposite*, TVector3&, float&, float&, float&, float&, float&, int&, int&, int&);
   void get_fittrk_vtx(const std::string&, const xAOD::TrigComposite*, float&, float&, float&, float&, std::vector<float>&);

   bool is_fittrk_match_to_chg(const std::string&, const xAOD::TrigComposite*, const std::vector<TVector3>&, float&, int&);

   // -------------------------------
   // process functions
   // -------------------------------
   StatusCode do_Triplet(const Trig::FeatureContainer&, const std::vector<TVector3>&);
   void remove_duplicates(std::map<unsigned int, std::map<std::string, float> >&,
			  std::map<unsigned int, std::map<std::string, int> >&);

   // -------------------------------
   // hist books
   // -------------------------------
   StatusCode book_h1_each(const std::string&, int, double, double);
   StatusCode book_chargino_info_base();
   StatusCode book_trigger_info_base();
   StatusCode book_vertex_info();
   StatusCode book_n_triplets_info();
   StatusCode book_isol_calc_info();
   StatusCode book_fittrk_info_base(const std::string&);
   StatusCode book_fittrk_n_info(const std::string&);
   StatusCode book_chargino_vs_fittrk_info(const std::string&);
   StatusCode book_dr();
   // -------------------------------
   // hist fills
   // -------------------------------
   StatusCode fill_h1_each(const std::string&, double);
   void  fill_chargino_info_base(const std::vector<TVector3>&,const std::vector<TVector3>&,const xAOD::JetContainer*,float);
   void  fill_fittrk_vertex_info(const xAOD::TrigComposite*,const std::vector<TVector3>&);
   void  fill_n_triplets_info(int, int, int, int, int, int);
   float fill_fittrk_info_base(const std::string&, const std::string&, const xAOD::TrigComposite*, float, std::map<std::string, float>&, std::map<std::string, int>&, std::map<unsigned int, std::map<std::string, float> >&, bool, std::ofstream&);
   void  fill_fittrk_n_info(const std::string&, std::map<std::string,int>&);
   void  fill_vertex_info(const std::vector<TVector3>&, const std::vector<TVector3>&);
   void  fill_chargino_vs_fittrk_info(const std::string&, const std::vector<TVector3>&, const std::vector<TVector3>&, float&, std::map<std::string, int>&);

   // -------------------------------
   // hist max utils and bin defintions
   // -------------------------------
   float hist_chi2ndof(float);
   float hist_chi2(float);
   float hist_pt(float);
   float hist_refit_ratio(float);

   const float HIST_PT_MAX          = 200;
   const float HIST_CHI2_MAX        = 20;
   const float HIST_CHI2NDOF_MAX    = 10;
   const float HIST_REFIT_RATIO_MAX = 10;
   const float HIST_N_FITTRK_MAX    = 20;

   // -------------------------------
   // cuts and switched
   // -------------------------------
   const float PT_CUT_VALUE           = 20.;
   const float Z0_CUT_VALUE           = 4.;
   // const float DR_TO_CHG_CUT_VALUE    = 0.03;
   const float DR_TO_CHG_CUT_VALUE    = 0.1;
   //
   const float CHG_PT_CUT_VALUE       = 20.;
   const float CHG_ETA_CUT_VALUE      = 1.8;
   const float CHG_DECAY_R_MIN_VALUE  = 130;
   const float CHG_DECAY_R_MAX_VALUE  = 300;
   //
   const float TRK_Z0_NO_PVTX         = -199.5;

   // switch between trigger presel or not
   const bool is_trigger_presel       = false;
   const char* presel_trigger         = "L1_XE35";

private: 
   int m_numCleanEvents = 0;
   
   // GRL Selection Tool
   asg::AnaToolHandle<IGoodRunsListSelectionTool> m_grlTool;

   // Pileup reweight Tool
   asg::AnaToolHandle<CP::IPileupReweightingTool> m_pileupTool;

   // TrigDecisionTool
   asg::AnaToolHandle<Trig::TrigDecisionTool> m_tdt;

   // MuonSelectionTool
   asg::AnaToolHandle<CP::IMuonSelectionTool> m_muonSelection;

   // Jet cleaning
   asg::AnaToolHandle<IJetSelector> m_jetCleaning;

   // Jet energy resolution
   asg::AnaToolHandle<IJERTool> m_JERTool;

   // ascii output files for MVA
   std::ofstream m_ofs_signal;
   std::ofstream m_ofs_background;

   // TMVA reader
   TMVA::Reader* m_tmva_pix4l_sct0_reader;
   TMVA::Reader* m_tmva_pix4l_sct1p_reader;
   TMVA::Reader* m_tmva_pix3l_sct0_reader;
   TMVA::Reader* m_tmva_pix3l_sct1p_reader;

   // TMVA variables
   // pix4l_sct0
   float m_tmva_pix4l_sct0_pt;
   float m_tmva_pix4l_sct0_z0;
   float m_tmva_pix4l_sct0_d0;
   float m_tmva_pix4l_sct0_trkiso3_dr01;
   float m_tmva_pix4l_sct0_trkiso3_dr0201;
   float m_tmva_pix4l_sct0_chi2ndof;
   float m_tmva_pix4l_sct0_chi2ndof_pix;
   float m_tmva_pix4l_sct0_refit_pt;
   float m_tmva_pix4l_sct0_n_pix;
   float m_tmva_pix4l_sct0_refit_pt_ratio;
   float m_tmva_pix4l_sct0_refit_chi2ndof;
   float m_tmva_pix4l_sct0_n_bl;

   // pix4l_sct1p
   float m_tmva_pix4l_sct1p_pt;
   //float m_tmva_pix4l_sct1p_z0;
   //float m_tmva_pix4l_sct1p_d0;
   float m_tmva_pix4l_sct1p_refit_pt;
   float m_tmva_pix4l_sct1p_refit_z0;
   float m_tmva_pix4l_sct1p_refit_d0;
   float m_tmva_pix4l_sct1p_n_sct;
   float m_tmva_pix4l_sct1p_refit_pt_ratio;
   float m_tmva_pix4l_sct1p_refit_chi2ndof_ratio;
   float m_tmva_pix4l_sct1p_trkiso3_dr01;
   float m_tmva_pix4l_sct1p_trkiso3_dr0201;
   float m_tmva_pix4l_sct1p_is_fail;
   float m_tmva_pix4l_sct1p_chi2ndof_pix;
   float m_tmva_pix4l_sct1p_n_pix;
   // pix3l_sct0
   float m_tmva_pix3l_sct0_pt;
   float m_tmva_pix3l_sct0_z0;
   float m_tmva_pix3l_sct0_d0;
   float m_tmva_pix3l_sct0_chi2ndof;
   float m_tmva_pix3l_sct0_chi2ndof_pix;
   float m_tmva_pix3l_sct0_trkiso3_dr01;
   float m_tmva_pix3l_sct0_trkiso3_dr0201;
   float m_tmva_pix3l_sct0_refit_pt;
   float m_tmva_pix3l_sct0_refit_z0;
   float m_tmva_pix3l_sct0_refit_d0;
   float m_tmva_pix3l_sct0_n_pix;
   float m_tmva_pix3l_sct0_n_bl;
   // pix3l_sct1p
   float m_tmva_pix3l_sct1p_pt;
   float m_tmva_pix3l_sct1p_z0;
   float m_tmva_pix3l_sct1p_d0;
   float m_tmva_pix3l_sct1p_refit_pt;
   float m_tmva_pix3l_sct1p_refit_z0;
   float m_tmva_pix3l_sct1p_refit_d0;
   float m_tmva_pix3l_sct1p_n_pix;
   float m_tmva_pix3l_sct1p_n_sct;
   float m_tmva_pix3l_sct1p_refit_pt_ratio;
   float m_tmva_pix3l_sct1p_is_fail;
   float m_tmva_pix3l_sct1p_n_bl;
   float m_tmva_pix3l_sct1p_chi2ndof;
   float m_tmva_pix3l_sct1p_refit_chi2ndof;
   float m_tmva_pix3l_sct1p_trkiso3_dr01;
   float m_tmva_pix3l_sct1p_trkiso3_dr0201;

   // control flags
   bool m_is_signal_mc;
   bool m_is_bdt_validation;
   bool m_is_debug_mode;

   // trigger info
   std::vector<std::string> m_v_l1_items;
   std::vector<std::string> m_v_hlt_chains;
   std::map<std::string, bool> m_map_l1_results;
   std::map<std::string, bool> m_map_hlt_results;

   // histograms
   std::map<std::string, TH1D*> m_map_h1;

   TH1D* m_h1_l1_counts      = 0;
   TH1D* m_h1_l1_bdt_counts  = 0;
   TH1D* m_h1_hlt_counts     = 0;
   TH1D* m_h1_hlt_bdt_counts = 0;
}; 

#endif //> !MYPACKAGE_MYPACKAGEALG_H
