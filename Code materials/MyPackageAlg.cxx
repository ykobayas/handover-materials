// MyPackage includes

#include <string>
#include <map>
#include <vector>

#include "TString.h"

#include "MyPackageAlg.h"

#include "xAODEventInfo/EventInfo.h"
#include "xAODEventInfo/versions/EventInfo_v1.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/PhotonContainer.h"

#include "xAODTrigMissingET/TrigMissingETContainer.h"
#include "xAODTrigMuon/L2StandAloneMuonContainer.h"
#include "xAODTrigger/TrigCompositeContainer.h"

// ================================================================================================
// ================================================================================================

MyPackageAlg::MyPackageAlg( const std::string& name, ISvcLocator* pSvcLocator ) :
   AthAnalysisAlgorithm( name, pSvcLocator ),
   m_grlTool ("GoodRunsListSelectionTool/grl", this),
   m_pileupTool ("CP::PileupReweightingTool/tool", this),
   m_muonSelection ("CP::MuonSelectionTool", this),
   m_jetCleaning ("JetCleaningTool/JetCleaning", this), m_JERTool ("JERTool", this),
   m_ofs_signal("signal.ascii"),
   m_ofs_background("background.ascii")
{
   /** signal mc mode */
   declareProperty("IsSignalMC", m_is_signal_mc = true);

   /** validation mode */
   declareProperty("IsBDTValidation", m_is_bdt_validation = true);

   /** debug mode */
   declareProperty("IsDebugMode", m_is_debug_mode = true);
}

MyPackageAlg::~MyPackageAlg() {}

// ================================================================================================
// ================================================================================================

StatusCode MyPackageAlg::book_h1_each(const std::string& name, int nbins, double xmin, double xmax)
{
   const std::string fname = "book_h1_each> ";
   if( m_map_h1.find(name) != m_map_h1.end() ) {
      std::cout << fname << "ERROR: defining hist two times" << std::endl;
      return StatusCode::FAILURE;
   }
   TH1D* h1 = new TH1D(("h1_"+name).c_str(),name.c_str(),nbins,xmin,xmax);
   CHECK( histSvc()->regHist(("/MYSTREAM/h1_"+name).c_str(), h1) );
   m_map_h1[name] = h1;
   return StatusCode::SUCCESS;
}

// ------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------

StatusCode MyPackageAlg::fill_h1_each(const std::string& name, double value)
{
   const std::string fname = "fill_h1_each> ";
   if( m_map_h1.find(name) == m_map_h1.end() ) {
      std::cout << fname << " ERROR no histgram = " << name << std::endl;
      return StatusCode::FAILURE;
   }
   m_map_h1[name]->Fill(value);
   return StatusCode::SUCCESS;
}

// ================================================================================================
// ================================================================================================

StatusCode MyPackageAlg::book_chargino_vs_fittrk_info(const std::string& prefix)
{
   std::string prefix_hist = "chg_vs_" + prefix;
   
   std::vector<std::string> v_names;
   v_names.push_back("_all");
   v_names.push_back("_clean");
   v_names.push_back("_presel");
   v_names.push_back("_bdt");

   for(auto it=v_names.begin(); it!=v_names.end(); it++) {
      book_h1_each(prefix_hist+"_chg_decay_r"+(*it),200,0,400);
      book_h1_each(prefix_hist+"_chg_pt"+(*it),     251,-1,501);
      book_h1_each(prefix_hist+"_chg_eta"+(*it),    160,-4,4);
      book_h1_each(prefix_hist+"_chg_phi"+(*it),    128,-3.2,3.2);
      book_h1_each(prefix_hist+"_pileup"+(*it),100,0,100);
   }

   return StatusCode::SUCCESS;
}

// ------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------

void MyPackageAlg::fill_chargino_vs_fittrk_info(const std::string& prefix, 
						const std::vector<TVector3>& v_chg_p3, const std::vector<TVector3>& v_chg_decay, float& actualMu,
						std::map<std::string, int>& map_chg_n_match_disTrkFit)
{
   std::string prefix_hist = "chg_vs_" + prefix;

   std::vector<std::string> v_names;
   v_names.push_back("_all");
   v_names.push_back("_clean");
   v_names.push_back("_presel");
   v_names.push_back("_bdt");

   for(unsigned int i_chg=0; i_chg<v_chg_p3.size(); i_chg++) {

      TVector3 decay_chg = v_chg_decay[i_chg];
      float chg_decay_r = decay_chg.Perp();
      TVector3 p3_chg = v_chg_p3[i_chg];
      char buf[64];
      sprintf(buf,"chg%i_",i_chg); 
      
      std::vector<int> v_n;
      v_n.push_back(1);
      for(unsigned int j=1; j<v_names.size(); j++) {
	 std::string name = v_names[j];
	 int n = 0;
	 if( name == "_clean" ) { n = map_chg_n_match_disTrkFit[std::string(buf)+prefix+"_n"]; }
	 else {
	    n = map_chg_n_match_disTrkFit[std::string(buf)+prefix+"_n"+name];
	 }
	 v_n.push_back(n);
      }

      for(unsigned int j=0; j<v_names.size(); j++) {
	 std::string name = v_names[j];
	 if( v_n[j] > 0 ) {
	    fill_h1_each(prefix_hist+"_chg_decay_r"+name, chg_decay_r);
	    fill_h1_each(prefix_hist+"_chg_pt"+name,  hist_pt(p3_chg.Pt()));
	    fill_h1_each(prefix_hist+"_chg_eta"+name, p3_chg.Eta());
	    fill_h1_each(prefix_hist+"_chg_phi"+name, p3_chg.Phi());
	    fill_h1_each(prefix_hist+"_pileup"+name, actualMu);
         }
      }
   }
}

// ================================================================================================
// ================================================================================================

StatusCode MyPackageAlg::book_chargino_info_base()
{
   std::string prefix = "chg";
   int nbins;
   double xmin, xmax;
   
   book_h1_each(prefix+"_n_all",10,0,10);
   book_h1_each(prefix+"_n",    10,0,10);
   book_h1_each(prefix+"_decay_r_all",200,0,400);
   book_h1_each(prefix+"_pt_all", 408,-1,101);
   book_h1_each(prefix+"_eta_all",160,-4,4);
   book_h1_each(prefix+"_decay_r",200,0,400);
   book_h1_each(prefix+"_decay_x",200,-200,200);
   book_h1_each(prefix+"_decay_y",200,-200,200);
   book_h1_each(prefix+"_decay_z",300,-900,900);
   // book_h2_each(prefix+"_decay_r_vs_z",300,-900,900,200,0,400);

   book_h1_each(prefix+"_pt", 2040,-1,501);
   book_h1_each(prefix+"_eta",160,-4,4);
   book_h1_each(prefix+"_phi",128,-3.2,3.2);
   book_h1_each(prefix+"_jetptsum_dr02",408,-1,101);
   book_h1_each(prefix+"_jetptsum_dr04",408,-1,101);
   book_h1_each(prefix+"_jetptsum_dr10",408,-1,101);
   book_h1_each(prefix+"_jetptsumfrac_dr02",120,-0.1,1.1);
   book_h1_each(prefix+"_jetptsumfrac_dr04",120,-0.1,1.1);
   book_h1_each(prefix+"_jetptsumfrac_dr10",120,-0.1,1.1);

   //
   nbins=30; xmin=0.0; xmax=600.0;
   book_h1_each(prefix+"_twosyst_pt",nbins,xmin,xmax);
   book_h1_each(prefix+"_twosyst_pt_L1XE35",nbins,xmin,xmax);
   book_h1_each(prefix+"_twosyst_pt_L1XE40",nbins,xmin,xmax);
   book_h1_each(prefix+"_twosyst_pt_L1XE50",nbins,xmin,xmax);
   book_h1_each(prefix+"_twosyst_pt_xe80",nbins,xmin,xmax);
   book_h1_each(prefix+"_twosyst_pt_xe110",nbins,xmin,xmax);
   book_h1_each(prefix+"_twosyst_pt_L1XE35_xe80",nbins,xmin,xmax);
   book_h1_each(prefix+"_twosyst_pt_L1XE35_xe110",nbins,xmin,xmax);
   if( m_is_bdt_validation ) {
      book_h1_each(prefix+"_twosyst_pt_L1XE35bdt",nbins,xmin,xmax);
      book_h1_each(prefix+"_twosyst_pt_L1XE50bdt",nbins,xmin,xmax);
      book_h1_each(prefix+"_twosyst_pt_xe80bdt",nbins,xmin,xmax);
      book_h1_each(prefix+"_twosyst_pt_xe110_or_L1XE35bdt",nbins,xmin,xmax);
      book_h1_each(prefix+"_twosyst_pt_xe110_or_xe80bdt",nbins,xmin,xmax);
      book_h1_each(prefix+"_twosyst_pt_xe110_or_xe60bdt",nbins,xmin,xmax);
      book_h1_each(prefix+"_twosyst_pt_xe110_and_xe80bdt",nbins,xmin,xmax);
      book_h1_each(prefix+"_twosyst_pt_notxe110_and_xe80bdt",nbins,xmin,xmax);
      book_h1_each(prefix+"_twosyst_pt_xe110_and_notxe80bdt",nbins,xmin,xmax);
      book_h1_each(prefix+"_twosyst_pt_xe110_xor_xe80bdt",nbins,xmin,xmax);
   }

   //
   return StatusCode::SUCCESS;
}

// ------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------

void MyPackageAlg::fill_chargino_info_base(const std::vector<TVector3>& v_chg_p3,
					   const std::vector<TVector3>& v_chg_decay,
					   const xAOD::JetContainer* jets,
					   float chg2_pt)
{
   fill_h1_each("chg_n",          v_chg_p3.size());
   fill_h1_each("chg_twosyst_pt", chg2_pt);

   for(unsigned int i_chg=0; i_chg<v_chg_p3.size(); i_chg++) {

      TVector3 decay_chg = v_chg_decay[i_chg];
      float chg_decay_r = decay_chg.Perp();

      fill_h1_each("chg_decay_r", chg_decay_r);
      fill_h1_each("chg_decay_x", decay_chg.X());
      fill_h1_each("chg_decay_y", decay_chg.Y());
      fill_h1_each("chg_decay_z", decay_chg.Z());
      // m_h2_chg_decay_r_vs_z->Fill(decay_chg.Z(),chg_decay_r);

      TVector3 p3_chg = v_chg_p3[i_chg];
      fill_h1_each("chg_pt",  hist_pt(p3_chg.Pt()));
      fill_h1_each("chg_eta", p3_chg.Eta());
      fill_h1_each("chg_phi", p3_chg.Phi());

      // check jet isolation
      float jetptsum_dr02=0; float jetptsum_dr04=0; float jetptsum_dr10=0;
      calc_jet_isolation(p3_chg, jets, jetptsum_dr02, jetptsum_dr04, jetptsum_dr10);
      fill_h1_each("chg_jetptsum_dr02", hist_pt(jetptsum_dr02));
      fill_h1_each("chg_jetptsum_dr04", hist_pt(jetptsum_dr04));
      fill_h1_each("chg_jetptsum_dr10", hist_pt(jetptsum_dr10));

      float pt = p3_chg.Pt();
      float ptfrac_dr02 = jetptsum_dr02 / pt; if(ptfrac_dr02 > 1 ) ptfrac_dr02=1.;
      float ptfrac_dr04 = jetptsum_dr04 / pt; if(ptfrac_dr04 > 1 ) ptfrac_dr04=1.;
      float ptfrac_dr10 = jetptsum_dr10 / pt; if(ptfrac_dr10 > 1 ) ptfrac_dr10=1.;
      fill_h1_each("chg_jetptsumfrac_dr02", ptfrac_dr02);
      fill_h1_each("chg_jetptsumfrac_dr04", ptfrac_dr04);
      fill_h1_each("chg_jetptsumfrac_dr10", ptfrac_dr10);
   }
}

// =================
// =================
StatusCode MyPackageAlg::book_dr()
{   
   book_h1_each("disTrkCand_n",100,0,100);
   book_h1_each("dr_to_chg_presel",401,-0.01,4);
   book_h1_each("dr_match_to_chg_presel",401,-0.01,4);
   return StatusCode::SUCCESS;
}

// ================================================================================================
// ================================================================================================

StatusCode MyPackageAlg::book_isol_calc_info()
{
   book_h1_each("isol_calc_z0_diff",200,-10,10);
   book_h1_each("isol_calc_dr",400,0,0.2);
   return StatusCode::SUCCESS;
}

// ================================================================================================
// ================================================================================================

StatusCode MyPackageAlg::book_vertex_info()
{
   book_h1_each("pvtx_n",10,0,10);
   book_h1_each("pvtx_x",100,-5,5);
   book_h1_each("pvtx_y",100,-5,5);
   book_h1_each("pvtx_z",400,-200,200);

   book_h1_each("pvtx_z_wrt_truth",400,-100,100);
   book_h1_each("disTrkVtx_z_wrt_truth",400,-100,100);
   book_h1_each("zfinderVtx_z_wrt_truth",400,-100,100);
   //
   return StatusCode::SUCCESS;
}

// ------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------

void MyPackageAlg::fill_vertex_info(const std::vector<TVector3>& v_prim_vertices,
				    const std::vector<TVector3>& v_chg_prod)
{
   fill_h1_each("pvtx_n", v_prim_vertices.size());
   float pvtx_x=-9.5; float pvtx_y=-9.5; float pvtx_z=-199.5;
   if( v_prim_vertices.size() >= 1 ) {
      pvtx_x = (v_prim_vertices[0]).x();
      pvtx_y = (v_prim_vertices[0]).y();
      pvtx_z = (v_prim_vertices[0]).z();
   }
   fill_h1_each("pvtx_x", pvtx_x);
   fill_h1_each("pvtx_y", pvtx_y);
   fill_h1_each("pvtx_z", pvtx_z);

   //
   if( v_chg_prod.size() >= 1 ) {
      float z_truth = (v_chg_prod[0]).z();
      fill_h1_each("pvtx_z_wrt_truth", pvtx_z-z_truth);
   }
}

// ------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------

void MyPackageAlg::fill_fittrk_vertex_info(const xAOD::TrigComposite* itMyData, const std::vector<TVector3>& v_chg_prod)
{
   if( v_chg_prod.size() >= 1 ) {
      float z_truth = (v_chg_prod[0]).z();

      // disTrkVtx
      float dz_min = 9999;
      for(unsigned int i=1; i<=5; i++) {
	 char buf[32];
	 sprintf(buf,"disTrkVtx_z%i",i);
	 float vtx_z=0;
	 (itMyData)->getDetail(std::string(buf),vtx_z);
	 if( fabs(vtx_z) > 1e-2 ) {
	    float dz = fabs(vtx_z-z_truth);
	    if( dz < fabs(dz_min) ) dz_min = vtx_z - z_truth;
	 }
      }
      fill_h1_each("disTrkVtx_z_wrt_truth", dz_min);

      // Zfinder
      dz_min = 9999;
      for(unsigned int i=1; i<=5; i++) {
	 char buf[32];
	 sprintf(buf,"zfinderVtx_z%i",i);
	 float vtx_z=0;
	 (itMyData)->getDetail(std::string(buf),vtx_z);
	 if( fabs(vtx_z) > 1e-2 ) {
	    float dz = fabs(vtx_z-z_truth);
	    if( dz < fabs(dz_min) ) dz_min = vtx_z - z_truth;
	 }
      }
      fill_h1_each("zfinderVtx_z_wrt_truth", dz_min);
   }
}

// ================================================================================================
// ================================================================================================

StatusCode MyPackageAlg::book_fittrk_info_base(const std::string& prefix)
{
   book_h1_each(prefix+"_pt",         201,0,201);
   book_h1_each(prefix+"_d0",         100,-1,1); 
   book_h1_each(prefix+"_d0_wrtVtx",  100,-1,1);
   book_h1_each(prefix+"_z0",           200,-10,10);
   book_h1_each(prefix+"_z0_wrtOffPVtx",200,-10,10);
   book_h1_each(prefix+"_z0_wrtVtx",    200,-10,10);
   book_h1_each(prefix+"_eta",        160,-4,4);
   book_h1_each(prefix+"_phi",        128,-3.2,3.2);
   book_h1_each(prefix+"_chi2",       210,-0.5,20.5);
   book_h1_each(prefix+"_chi2_ndof",  220,-0.5,10.5);
   book_h1_each(prefix+"_n_hits_bl",  11,0,11);
   book_h1_each(prefix+"_n_hits_pix", 11,0,11);
   book_h1_each(prefix+"_n_hits_sct", 11,0,11);
   book_h1_each(prefix+"_is_fail",    2,-0.5,1.5);

   book_h1_each(prefix+"_iso1_dr01", 110,-0.5,10.5);
   book_h1_each(prefix+"_iso1_dr02", 110,-0.5,10.5);
   book_h1_each(prefix+"_iso1_dr04", 110,-0.5,10.5);
   book_h1_each(prefix+"_iso2_dr01", 110,-0.5,10.5);
   book_h1_each(prefix+"_iso2_dr02", 110,-0.5,10.5);
   book_h1_each(prefix+"_iso2_dr04", 110,-0.5,10.5);
   book_h1_each(prefix+"_iso3_dr01", 110,-0.5,10.5);
   book_h1_each(prefix+"_iso3_dr02", 110,-0.5,10.5);
   book_h1_each(prefix+"_iso3_dr04", 110,-0.5,10.5);

   book_h1_each(prefix+"_refit_pt",         201,0,201);
   book_h1_each(prefix+"_refit_eta",        160,-4,4);
   book_h1_each(prefix+"_refit_phi",        128,-3.2,3.2);
   book_h1_each(prefix+"_refit_d0_wrtVtx",  100,-1,1);
   book_h1_each(prefix+"_refit_z0_wrtVtx",  200,-10,10);
   book_h1_each(prefix+"_refit_chi2_ndof",  220,-0.5,10.5);
   book_h1_each(prefix+"_refit_chi2_ndof_ratio", 102,-0.1,10.1);
   book_h1_each(prefix+"_refit_pt_ratio",        102,-0.1,10.1);
   book_h1_each(prefix+"_refit_chi2_ndof_pix_ratio", 102,-0.1,10.1);

   if( m_is_bdt_validation ) { book_h1_each(prefix+"_bdtscore", 220, -0.55, 0.55); }

   std::vector<std::string> v_names;
   v_names.push_back("ibl");  v_names.push_back("pix1"); v_names.push_back("pix2"); v_names.push_back("pix3");
   v_names.push_back("sct1"); v_names.push_back("sct2"); v_names.push_back("sct3"); v_names.push_back("sct4");
   for(unsigned int j=0; j<8; j++) { book_h1_each(prefix+"_n_brhits_"+v_names[j],      8,0,8); }
   for(unsigned int j=0; j<8; j++) { book_h1_each(prefix+"_n_brhits_good_"+v_names[j], 8,0,8); }
   for(unsigned int j=0; j<8; j++) { book_h1_each(prefix+"_brhits_chi2_"+v_names[j],      210,-0.5,20.5); }
   for(unsigned int j=0; j<8; j++) { book_h1_each(prefix+"_brhits_chi2_ndof_"+v_names[j], 220,-0.5,10.5); }
   for(unsigned int j=0; j<8; j++) { book_h1_each(prefix+"_is_brhits_good_"+v_names[j],       2,0,2); }
   for(unsigned int j=0; j<8; j++) { book_h1_each(prefix+"_is_brhits_doublegood_"+v_names[j], 2,0,2); }
   //
   for(unsigned int j=0; j<8; j++) { book_h1_each(prefix+"_refit_n_brhits_"+v_names[j],      8,0,8); }
   for(unsigned int j=0; j<8; j++) { book_h1_each(prefix+"_refit_n_brhits_good_"+v_names[j], 8,0,8); }
   for(unsigned int j=0; j<8; j++) { book_h1_each(prefix+"_refit_brhits_chi2_"+v_names[j],      210,-0.5,20.5); }
   for(unsigned int j=0; j<8; j++) { book_h1_each(prefix+"_refit_brhits_chi2_ndof_"+v_names[j], 220,-0.5,10.5); }
   for(unsigned int j=0; j<8; j++) { book_h1_each(prefix+"_refit_is_brhits_good_"+v_names[j],       2,0,2); }
   for(unsigned int j=0; j<8; j++) { book_h1_each(prefix+"_refit_is_brhits_doublegood_"+v_names[j], 2,0,2); }

   book_h1_each(prefix+"_n_doublegood_brlayers_pix",5,0,5);
   book_h1_each(prefix+"_n_doublegood_brlayers_sct",5,0,5);
   book_h1_each(prefix+"_chi2_pix",       210,-0.5,20.5);
   book_h1_each(prefix+"_chi2_ndof_pix",  220,-0.5,10.5);
   //
   book_h1_each(prefix+"_refit_n_doublegood_brlayers_pix",5,0,5);
   book_h1_each(prefix+"_refit_n_doublegood_brlayers_sct",5,0,5);
   book_h1_each(prefix+"_refit_chi2_pix",       210,-0.5,20.5);
   book_h1_each(prefix+"_refit_chi2_ndof_pix",  220,-0.5,10.5);

   book_h1_each(prefix+"_category",4,0.5,4.5);
   m_map_h1[prefix+"_category"]->GetXaxis()->SetBinLabel(1,"pix4l_sct0");
   m_map_h1[prefix+"_category"]->GetXaxis()->SetBinLabel(2,"pix4l_sct1p");
   m_map_h1[prefix+"_category"]->GetXaxis()->SetBinLabel(3,"pix3l_sct0");
   m_map_h1[prefix+"_category"]->GetXaxis()->SetBinLabel(4,"pix3l_sct1p");
   m_map_h1[prefix+"_category"]->GetXaxis()->SetLabelSize(0.06);
   //
   std::vector<std::string> vpre;
   vpre.push_back("_pix4l");
   vpre.push_back("_pix3l");
   for(unsigned int j=0; j<vpre.size(); j++) {
      std::string pre = vpre[j];
      std::vector<std::string> ppfix;
      // ppfix.push_back("");
      ppfix.push_back("_sct0");
      ppfix.push_back("_sct1p");
      for(unsigned int k=0; k<ppfix.size(); k++) {
	 book_h1_each(prefix+pre+ppfix[k]+"_pt",         201,0,201);
	 book_h1_each(prefix+pre+ppfix[k]+"_eta",        160,-4,4);
	 book_h1_each(prefix+pre+ppfix[k]+"_phi",        128,-3.2,3.2);
	 book_h1_each(prefix+pre+ppfix[k]+"_d0_wrtVtx",  100,-1,1);
	 book_h1_each(prefix+pre+ppfix[k]+"_z0_wrtVtx",  200,-10,10);
	 book_h1_each(prefix+pre+ppfix[k]+"_chi2_ndof",  220,-0.5,10.5);
	 book_h1_each(prefix+pre+ppfix[k]+"_n_hits_bl",  11,0,11);
	 book_h1_each(prefix+pre+ppfix[k]+"_n_hits_pix", 11,0,11);
	 book_h1_each(prefix+pre+ppfix[k]+"_n_hits_sct", 11,0,11);
	 book_h1_each(prefix+pre+ppfix[k]+"_chi2_ndof_pix",  220,-0.5,10.5);
	 book_h1_each(prefix+pre+ppfix[k]+"_refit_pt",         201,0,201);
	 book_h1_each(prefix+pre+ppfix[k]+"_refit_eta",        160,-4,4);
	 book_h1_each(prefix+pre+ppfix[k]+"_refit_phi",        128,-3.2,3.2);
	 book_h1_each(prefix+pre+ppfix[k]+"_refit_d0_wrtVtx",  100,-1,1);
	 book_h1_each(prefix+pre+ppfix[k]+"_refit_z0_wrtVtx",  200,-10,10);
	 book_h1_each(prefix+pre+ppfix[k]+"_refit_chi2_ndof",  220,-0.5,10.5);
	 book_h1_each(prefix+pre+ppfix[k]+"_refit_chi2_ndof_ratio", 102,-0.1,10.1);
	 book_h1_each(prefix+pre+ppfix[k]+"_refit_pt_ratio",        102,-0.1,10.1);
	 book_h1_each(prefix+pre+ppfix[k]+"_refit_chi2_ndof_pix_ratio", 102,-0.1,10.1);
	 book_h1_each(prefix+pre+ppfix[k]+"_is_fail",             2,-0.5,1.5);
	 book_h1_each(prefix+pre+ppfix[k]+"_iso1_dr01", 110,-0.5,10.5);
	 book_h1_each(prefix+pre+ppfix[k]+"_iso1_dr02", 110,-0.5,10.5);
	 book_h1_each(prefix+pre+ppfix[k]+"_iso1_dr04", 110,-0.5,10.5);
	 book_h1_each(prefix+pre+ppfix[k]+"_iso2_dr01", 110,-0.5,10.5);
	 book_h1_each(prefix+pre+ppfix[k]+"_iso2_dr02", 110,-0.5,10.5);
	 book_h1_each(prefix+pre+ppfix[k]+"_iso2_dr04", 110,-0.5,10.5);
	 book_h1_each(prefix+pre+ppfix[k]+"_iso3_dr01", 110,-0.5,10.5);
	 book_h1_each(prefix+pre+ppfix[k]+"_iso3_dr02", 110,-0.5,10.5);
	 book_h1_each(prefix+pre+ppfix[k]+"_iso3_dr04", 110,-0.5,10.5);
	 book_h1_each(prefix+pre+ppfix[k]+"_refit_n_doublegood_brlayers_pix",5,0,5);
	 book_h1_each(prefix+pre+ppfix[k]+"_refit_n_doublegood_brlayers_sct",5,0,5);
	 book_h1_each(prefix+pre+ppfix[k]+"_refit_chi2_pix",       210,-0.5,20.5);
	 book_h1_each(prefix+pre+ppfix[k]+"_refit_chi2_ndof_pix",  220,-0.5,10.5);
	 for(unsigned int j=0; j<8; j++) { book_h1_each(prefix+pre+ppfix[k]+"_n_brhits_"+v_names[j],      8,0,8); }
	 for(unsigned int j=0; j<8; j++) { book_h1_each(prefix+pre+ppfix[k]+"_n_brhits_good_"+v_names[j], 8,0,8); }
	 for(unsigned int j=0; j<8; j++) { book_h1_each(prefix+pre+ppfix[k]+"_refit_n_brhits_"+v_names[j],      8,0,8); }
	 for(unsigned int j=0; j<8; j++) { book_h1_each(prefix+pre+ppfix[k]+"_refit_n_brhits_good_"+v_names[j], 8,0,8); }
	 if( m_is_bdt_validation ) { book_h1_each(prefix+pre+ppfix[k]+"_bdtscore", 220, -0.55, 0.55); }
      }
   }

   //
   return StatusCode::SUCCESS;
}

// ------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------

bool MyPackageAlg::is_bdt_passed(int cat, const std::string& prefix, const xAOD::TrigComposite* itMyData, float bdt_score)
{
   if( cat<=0 || cat>=5 ) return false;

   float pt = 0;
   if( cat==1 || cat==3 ) {
      TVector3 p3;
      get_fittrk_p3(prefix,itMyData,p3);
      pt = p3.Pt();
   }
   
   float refit_pt = 0;
   if( cat==2 || cat==4 ) {
      TVector3 p3;
      float d0, d0err, z0, chi2, ndof;
      int  n_hits_bl, n_hits_pix, n_hits_sct;
      get_fittrk_trk(prefix+"_refit",itMyData,p3,d0,d0err,z0,chi2,ndof,n_hits_bl,n_hits_pix,n_hits_sct);
      refit_pt = p3.Pt();
   }
   
   const float bdt_threshold_pix4l_sct0  = 0.250;//bdt9.2:0.25(100%) bdt9:0.270(96%)  bdt7(&8):0.320 90%    effi 98% (rej: ~3.5)
   const float bdt_threshold_pix4l_sct1p = -0.020; //bdt9.2:0.05(99%)  bdt9:0.08(98%)   bdt8:0.190(90%)  bdt7:0.160(96%)  0.230effi90%  0.172 effi 96%  0.095 effi 98% (rej: ~50) 0.167
   const float bdt_threshold_pix3l_sct0  = 0.090; //bdt9.2:0.14(67%): bdt9:0.05(82%)   bdt8:0.15(60%)  bdt7:0.125(67%)  0.201effi80%  0.010 effi 90% (rej: ~100) 0.167
   const float bdt_threshold_pix3l_sct1p = 0.040; //bdt9.2:0.140(60%) bdt9:0.08(80%)   bdt8:0.195(40%)  bdt7:0.160(50%)  effi 40% (rej: ~1100)

   const float pt_threshold_pix4l_sct0        = 20.0;
   const float refit_pt_threshold_pix4l_sct1p = 20.0;
   const float pt_threshold_pix3l_sct0        = 20.0;
   const float refit_pt_threshold_pix3l_sct1p = 20.0;

   bool is_passed = false;

   if( cat==1 ) {
      if( pt >= pt_threshold_pix4l_sct0 && bdt_score >= bdt_threshold_pix4l_sct0 ) is_passed = true;
   }
   else if( cat==2 ) {
      if( refit_pt >= refit_pt_threshold_pix4l_sct1p && bdt_score >= bdt_threshold_pix4l_sct1p ) is_passed = true;
   }
   else if( cat==3 ) {
      if( pt >= pt_threshold_pix3l_sct0 && bdt_score >= bdt_threshold_pix3l_sct0 ) is_passed = true;
   }
   else if( cat==4 ) {
      if( refit_pt >= refit_pt_threshold_pix3l_sct1p && bdt_score >= bdt_threshold_pix3l_sct1p ) is_passed = true;
   }

   return is_passed;
}

// ------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------

float MyPackageAlg::bdt_eval_pix4l_sct0(float pt, float z0, float d0, float trkiso3_dr01, float trkiso3_dr0201, float chi2ndof, float chi2ndof_pix, float refit_pt, int n_pix, float refit_pt_ratio, float refit_chi2ndof, int n_bl)
{
   m_tmva_pix4l_sct0_pt             = pt;
   m_tmva_pix4l_sct0_z0             = z0;
   m_tmva_pix4l_sct0_d0             = d0;
   m_tmva_pix4l_sct0_trkiso3_dr01   = trkiso3_dr01;
   m_tmva_pix4l_sct0_trkiso3_dr0201 = trkiso3_dr0201;
   m_tmva_pix4l_sct0_chi2ndof       = chi2ndof;  
   m_tmva_pix4l_sct0_chi2ndof_pix   = chi2ndof_pix;
   m_tmva_pix4l_sct0_refit_pt       = refit_pt;
   m_tmva_pix4l_sct0_n_pix          = (float)n_pix;
   m_tmva_pix4l_sct0_refit_pt_ratio = refit_pt_ratio;
   m_tmva_pix4l_sct0_refit_chi2ndof = refit_chi2ndof;
   m_tmva_pix4l_sct0_n_bl           = (float)n_bl;
   return m_tmva_pix4l_sct0_reader->EvaluateMVA("BDT method");
}

float MyPackageAlg::bdt_eval_pix4l_sct1p(float pt, float refit_pt, float refit_z0, float refit_d0, int n_sct, float refit_pt_ratio,
					 float refit_chi2ndof_ratio, float trkiso3_dr01, float trkiso3_dr0201, int is_fail,float chi2ndof_pix, int n_pix)
{
   m_tmva_pix4l_sct1p_pt             = pt;
   //m_tmva_pix4l_sct1p_z0             = z0;
   //m_tmva_pix4l_sct1p_d0             = d0;
   m_tmva_pix4l_sct1p_refit_pt       = refit_pt;
   m_tmva_pix4l_sct1p_refit_z0       = refit_z0;
   m_tmva_pix4l_sct1p_refit_d0       = refit_d0;
   m_tmva_pix4l_sct1p_n_sct          = (float)n_sct;
   m_tmva_pix4l_sct1p_refit_pt_ratio = refit_pt_ratio;
   m_tmva_pix4l_sct1p_refit_chi2ndof_ratio = refit_chi2ndof_ratio;
   m_tmva_pix4l_sct1p_trkiso3_dr01   = trkiso3_dr01;
   m_tmva_pix4l_sct1p_trkiso3_dr0201 = trkiso3_dr0201;
   m_tmva_pix4l_sct1p_is_fail        = (float)is_fail;
   m_tmva_pix4l_sct1p_chi2ndof_pix   = chi2ndof_pix;
   m_tmva_pix4l_sct1p_n_pix          = (float)n_pix;
   return m_tmva_pix4l_sct1p_reader->EvaluateMVA("BDT method");
}

float MyPackageAlg::bdt_eval_pix3l_sct0(float pt, float z0, float d0, float chi2ndof, float chi2ndof_pix, float trkiso3_dr01, float trkiso3_dr0201, float refit_pt, float refit_z0, float refit_d0, int n_pix, int n_bl)
{
   m_tmva_pix3l_sct0_pt             = pt;
   m_tmva_pix3l_sct0_z0             = z0;
   m_tmva_pix3l_sct0_d0             = d0;
   m_tmva_pix3l_sct0_chi2ndof       = chi2ndof;
   m_tmva_pix3l_sct0_chi2ndof_pix   = chi2ndof_pix;
   m_tmva_pix3l_sct0_trkiso3_dr01   = trkiso3_dr01;
   m_tmva_pix3l_sct0_trkiso3_dr0201 = trkiso3_dr0201;
   m_tmva_pix3l_sct0_refit_pt       = refit_pt;
   m_tmva_pix3l_sct0_refit_z0       = refit_z0;
   m_tmva_pix3l_sct0_refit_d0       = refit_d0;
   m_tmva_pix3l_sct0_n_pix          = (float)n_pix;
   m_tmva_pix3l_sct0_n_bl           = (float)n_bl;
   return m_tmva_pix3l_sct0_reader->EvaluateMVA("BDT method");
}

float MyPackageAlg::bdt_eval_pix3l_sct1p(float pt, float z0, float d0, float refit_pt, float refit_z0, float refit_d0,
					 int n_pix, int n_sct, float refit_pt_ratio, int is_fail, int n_bl, float chi2ndof, float refit_chi2ndof, float trkiso3_dr01, float trkiso3_dr0201)
{
   m_tmva_pix3l_sct1p_pt             = pt;
   m_tmva_pix3l_sct1p_z0             = z0;
   m_tmva_pix3l_sct1p_d0             = d0;
   m_tmva_pix3l_sct1p_refit_pt       = refit_pt;
   m_tmva_pix3l_sct1p_refit_z0       = refit_z0;
   m_tmva_pix3l_sct1p_refit_d0       = refit_d0;
   m_tmva_pix3l_sct1p_n_pix          = (float)n_pix;
   m_tmva_pix3l_sct1p_n_sct          = (float)n_sct;
   m_tmva_pix3l_sct1p_refit_pt_ratio = refit_pt_ratio;
   m_tmva_pix3l_sct1p_is_fail        = (float)is_fail;
   m_tmva_pix3l_sct1p_n_bl           = (float)n_bl;
   m_tmva_pix3l_sct1p_chi2ndof       = chi2ndof;
   m_tmva_pix3l_sct1p_refit_chi2ndof = refit_chi2ndof;
   m_tmva_pix3l_sct1p_trkiso3_dr01   = trkiso3_dr01;
   m_tmva_pix3l_sct1p_trkiso3_dr0201 = trkiso3_dr0201;
   return m_tmva_pix3l_sct1p_reader->EvaluateMVA("BDT method");
}
// ------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------

int MyPackageAlg::get_fittrk_category(const std::string& prefix_aod, const xAOD::TrigComposite* itMyData)
{
   TVector3 trk_p3;
   float trk_d0=0; float trk_d0err=0; float trk_z0=0; float trk_chi2=0; float trk_ndof=0;
   int trk_n_hits_bl=0; int trk_n_hits_pix=0; int trk_n_hits_sct=0;
   get_fittrk_trk(prefix_aod, itMyData, trk_p3, trk_d0, trk_d0err, trk_z0, trk_chi2, trk_ndof, trk_n_hits_bl, trk_n_hits_pix, trk_n_hits_sct);

   std::vector<int>   v_nhits;
   std::vector<float> v_chi2;
   std::vector<int>   v_ndof;
   std::vector<int>   v_nhits_good;
   get_fittrk_brhits(prefix_aod, itMyData, v_nhits, v_chi2, v_ndof, v_nhits_good);

   std::vector<std::string> v_names;
   v_names.push_back("ibl");  v_names.push_back("pix1"); v_names.push_back("pix2"); v_names.push_back("pix3");
   v_names.push_back("sct1"); v_names.push_back("sct2"); v_names.push_back("sct3"); v_names.push_back("sct4");
   int n_doublegood_brlayers_pix = 0;
   int n_doublegood_brlayers_sct = 0;
   float chi2_pix = 0; int ndof_pix = 0;
   for(unsigned int j=0; j<8; j++) {
      if( j<=3 ) {
	 chi2_pix += v_chi2[j];
	 ndof_pix += v_ndof[j];
      }
      int incl_good   = 0;
      int double_good = 0;
      if( v_nhits_good[j] >= 1 ) incl_good=1;
      if( j<= 3 ) { double_good = incl_good; }
      else {
	 if( v_nhits_good[j] >= 2 ) double_good=1;
      }
      if( double_good == 1 ) {
	 (j<=3) ? n_doublegood_brlayers_pix++ :  n_doublegood_brlayers_sct++; 
      }
   }

   // category
   int cat = 0;
   if( n_doublegood_brlayers_pix == 4 ) {
      if( trk_n_hits_sct==0 ) { cat=1; }
      else                    { cat=2; }
   }
   else if( n_doublegood_brlayers_pix == 3 ) {
      if( trk_n_hits_sct==0 ) { cat=3; }
      else                    { cat=4; }
   }

   // 
   return cat;
}

// ------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------

float MyPackageAlg::fill_fittrk_info_base(const std::string& prefix_aod, const std::string& prefix_hist,
					  const xAOD::TrigComposite* itMyData, float pvtx_z,
					  std::map<std::string, float>& varf, std::map<std::string, int>& vari,
					  std::map<unsigned int, std::map<std::string, float> >& map_goodTrk_varf,
					  bool doDump, std::ofstream& ofs)
{
   TVector3 trk_p3;
   float trk_d0=0; float trk_d0err=0; float trk_z0=0; float trk_chi2=0; float trk_ndof=0;
   int trk_n_hits_bl=0; int trk_n_hits_pix=0; int trk_n_hits_sct=0;
   get_fittrk_trk(prefix_aod, itMyData, trk_p3, trk_d0, trk_d0err, trk_z0, trk_chi2, trk_ndof, trk_n_hits_bl, trk_n_hits_pix, trk_n_hits_sct);

   float d0_wrtVtx=0; float z0_wrtVtx=0; float d0err_wrtVtx=0; float z0err_wrtVtx=0;
   std::vector<float> v_vtx_z;
   get_fittrk_vtx(prefix_aod, itMyData, d0_wrtVtx, z0_wrtVtx, d0err_wrtVtx, z0err_wrtVtx, v_vtx_z);

   // track info
   double chi2_ndof  = 20.0;
   if( fabs(trk_ndof) > 1e-5 ) chi2_ndof = trk_chi2/trk_ndof;
   fill_h1_each(prefix_hist+"_pt",            hist_pt(trk_p3.Pt()));
   fill_h1_each(prefix_hist+"_eta",           trk_p3.Eta());
   fill_h1_each(prefix_hist+"_phi",           trk_p3.Phi());
   fill_h1_each(prefix_hist+"_d0",            trk_d0);
   fill_h1_each(prefix_hist+"_z0_wrtVtx",     z0_wrtVtx);
   fill_h1_each(prefix_hist+"_z0",            trk_z0);
   fill_h1_each(prefix_hist+"_z0_wrtOffPVtx", trk_z0_wrt_pvtx(trk_z0, pvtx_z));
   fill_h1_each(prefix_hist+"_d0_wrtVtx",     d0_wrtVtx);
   fill_h1_each(prefix_hist+"_chi2",          hist_chi2(trk_chi2));
   fill_h1_each(prefix_hist+"_chi2_ndof",     hist_chi2ndof(chi2_ndof));
   fill_h1_each(prefix_hist+"_n_hits_bl",     trk_n_hits_bl);
   fill_h1_each(prefix_hist+"_n_hits_pix",    trk_n_hits_pix);
   fill_h1_each(prefix_hist+"_n_hits_sct",    trk_n_hits_sct);
   fill_h1_each(prefix_hist+"_is_fail",       vari["is_fail"]);

   // isolation
   std::map<std::string, float> map_isol;
   fittrk_calc_isolation(prefix_aod,itMyData,map_goodTrk_varf,map_isol);
   fill_h1_each(prefix_hist+"_iso1_dr01", map_isol["iso1_dr01"]);
   fill_h1_each(prefix_hist+"_iso1_dr02", map_isol["iso1_dr02"]);
   fill_h1_each(prefix_hist+"_iso1_dr04", map_isol["iso1_dr04"]);
   fill_h1_each(prefix_hist+"_iso2_dr01", map_isol["iso2_dr01"]);
   fill_h1_each(prefix_hist+"_iso2_dr02", map_isol["iso2_dr02"]);
   fill_h1_each(prefix_hist+"_iso2_dr04", map_isol["iso2_dr04"]);
   fill_h1_each(prefix_hist+"_iso3_dr01", map_isol["iso3_dr01"]);
   fill_h1_each(prefix_hist+"_iso3_dr02", map_isol["iso3_dr02"]);
   fill_h1_each(prefix_hist+"_iso3_dr04", map_isol["iso3_dr04"]);

   // refit
   double chi2_ndof_refit = 20.0;
   if( vari["refit_ndof"] != 0 ) chi2_ndof_refit = varf["refit_chi2"]/vari["refit_ndof"];
   fill_h1_each(prefix_hist+"_refit_pt",        hist_pt(varf["refit_pt"]));
   fill_h1_each(prefix_hist+"_refit_eta",       varf["refit_eta"]);
   fill_h1_each(prefix_hist+"_refit_phi",       varf["refit_phi"]);
   fill_h1_each(prefix_hist+"_refit_d0_wrtVtx", varf["refit_d0_wrtVtx"]);
   fill_h1_each(prefix_hist+"_refit_z0_wrtVtx", varf["refit_z0_wrtVtx"]);
   fill_h1_each(prefix_hist+"_refit_chi2_ndof", hist_chi2ndof(chi2_ndof_refit));
   fill_h1_each(prefix_hist+"_refit_chi2_ndof_ratio", hist_refit_ratio(chi2_ndof_refit/chi2_ndof));
   fill_h1_each(prefix_hist+"_refit_pt_ratio",        hist_refit_ratio(varf["refit_pt"]/trk_p3.Pt()));

   // n layers
   std::vector<int>   v_nhits;
   std::vector<float> v_chi2;
   std::vector<int>   v_ndof;
   std::vector<int>   v_nhits_good;
   get_fittrk_brhits(prefix_aod, itMyData, v_nhits, v_chi2, v_ndof, v_nhits_good);

   std::vector<std::string> v_names;
   v_names.push_back("ibl");  v_names.push_back("pix1"); v_names.push_back("pix2"); v_names.push_back("pix3");
   v_names.push_back("sct1"); v_names.push_back("sct2"); v_names.push_back("sct3"); v_names.push_back("sct4");
   int n_doublegood_brlayers_pix = 0;
   int n_doublegood_brlayers_sct = 0;
   float chi2_pix = 0; int ndof_pix = 0;
   for(unsigned int j=0; j<8; j++) {
      fill_h1_each(prefix_hist+"_n_brhits_"+v_names[j],       v_nhits[j]);
      fill_h1_each(prefix_hist+"_n_brhits_good_"+v_names[j],  v_nhits_good[j]);
      if( v_nhits[j] > 0 ) {
	 fill_h1_each(prefix_hist+"_brhits_chi2_"+v_names[j],      hist_chi2(v_chi2[j]));
	 fill_h1_each(prefix_hist+"_brhits_chi2_ndof_"+v_names[j], hist_chi2ndof(v_chi2[j]/v_ndof[j]));
      }
      if( j<=3 ) {
	 chi2_pix += v_chi2[j];
	 ndof_pix += v_ndof[j];
      }
      //
      int incl_good   = 0;
      int double_good = 0;
      if( v_nhits_good[j] >= 1 ) incl_good=1;
      if( j<= 3 ) { double_good = incl_good; }
      else {
	 if( v_nhits_good[j] >= 2 ) double_good=1;
      }
      fill_h1_each(prefix_hist+"_is_brhits_good_"+v_names[j],      incl_good);
      fill_h1_each(prefix_hist+"_is_brhits_doublegood_"+v_names[j],double_good);
      if( double_good == 1 ) {
	 (j<=3) ? n_doublegood_brlayers_pix++ :  n_doublegood_brlayers_sct++; 
      }
   }
   fill_h1_each(prefix_hist+"_n_doublegood_brlayers_pix", n_doublegood_brlayers_pix);
   fill_h1_each(prefix_hist+"_n_doublegood_brlayers_sct", n_doublegood_brlayers_sct);

   double chi2_ndof_pix = 20.0;
   if( ndof_pix > 0 ) chi2_ndof_pix = chi2_pix / ndof_pix;
   fill_h1_each(prefix_hist+"_chi2_pix",     hist_chi2(chi2_pix));
   fill_h1_each(prefix_hist+"_chi2_ndof_pix",hist_chi2ndof(chi2_ndof_pix));

   // refit n layers
   std::vector<int>   v_nhits_refit;
   std::vector<float> v_chi2_refit;
   std::vector<int>   v_ndof_refit;
   std::vector<int>   v_nhits_good_refit;
   get_fittrk_brhits(prefix_aod+"_refit", itMyData, v_nhits_refit, v_chi2_refit, v_ndof_refit, v_nhits_good_refit);

   std::vector<std::string> v_names_refit;
   v_names_refit.push_back("ibl");  v_names_refit.push_back("pix1"); v_names_refit.push_back("pix2"); v_names_refit.push_back("pix3");
   v_names_refit.push_back("sct1"); v_names_refit.push_back("sct2"); v_names_refit.push_back("sct3"); v_names_refit.push_back("sct4");
   int n_doublegood_brlayers_pix_refit = 0;
   int n_doublegood_brlayers_sct_refit = 0;
   float chi2_pix_refit = 0; int ndof_pix_refit = 0;
   for(unsigned int j=0; j<8; j++) {
      fill_h1_each(prefix_hist+"_refit_n_brhits_"+v_names_refit[j],       v_nhits_refit[j]);
      fill_h1_each(prefix_hist+"_refit_n_brhits_good_"+v_names_refit[j],  v_nhits_good_refit[j]);
      if( v_nhits_refit[j] > 0 ) {
	 fill_h1_each(prefix_hist+"_refit_brhits_chi2_"+v_names_refit[j],      hist_chi2(v_chi2_refit[j]));
	 fill_h1_each(prefix_hist+"_refit_brhits_chi2_ndof_"+v_names_refit[j], hist_chi2ndof(v_chi2_refit[j]/v_ndof_refit[j]));
      }
      if( j<=3 ) {
	 chi2_pix_refit += v_chi2_refit[j];
	 ndof_pix_refit += v_ndof_refit[j];
      }
      //
      int incl_good_refit   = 0;
      int double_good_refit = 0;
      if( v_nhits_good_refit[j] >= 1 ) incl_good_refit=1;
      if( j<= 3 ) { double_good_refit = incl_good_refit; }
      else {
	 if( v_nhits_good_refit[j] >= 2 ) double_good_refit=1;
      }
      fill_h1_each(prefix_hist+"_refit_is_brhits_good_"+v_names_refit[j],      incl_good_refit);
      fill_h1_each(prefix_hist+"_refit_is_brhits_doublegood_"+v_names_refit[j],double_good_refit);
      if( double_good_refit == 1 ) {
	 (j<=3) ? n_doublegood_brlayers_pix_refit++ :  n_doublegood_brlayers_sct_refit++; 
      }
   }
   fill_h1_each(prefix_hist+"_refit_n_doublegood_brlayers_pix", n_doublegood_brlayers_pix_refit);
   fill_h1_each(prefix_hist+"_refit_n_doublegood_brlayers_sct", n_doublegood_brlayers_sct_refit);

   double chi2_ndof_pix_refit = 20.0;
   if( ndof_pix_refit > 0 ) chi2_ndof_pix_refit = chi2_pix_refit / ndof_pix_refit;
   fill_h1_each(prefix_hist+"_refit_chi2_pix",     hist_chi2(chi2_pix_refit));
   fill_h1_each(prefix_hist+"_refit_chi2_ndof_pix",hist_chi2ndof(chi2_ndof_pix_refit));
   fill_h1_each(prefix_hist+"_refit_chi2_ndof_pix_ratio",chi2_ndof_pix_refit/chi2_ndof_pix);

   // 
   // category
   int cat = 0;
   if( n_doublegood_brlayers_pix == 4 ) {
      if( trk_n_hits_sct==0 ) { cat=1; }
      else                    { cat=2; }
   }
   else if( n_doublegood_brlayers_pix == 3 ) {
      if( trk_n_hits_sct==0 ) { cat=3; }
      else                    { cat=4; }
   }
   fill_h1_each(prefix_hist+"_category",(float)cat);

   // dump ascii for BDT tuning
   if( doDump ) {
      ofs << trk_p3.Pt() << " " << z0_wrtVtx << " " << d0_wrtVtx << " " << trk_chi2 << " " << trk_ndof;
      ofs << " " << varf["refit_pt"] << " " << varf["refit_z0_wrtVtx"] << " " << varf["refit_d0_wrtVtx"];
      ofs << " " << varf["refit_chi2"] << " " << vari["refit_ndof"];
      ofs << " " << trk_n_hits_bl << " " << trk_n_hits_pix << " " << trk_n_hits_sct;
      ofs << " " << n_doublegood_brlayers_pix << " " << chi2_pix << " " << ndof_pix << " " << cat;
      ofs << " " << map_isol["iso3_dr01"] << " " << map_isol["iso3_dr02"] << " " << map_isol["iso3_dr04"];
      ofs << " " << vari["is_fail"];
      ofs << " " << v_nhits_good[0] << " " << v_nhits_good[1] << " " << v_nhits_good[2] << " " << v_nhits_good[3];
      ofs << std::endl;
   }

   // BDT evaluation
   float bdt_score = 0.0;
   if( m_is_bdt_validation ) {
      if( cat==1 ) {
	 float refit_pt_ratio = varf["refit_pt"] / trk_p3.Pt();
         float chi2ndof = 20.0;
         if( trk_ndof != 0 ) chi2ndof = trk_chi2 / trk_ndof;
         float refit_chi2ndof = 20.0;
         float chi2ndof_pix = 20.0;
         if( ndof_pix != 0 ) chi2ndof_pix = chi2_pix / ndof_pix;
         if( vari["refit_ndof"] != 0 ) refit_chi2ndof = varf["refit_chi2"] / vari["refit_ndof"];
         bdt_score = bdt_eval_pix4l_sct0(trk_p3.Pt(),z0_wrtVtx,d0_wrtVtx,map_isol["iso3_dr01"],map_isol["iso3_dr02"]-map_isol["iso3_dr01"],chi2ndof,chi2ndof_pix,varf["refit_pt"],trk_n_hits_pix,refit_pt_ratio,refit_chi2ndof,trk_n_hits_bl);
      }
      else if( cat==2 ) {
	 float refit_pt_ratio = varf["refit_pt"] / trk_p3.Pt();
	 float chi2ndof = 20.0;
	 if( trk_ndof != 0 ) chi2ndof = trk_chi2 / trk_ndof;
	 float refit_chi2ndof = 20.0;
	 if( vari["refit_ndof"] != 0 ) refit_chi2ndof = varf["refit_chi2"] / vari["refit_ndof"];
         float chi2ndof_pix = 20.0;
         if( ndof_pix != 0 ) chi2ndof_pix = chi2_pix / ndof_pix;
	 float refit_chi2ndof_ratio = refit_chi2ndof / chi2ndof;
	 bdt_score = bdt_eval_pix4l_sct1p(trk_p3.Pt(),varf["refit_pt"],varf["refit_z0_wrtVtx"],varf["refit_d0_wrtVtx"],
					  trk_n_hits_sct,refit_pt_ratio,refit_chi2ndof_ratio,
					  map_isol["iso3_dr01"],map_isol["iso3_dr02"]-map_isol["iso3_dr01"],vari["is_fail"],chi2ndof_pix,trk_n_hits_pix);
      }
      else if( cat==3 ) {
	 float chi2ndof = 20.0;
	 if( trk_ndof != 0 ) chi2ndof = trk_chi2 / trk_ndof;
	 float chi2ndof_pix = 20.0;
	 if( ndof_pix != 0 ) chi2ndof_pix = chi2_pix / ndof_pix;
	 bdt_score = bdt_eval_pix3l_sct0(trk_p3.Pt(),z0_wrtVtx,d0_wrtVtx,chi2ndof,chi2ndof_pix,map_isol["iso3_dr01"],map_isol["iso3_dr02"]-map_isol["iso3_dr01"],varf["refit_pt"],varf["refit_z0_wrtVtx"],varf["refit_d0_wrtVtx"],trk_n_hits_pix,trk_n_hits_bl);
      }
      else if( cat==4 ) {
         float chi2ndof = 20.0;
         if( trk_ndof != 0 ) chi2ndof = trk_chi2 / trk_ndof;
         float refit_chi2ndof = 20.0;
         if( vari["refit_ndof"] != 0 ) refit_chi2ndof = varf["refit_chi2"] / vari["refit_ndof"];
	 float refit_pt_ratio = varf["refit_pt"] / trk_p3.Pt();
	 bdt_score = bdt_eval_pix3l_sct1p(trk_p3.Pt(),z0_wrtVtx,d0_wrtVtx,varf["refit_pt"],varf["refit_z0_wrtVtx"],varf["refit_d0_wrtVtx"],
					  trk_n_hits_pix,trk_n_hits_sct,refit_pt_ratio,vari["is_fail"],trk_n_hits_bl,chi2ndof,refit_chi2ndof,map_isol["iso3_dr01"],map_isol["iso3_dr02"]-map_isol["iso3_dr01"]);
      } 
      //
      fill_h1_each(prefix_hist+"_bdtscore",bdt_score);
   }

   // divide into pix layer 4 and 3
   std::string pfix = "";
   if( n_doublegood_brlayers_pix == 4 )      { pfix = "_pix4l"; }
   else if( n_doublegood_brlayers_pix == 3 ) { pfix = "_pix3l"; }
   if( pfix != "" ) {
      std::vector<std::string> ppfix;
      // ppfix.push_back("");
      if( trk_n_hits_sct==0 ) { ppfix.push_back("_sct0"); }
      else                    { ppfix.push_back("_sct1p"); }
      for(unsigned int j=0; j<ppfix.size(); j++) {
	 fill_h1_each(prefix_hist+pfix+ppfix[j]+"_pt",           hist_pt(trk_p3.Pt()));
	 fill_h1_each(prefix_hist+pfix+ppfix[j]+"_eta",          trk_p3.Eta());
	 fill_h1_each(prefix_hist+pfix+ppfix[j]+"_phi",          trk_p3.Phi());
	 fill_h1_each(prefix_hist+pfix+ppfix[j]+"_d0_wrtVtx",    d0_wrtVtx);
	 fill_h1_each(prefix_hist+pfix+ppfix[j]+"_z0_wrtVtx",    z0_wrtVtx);
	 fill_h1_each(prefix_hist+pfix+ppfix[j]+"_chi2_ndof",    hist_chi2ndof(chi2_ndof));
	 fill_h1_each(prefix_hist+pfix+ppfix[j]+"_n_hits_bl",    trk_n_hits_bl);
	 fill_h1_each(prefix_hist+pfix+ppfix[j]+"_n_hits_pix",   trk_n_hits_pix);
	 fill_h1_each(prefix_hist+pfix+ppfix[j]+"_n_hits_sct",   trk_n_hits_sct);
	 fill_h1_each(prefix_hist+pfix+ppfix[j]+"_is_fail",      vari["is_fail"]);
	 fill_h1_each(prefix_hist+pfix+ppfix[j]+"_chi2_ndof_pix",hist_chi2ndof(chi2_ndof_pix));
	 fill_h1_each(prefix_hist+pfix+ppfix[j]+"_refit_pt",          hist_pt(varf["refit_pt"]));
	 fill_h1_each(prefix_hist+pfix+ppfix[j]+"_refit_eta",         varf["refit_eta"]);
	 fill_h1_each(prefix_hist+pfix+ppfix[j]+"_refit_phi",         varf["refit_phi"]);
	 fill_h1_each(prefix_hist+pfix+ppfix[j]+"_refit_d0_wrtVtx",   varf["refit_d0_wrtVtx"]);
	 fill_h1_each(prefix_hist+pfix+ppfix[j]+"_refit_z0_wrtVtx",   varf["refit_z0_wrtVtx"]);
	 fill_h1_each(prefix_hist+pfix+ppfix[j]+"_refit_chi2_ndof",       hist_chi2ndof(chi2_ndof_refit));
	 fill_h1_each(prefix_hist+pfix+ppfix[j]+"_refit_chi2_ndof_ratio", chi2_ndof_refit/chi2_ndof);
	 fill_h1_each(prefix_hist+pfix+ppfix[j]+"_refit_pt_ratio",    varf["refit_pt"]/trk_p3.Pt());
	 fill_h1_each(prefix_hist+pfix+ppfix[j]+"_iso1_dr01", map_isol["iso1_dr01"]);
	 fill_h1_each(prefix_hist+pfix+ppfix[j]+"_iso1_dr02", map_isol["iso1_dr02"]);
	 fill_h1_each(prefix_hist+pfix+ppfix[j]+"_iso1_dr04", map_isol["iso1_dr04"]);
	 fill_h1_each(prefix_hist+pfix+ppfix[j]+"_iso2_dr01", map_isol["iso2_dr01"]);
	 fill_h1_each(prefix_hist+pfix+ppfix[j]+"_iso2_dr02", map_isol["iso2_dr02"]);
	 fill_h1_each(prefix_hist+pfix+ppfix[j]+"_iso2_dr04", map_isol["iso2_dr04"]);
	 fill_h1_each(prefix_hist+pfix+ppfix[j]+"_iso3_dr01", map_isol["iso3_dr01"]);
	 fill_h1_each(prefix_hist+pfix+ppfix[j]+"_iso3_dr02", map_isol["iso3_dr02"]);
	 fill_h1_each(prefix_hist+pfix+ppfix[j]+"_iso3_dr04", map_isol["iso3_dr04"]);
	 fill_h1_each(prefix_hist+pfix+ppfix[j]+"_refit_n_doublegood_brlayers_pix", n_doublegood_brlayers_pix_refit);
	 fill_h1_each(prefix_hist+pfix+ppfix[j]+"_refit_n_doublegood_brlayers_sct", n_doublegood_brlayers_sct_refit);
	 fill_h1_each(prefix_hist+pfix+ppfix[j]+"_refit_chi2_pix",     hist_chi2(chi2_pix_refit));
	 fill_h1_each(prefix_hist+pfix+ppfix[j]+"_refit_chi2_ndof_pix",hist_chi2ndof(chi2_ndof_pix_refit));
	 fill_h1_each(prefix_hist+pfix+ppfix[j]+"_refit_chi2_ndof_pix_ratio",chi2_ndof_pix_refit/chi2_ndof_pix);
	 //
	 for(unsigned int k=0; k<8; k++) {
	    fill_h1_each(prefix_hist+pfix+ppfix[j]+"_n_brhits_"+v_names[k],       v_nhits[k]);
	    fill_h1_each(prefix_hist+pfix+ppfix[j]+"_n_brhits_good_"+v_names[k],  v_nhits_good[k]);
	    fill_h1_each(prefix_hist+pfix+ppfix[j]+"_refit_n_brhits_"+v_names[k],       v_nhits_refit[k]);
	    fill_h1_each(prefix_hist+pfix+ppfix[j]+"_refit_n_brhits_good_"+v_names[k],  v_nhits_good_refit[k]);
	 }
	 //
	 if( m_is_bdt_validation ) { fill_h1_each(prefix_hist+pfix+ppfix[j]+"_bdtscore",bdt_score); }
      }
   }

   // return
   return bdt_score;
}

// ================================================================================================
// ================================================================================================

StatusCode MyPackageAlg::book_fittrk_n_info(const std::string& prefix)
{
   std::string match = "_match_to_chg";

   book_h1_each(prefix+"_n",             21,0,21);
   book_h1_each(prefix+"_n_presel",      21,0,21);
   book_h1_each(prefix+"_n_bdt",         21,0,21);
   //
   book_h1_each(prefix+"_n"+match,       21,0,21);
   book_h1_each(prefix+"_n_presel"+match,21,0,21);
   book_h1_each(prefix+"_n_bdt"+match,   21,0,21);
   //
   book_h1_each(prefix+"_n_vs_selection",4,0.5,4.5);
   m_map_h1[prefix+"_n_vs_selection"]->GetXaxis()->SetBinLabel(1,"N events");
   m_map_h1[prefix+"_n_vs_selection"]->GetXaxis()->SetBinLabel(2,"Cleaning");
   m_map_h1[prefix+"_n_vs_selection"]->GetXaxis()->SetBinLabel(3,"Presel");
   m_map_h1[prefix+"_n_vs_selection"]->GetXaxis()->SetBinLabel(4,"BDT");
   //
   book_h1_each(prefix+"_n1_vs_selection",4,0.5,4.5);
   m_map_h1[prefix+"_n1_vs_selection"]->GetXaxis()->SetBinLabel(1,"N events");
   m_map_h1[prefix+"_n1_vs_selection"]->GetXaxis()->SetBinLabel(2,"Cleaning");
   m_map_h1[prefix+"_n1_vs_selection"]->GetXaxis()->SetBinLabel(3,"Presel");
   m_map_h1[prefix+"_n1_vs_selection"]->GetXaxis()->SetBinLabel(4,"BDT");
   //
   book_h1_each(prefix+"_n"+match+"_vs_selection",4,0.5,4.5);
   m_map_h1[prefix+"_n"+match+"_vs_selection"]->GetXaxis()->SetBinLabel(1,"N events");
   m_map_h1[prefix+"_n"+match+"_vs_selection"]->GetXaxis()->SetBinLabel(2,"Cleaning");
   m_map_h1[prefix+"_n"+match+"_vs_selection"]->GetXaxis()->SetBinLabel(3,"Presel");
   m_map_h1[prefix+"_n"+match+"_vs_selection"]->GetXaxis()->SetBinLabel(4,"BDT");
   //
   book_h1_each(prefix+"_n1"+match+"_vs_selection",4,0.5,4.5);
   m_map_h1[prefix+"_n1"+match+"_vs_selection"]->GetXaxis()->SetBinLabel(1,"N events");
   m_map_h1[prefix+"_n1"+match+"_vs_selection"]->GetXaxis()->SetBinLabel(2,"Cleaning");
   m_map_h1[prefix+"_n1"+match+"_vs_selection"]->GetXaxis()->SetBinLabel(3,"Presel");
   m_map_h1[prefix+"_n1"+match+"_vs_selection"]->GetXaxis()->SetBinLabel(4,"BDT");

   //
   book_h1_each(prefix+"_n1_xe80_pufit_L1XE50"+match+"_vs_selection",4,0.5,4.5);
   m_map_h1[prefix+"_n1_xe80_pufit_L1XE50"+match+"_vs_selection"]->GetXaxis()->SetBinLabel(1,"N events");
   m_map_h1[prefix+"_n1_xe80_pufit_L1XE50"+match+"_vs_selection"]->GetXaxis()->SetBinLabel(2,"Cleaning");
   m_map_h1[prefix+"_n1_xe80_pufit_L1XE50"+match+"_vs_selection"]->GetXaxis()->SetBinLabel(3,"Presel");
   m_map_h1[prefix+"_n1_xe80_pufit_L1XE50"+match+"_vs_selection"]->GetXaxis()->SetBinLabel(4,"BDT");
   //
   return StatusCode::SUCCESS;
}

// ------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------

float MyPackageAlg::get_fittrk_n_hist(int n) 
{
   if( n > HIST_N_FITTRK_MAX ) return float(HIST_N_FITTRK_MAX);
   return float(n);
}

// ------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------

void MyPackageAlg::fill_fittrk_n_info(const std::string& prefix, std::map<std::string,int>& map_vari)
{
   std::string match = "_match_to_chg";
   std::vector<std::string> v_names;
   v_names.push_back("_n");
   v_names.push_back("_n_presel");
   v_names.push_back("_n_bdt");
   v_names.push_back("_n"+match);
   v_names.push_back("_n_presel"+match);
   v_names.push_back("_n_bdt"+match);
   for(auto it=v_names.begin(); it!=v_names.end(); it++) {
      std::string name = prefix + (*it);
      fill_h1_each(name, get_fittrk_n_hist(map_vari[name]));
   }

   //
   fill_h1_each(prefix+"_n_vs_selection",1);           // nevents
   fill_h1_each(prefix+"_n1_vs_selection",1);          // nevents
   fill_h1_each(prefix+"_n"+match+"_vs_selection",1);  // nevents
   fill_h1_each(prefix+"_n1"+match+"_vs_selection",1); // nevents
   fill_h1_each(prefix+"_n1_xe80_pufit_L1XE50"+match+"_vs_selection",1); // nevents
   for(unsigned int j=0; j<v_names.size()/2; j++) {
      std::string name  = prefix + v_names[j];
      m_map_h1[prefix+"_n_vs_selection"]->Fill(j+2,map_vari[name]);
      if( map_vari[name] > 0 ) fill_h1_each(prefix+"_n1_vs_selection", j+2);
      name = prefix + v_names[j+v_names.size()/2];
      m_map_h1[prefix+"_n"+match+"_vs_selection"]->Fill(j+2,map_vari[name]);
      if( map_vari[name] > 0 ) fill_h1_each(prefix+"_n1"+match+"_vs_selection", j+2);
      if( (map_vari[name] > 0) & (m_map_hlt_results["xe80_pufit_L1XE50"])) fill_h1_each(prefix+"_n1_xe80_pufit_L1XE50"+match+"_vs_selection", j+2);
   }
}

// ================================================================================================
// ================================================================================================

StatusCode MyPackageAlg::book_trigger_info_base()
{
   std::string name = "";

   // define L1 items
   m_v_l1_items.clear();
   m_v_l1_items.push_back("XE35");
   m_v_l1_items.push_back("XE40");
   m_v_l1_items.push_back("XE45");
   m_v_l1_items.push_back("XE50");
   m_v_l1_items.push_back("XE55");
   m_v_l1_items.push_back("XE60");
   m_v_l1_items.push_back("J50");
   m_v_l1_items.push_back("J75");
   m_v_l1_items.push_back("J100");
   m_v_l1_items.push_back("4J15");
   m_v_l1_items.push_back("4J20");

   // define HLT items
   m_v_hlt_chains.clear();
   m_v_hlt_chains.push_back("xe60_pufit_L1XE35");
   m_v_hlt_chains.push_back("xe70_pufit_L1XE35");
   m_v_hlt_chains.push_back("xe80_pufit_L1XE35");
   m_v_hlt_chains.push_back("xe90_pufit_L1XE35");
   m_v_hlt_chains.push_back("xe100_pufit_L1XE35");
   m_v_hlt_chains.push_back("xe110_pufit_L1XE35");
   m_v_hlt_chains.push_back("xe60_pufit_L1XE50");
   m_v_hlt_chains.push_back("xe70_pufit_L1XE50");
   m_v_hlt_chains.push_back("xe80_pufit_L1XE50");
   m_v_hlt_chains.push_back("xe90_pufit_L1XE50");
   m_v_hlt_chains.push_back("xe100_pufit_L1XE50");
   m_v_hlt_chains.push_back("xe110_pufit_L1XE50");
   m_v_hlt_chains.push_back("4j100");
   m_v_hlt_chains.push_back("4j110");
   m_v_hlt_chains.push_back("4j120");
   m_v_hlt_chains.push_back("4j60_gsc100_boffperf_split");
   m_v_hlt_chains.push_back("4j60_gsc110_boffperf_split");
   m_v_hlt_chains.push_back("4j60_gsc115_boffperf_split");
   m_v_hlt_chains.push_back("j260");
   m_v_hlt_chains.push_back("j360");
   m_v_hlt_chains.push_back("j380");
   m_v_hlt_chains.push_back("j400");
   m_v_hlt_chains.push_back("j420");
   m_v_hlt_chains.push_back("j225_gsc400_boffperf_split");
   m_v_hlt_chains.push_back("j225_gsc420_boffperf_split");

   //
   int n_trig = 0;

   name = "l1_counts";
   n_trig = m_v_l1_items.size();
   m_h1_l1_counts = new TH1D(("h1_"+name).c_str(),name.c_str(),n_trig,0,n_trig);
   for(unsigned int i=0; i<m_v_l1_items.size(); i++) {
      m_h1_l1_counts->GetXaxis()->SetBinLabel(i+1,m_v_l1_items[i].c_str());
   }
   CHECK( histSvc()->regHist(("/MYSTREAM/h1_"+name).c_str(), m_h1_l1_counts) );

   name = "l1_bdt_counts";
   n_trig = m_v_l1_items.size();
   m_h1_l1_bdt_counts = new TH1D(("h1_"+name).c_str(),name.c_str(),n_trig,0,n_trig);
   for(unsigned int i=0; i<m_v_l1_items.size(); i++) {
      m_h1_l1_bdt_counts->GetXaxis()->SetBinLabel(i+1,m_v_l1_items[i].c_str());
   }
   CHECK( histSvc()->regHist(("/MYSTREAM/h1_"+name).c_str(), m_h1_l1_bdt_counts) );

   //
   name = "hlt_counts";
   n_trig = m_v_hlt_chains.size();
   m_h1_hlt_counts = new TH1D(("h1_"+name).c_str(),name.c_str(),n_trig,0,n_trig);
   for(unsigned int i=0; i<m_v_hlt_chains.size(); i++) {
      m_h1_hlt_counts->GetXaxis()->SetBinLabel(i+1,m_v_hlt_chains[i].c_str());
   }
   CHECK( histSvc()->regHist(("/MYSTREAM/h1_"+name).c_str(), m_h1_hlt_counts) );

   name = "hlt_bdt_counts";
   n_trig = m_v_hlt_chains.size();
   m_h1_hlt_bdt_counts = new TH1D(("h1_"+name).c_str(),name.c_str(),n_trig,0,n_trig);
   for(unsigned int i=0; i<m_v_hlt_chains.size(); i++) {
      m_h1_hlt_bdt_counts->GetXaxis()->SetBinLabel(i+1,m_v_hlt_chains[i].c_str());
   }
   CHECK( histSvc()->regHist(("/MYSTREAM/h1_"+name).c_str(), m_h1_hlt_bdt_counts) );

   //
   return StatusCode::SUCCESS;
}

// ================================================================================================
// ================================================================================================

StatusCode MyPackageAlg::book_n_triplets_info()
{
   book_h1_each("n_triplets_wide",     100,0,100000);
   book_h1_each("n_triplets",          250,0,5000);
   book_h1_each("n_triplets_w_cbtrk",  250,0,5000);
   book_h1_each("triplets_frac_cbtrk", 100,0,1);
   //
   book_h1_each("n_triplets_allpix_wide",     100,0,100000);
   book_h1_each("n_triplets_allpix",          250,0,5000);
   book_h1_each("n_triplets_allpix_w_cbtrk",  250,0,5000);
   book_h1_each("triplets_allpix_frac_cbtrk", 100,0,1);
   //
   book_h1_each("n_triplets_allpix_allbarrel",          250,0,5000);
   book_h1_each("n_triplets_allpix_allbarrel_w_cbtrk",  250,0,5000);
   book_h1_each("triplets_allpix_allbarrel_frac_cbtrk", 100,0,1);
   //
   return StatusCode::SUCCESS;
}

// ------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------

void MyPackageAlg::fill_n_triplets_info(int n_triplets, int n_triplets_w_cbtrk, int n_triplets_allpix, int n_triplets_allpix_w_cbtrk,
					int n_triplets_allpix_allbarrel, int n_triplets_allpix_allbarrel_w_cbtrk)
{
   fill_h1_each("n_triplets_wide",    n_triplets);
   fill_h1_each("n_triplets",         n_triplets);
   fill_h1_each("n_triplets_w_cbtrk", n_triplets_w_cbtrk);
   fill_h1_each("triplets_frac_cbtrk",(float)n_triplets_w_cbtrk/(float)n_triplets);

   fill_h1_each("n_triplets_allpix_wide",    n_triplets_allpix);
   fill_h1_each("n_triplets_allpix",         n_triplets_allpix);
   fill_h1_each("n_triplets_allpix_w_cbtrk", n_triplets_allpix_w_cbtrk);
   fill_h1_each("triplets_allpix_frac_cbtrk",(float)n_triplets_allpix_w_cbtrk/(float)n_triplets_allpix);

   fill_h1_each("n_triplets_allpix_allbarrel",         n_triplets_allpix_allbarrel);
   fill_h1_each("n_triplets_allpix_allbarrel_w_cbtrk", n_triplets_allpix_allbarrel_w_cbtrk);
   fill_h1_each("triplets_allpix_allbarrel_frac_cbtrk",(float)n_triplets_allpix_allbarrel_w_cbtrk/(float)n_triplets_allpix_allbarrel);
}

// ================================================================================================
// ================================================================================================

float MyPackageAlg::hist_pt(float pt) {
   if( pt > HIST_PT_MAX ) return HIST_PT_MAX;
   return pt;
}

float MyPackageAlg::hist_chi2(float chi2) {
   if( chi2 > HIST_CHI2_MAX ) return HIST_CHI2_MAX;
   return chi2;
}

float MyPackageAlg::hist_chi2ndof(float chi2ndof) {
   if( chi2ndof > HIST_CHI2NDOF_MAX ) return HIST_CHI2NDOF_MAX;
   return chi2ndof;
}

float MyPackageAlg::hist_refit_ratio(float ratio) {
   if( ratio > HIST_REFIT_RATIO_MAX ) return HIST_REFIT_RATIO_MAX;
   return ratio;
}

// ================================================================================================
// ================================================================================================

bool MyPackageAlg::is_target_chg(float chg_decay_r, const TVector3& chg_p3)
{
   // decay between Pixel and SCT
   if( chg_decay_r < CHG_DECAY_R_MIN_VALUE || chg_decay_r > CHG_DECAY_R_MAX_VALUE ) return false;

   // high pT and barrel 
   if( chg_p3.Pt() < CHG_PT_CUT_VALUE ) return false;
   if( abs(chg_p3.Eta()) > CHG_ETA_CUT_VALUE ) return false;

   // all ok
   return true;
}

// ================================================================================================
// ================================================================================================

float MyPackageAlg::trk_z0_wrt_pvtx(float trk_z0, float pvtx_z)
{
   float z0_wrt_pvtx = trk_z0;
   if( fabs(pvtx_z-TRK_Z0_NO_PVTX) > 0.01 ) { z0_wrt_pvtx -= pvtx_z; }
   return z0_wrt_pvtx;
}

// ------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------

bool MyPackageAlg::is_fittrk_match_to_chg(const std::string& prefix, const xAOD::TrigComposite* itMyData,
					  const std::vector<TVector3>& v_chg_p3, float& dr_to_chg, int& idx_of_chg)
{
   dr_to_chg = 9999; idx_of_chg = 9999;
   
   TVector3 trk_p3;
   get_fittrk_p3(prefix,itMyData,trk_p3);
   calc_dr_min(trk_p3, v_chg_p3, dr_to_chg, idx_of_chg);
   //std::cout << " -------dr_to_chg = " << dr_to_chg << "  ->" << "is_match_to_chg finish!!!-------" << std::endl; 
   return (dr_to_chg < DR_TO_CHG_CUT_VALUE);
}

// ================================================================================================
// ================================================================================================

bool MyPackageAlg::is_fittrk_good_for_isolation(const std::string& prefix, const xAOD::TrigComposite* itMyData)
{
   const float PRESEL_PT         = 1.0;
   const float PRESEL_CHI2_NDOF  = 5.0;
   const int   PRESEL_N_HITS_PIX = 3;
   const int   PRESEL_N_HITS     = 7;
   const float PRESEL_Z0_WRTVTX  = 5.0;
   const float PRESEL_D0_WRTVTX  = 2.0;
   
   TVector3 p3;
   float d0, d0err, z0, chi2, ndof;
   int  n_hits_bl, n_hits_pix, n_hits_sct;
   get_fittrk_trk(prefix,itMyData,p3,d0,d0err,z0,chi2,ndof,n_hits_bl,n_hits_pix,n_hits_sct);

   // pt
   if( p3.Pt() < PRESEL_PT ) return false;

   // chi2/ndof
   if( fabs(ndof) < 1e-2 ) return false;
   if( (chi2/ndof) > PRESEL_CHI2_NDOF ) return false;
   
   // # of hits pixel
   if( n_hits_pix < PRESEL_N_HITS_PIX ) return false;

   // # of total hits
   int n_hits = n_hits_pix + n_hits_sct;
   if( n_hits < PRESEL_N_HITS ) return false;

   // z0
   float z0_wrtVtx = 0;
   itMyData->getDetail(prefix+"_z0_wrtVtx", z0_wrtVtx);
   if( fabs(z0_wrtVtx) > PRESEL_Z0_WRTVTX ) return false;

   // d0
   float d0_wrtVtx = 0;
   itMyData->getDetail(prefix+"_d0_wrtVtx", d0_wrtVtx);
   if( fabs(d0_wrtVtx) > PRESEL_D0_WRTVTX ) return false;
   
   // ok
   return true;
}

// ------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------

void MyPackageAlg::fittrk_calc_isolation(const std::string& prefix, const xAOD::TrigComposite* itMyData,
					 std::map<unsigned int, std::map<std::string, float> >& map_ftrk_varf,
					 std::map<std::string, float>& map_isolation)
{
   const float ISOL_CALC_Z0_DIFF_CUT          = 2.5;
   const float ISOL_CALC_DR_CUT_TO_AVOID_ZERO = 0.015;

   //
   float z0;
   (itMyData)->getDetail(prefix+"_z0", z0);
   TVector3 p3;
   get_fittrk_p3(prefix,itMyData,p3);

   //
   map_isolation.clear();
   map_isolation.insert(std::make_pair("iso1_dr01",0));
   map_isolation.insert(std::make_pair("iso1_dr02",0));
   map_isolation.insert(std::make_pair("iso1_dr04",0));
   map_isolation.insert(std::make_pair("iso2_dr01",0));
   map_isolation.insert(std::make_pair("iso2_dr02",0));
   map_isolation.insert(std::make_pair("iso2_dr04",0));
   map_isolation.insert(std::make_pair("iso3_dr01",0));
   map_isolation.insert(std::make_pair("iso3_dr02",0));
   map_isolation.insert(std::make_pair("iso3_dr04",0));

   //
   for(auto it=map_ftrk_varf.begin(); it!=map_ftrk_varf.end(); it++) {

      float trk_z0 = it->second["z0"];
      fill_h1_each("isol_calc_z0_diff", trk_z0 - z0);
      if( fabs(trk_z0 - z0) > ISOL_CALC_Z0_DIFF_CUT ) continue;

      float trk_pt  = it->second["pt"];
      float trk_eta = it->second["eta"];
      float trk_phi = it->second["phi"];
      TVector3 trk_p3;
      trk_p3.SetPtEtaPhi(trk_pt, trk_eta, trk_phi);
      float dr = p3.DrEtaPhi(trk_p3);
      fill_h1_each("isol_calc_dr", dr);
      
      if( dr > ISOL_CALC_DR_CUT_TO_AVOID_ZERO && dr<0.1 && trk_pt > 1.0 ) map_isolation["iso1_dr01"] += trk_pt;
      if( dr > ISOL_CALC_DR_CUT_TO_AVOID_ZERO && dr<0.2 && trk_pt > 1.0 ) map_isolation["iso1_dr02"] += trk_pt;
      if( dr > ISOL_CALC_DR_CUT_TO_AVOID_ZERO && dr<0.4 && trk_pt > 1.0 ) map_isolation["iso1_dr04"] += trk_pt;

      if( dr > ISOL_CALC_DR_CUT_TO_AVOID_ZERO && dr<0.1 && trk_pt > 2.0 ) map_isolation["iso2_dr01"] += trk_pt;
      if( dr > ISOL_CALC_DR_CUT_TO_AVOID_ZERO && dr<0.2 && trk_pt > 2.0 ) map_isolation["iso2_dr02"] += trk_pt;
      if( dr > ISOL_CALC_DR_CUT_TO_AVOID_ZERO && dr<0.4 && trk_pt > 2.0 ) map_isolation["iso2_dr04"] += trk_pt;

      if( dr > ISOL_CALC_DR_CUT_TO_AVOID_ZERO && dr<0.1 && trk_pt > 3.0 ) map_isolation["iso3_dr01"] += trk_pt;
      if( dr > ISOL_CALC_DR_CUT_TO_AVOID_ZERO && dr<0.2 && trk_pt > 3.0 ) map_isolation["iso3_dr02"] += trk_pt;
      if( dr > ISOL_CALC_DR_CUT_TO_AVOID_ZERO && dr<0.4 && trk_pt > 3.0 ) map_isolation["iso3_dr04"] += trk_pt;
   }
}

// ================================================================================================
// ================================================================================================

bool MyPackageAlg::presel_fittrk(const std::string& prefix, const xAOD::TrigComposite* itMyData)
{
   const float PRESEL_Z0_WRTVTX = 50.0;
   const float PRESEL_D0_WRTVTX =  5.0;
   const int   PRESEL_N_DOUBLEGOOD_BR_LAYERS_PIX = 3;
   //const int   PRESEL_N_DOUBLEGOOD_BR_LAYERS_SCT = 0;
   const float PRESEL_CHI2_NDOF_PIX = 5.0;

   // z0
   float z0 = 0;
   itMyData->getDetail(prefix+"_z0_wrtVtx", z0);
   if( fabs(z0) > PRESEL_Z0_WRTVTX ) return false;

   // d0
   float d0 = 0;
   itMyData->getDetail(prefix+"_d0_wrtVtx", d0);
   if( fabs(d0) > PRESEL_D0_WRTVTX ) return false;
   
   // nr of br layers double good
   std::vector<int>   v_nhits;
   std::vector<float> v_chi2;
   std::vector<int>   v_ndof;
   std::vector<int>   v_nhits_good;
   get_fittrk_brhits(prefix, itMyData, v_nhits, v_chi2, v_ndof, v_nhits_good);
   int n_doublegood_brlayers_pix = 0;
   int n_doublegood_brlayers_sct = 0;
   float chi2_pix = 0; int ndof_pix = 0;
   for(unsigned int j=0; j<8; j++) {
      if( j<= 3 ) { // PIX
	 if( v_nhits_good[j] >= 1 ) n_doublegood_brlayers_pix++;
	 chi2_pix += v_chi2[j];
	 ndof_pix += v_ndof[j];
      }
      else { // SCT
	 if( v_nhits_good[j] >= 2 ) n_doublegood_brlayers_sct++; 
      }
   }
   if( n_doublegood_brlayers_pix < PRESEL_N_DOUBLEGOOD_BR_LAYERS_PIX ) return false;
   //if( n_doublegood_brlayers_sct > PRESEL_N_DOUBLEGOOD_BR_LAYERS_SCT ) return false;

   // chi2/ndof PIX
   if( ndof_pix==0 ) return false;
   if( (chi2_pix/ndof_pix) > PRESEL_CHI2_NDOF_PIX ) return false;

   // ok
   return true;
}

// ================================================================================================
// ================================================================================================

bool MyPackageAlg::is_seed_allpix(const xAOD::TrigComposite* itMyData)
{
   int n_pix = get_seed_npix(itMyData);
   return (n_pix == 3);
}

bool MyPackageAlg::is_seed_allpix_allbarrel(const xAOD::TrigComposite* itMyData)
{
   if( ! is_seed_allpix(itMyData) ) return false;

   const float PIXEL_BARREL_Z = 410;

   float s1_z = 0; float s2_z = 0; float s3_z = 0;
   (itMyData)->getDetail("s1_z", s1_z);
   (itMyData)->getDetail("s2_z", s2_z);
   (itMyData)->getDetail("s3_z", s3_z);
   if( fabs(s1_z) > PIXEL_BARREL_Z ) return false;
   if( fabs(s2_z) > PIXEL_BARREL_Z ) return false;
   if( fabs(s3_z) > PIXEL_BARREL_Z ) return false;

   return true;
}

int MyPackageAlg::get_seed_npix(const xAOD::TrigComposite* itMyData)
{
   int s1_pix = -1; int s2_pix = -2; int s3_pix = -1;
   itMyData->getDetail("s1_pix", s1_pix); itMyData->getDetail("s2_pix", s2_pix); itMyData->getDetail("s3_pix", s3_pix);
   int n_pix=0;
   if( s1_pix==1 ) {n_pix++;}; if( s2_pix==1 ) {n_pix++;}; if( s3_pix==1 ) {n_pix++;};
   return n_pix;
}

// ================================================================================================
// ================================================================================================

TVector3 MyPackageAlg::get_track_p3(const xAOD::TrigComposite* itMyData)
{
   float trk_pt = 0; float trk_theta = 0; float trk_phi = 0;
   (itMyData)->getDetail("trk_pt", trk_pt); 
   (itMyData)->getDetail("trk_theta", trk_theta); 
   (itMyData)->getDetail("trk_phi", trk_phi);
   TVector3 p3_trk(1,1,1);
   trk_pt /= 1000.;
   p3_trk.SetPtThetaPhi(trk_pt,trk_theta,trk_phi);
   return p3_trk;
}

void MyPackageAlg::get_track(const xAOD::TrigComposite* itMyData, TVector3& trk_p3, float& trk_d0, float& trk_z0)
{
   trk_p3 = get_track_p3(itMyData);
   (itMyData)->getDetail("trk_d0", trk_d0); 
   (itMyData)->getDetail("trk_z0", trk_z0);
}

void MyPackageAlg::get_track(const xAOD::TrigComposite* itMyData, TVector3& trk_p3, float& trk_d0, float& trk_z0,
			     float& trk_chi2, float& trk_ndof, int& trk_n_hits_bl, int& trk_n_hits_pix, int& trk_n_hits_sct)
{
   get_track(itMyData, trk_p3, trk_d0, trk_z0);

   (itMyData)->getDetail("trk_chi2",trk_chi2);
   (itMyData)->getDetail("trk_ndof",trk_ndof);
   (itMyData)->getDetail("trk_n_hits_bl", trk_n_hits_bl);
   (itMyData)->getDetail("trk_n_hits_pix",trk_n_hits_pix);
   (itMyData)->getDetail("trk_n_hits_sct",trk_n_hits_sct);
}

// ================================================================================================
// ================================================================================================

void MyPackageAlg::get_fittrk_p3(const std::string& prefix, const xAOD::TrigComposite* itMyData, TVector3& trk_p3)
{
   float trk_pt=0; float trk_theta=0; float trk_phi=0;
   (itMyData)->getDetail(prefix+"_pt",    trk_pt);
   (itMyData)->getDetail(prefix+"_theta", trk_theta);
   (itMyData)->getDetail(prefix+"_phi",   trk_phi);
   trk_pt /= 1000.;
   trk_p3.SetPtThetaPhi(trk_pt,trk_theta,trk_phi);
}

// ------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------

void MyPackageAlg::get_fittrk_trk(const std::string& prefix, const xAOD::TrigComposite* itMyData, TVector3& trk_p3, float& trk_d0, float& trk_d0err, float& trk_z0,
				  float& trk_chi2, float& trk_ndof, int& trk_n_hits_bl, int& trk_n_hits_pix, int& trk_n_hits_sct)
{
   get_fittrk_p3(prefix,itMyData,trk_p3);
   (itMyData)->getDetail(prefix+"_d0",        trk_d0);
   (itMyData)->getDetail(prefix+"_d0err",     trk_d0err);
   (itMyData)->getDetail(prefix+"_z0",        trk_z0);
   (itMyData)->getDetail(prefix+"_chi2",      trk_chi2);
   (itMyData)->getDetail(prefix+"_ndof",      trk_ndof);
   (itMyData)->getDetail(prefix+"_n_hits_bl", trk_n_hits_bl);
   (itMyData)->getDetail(prefix+"_n_hits_pix",trk_n_hits_pix);
   (itMyData)->getDetail(prefix+"_n_hits_sct",trk_n_hits_sct);
}

// ------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------

void MyPackageAlg::get_fittrk(const std::string& prefix, const xAOD::TrigComposite* itMyData,
			      std::map<std::string, float>& varf, std::map<std::string, int>& vari)
{
   TVector3 p3;
   float d0, d0err, z0, chi2, ndof;
   int  n_hits_bl, n_hits_pix, n_hits_sct;
   get_fittrk_trk(prefix,itMyData,p3,d0,d0err,z0,chi2,ndof,n_hits_bl,n_hits_pix,n_hits_sct);
   varf.insert(std::make_pair("pt",   p3.Pt()));
   varf.insert(std::make_pair("eta",  p3.Eta()));
   varf.insert(std::make_pair("phi",  p3.Phi()));
   varf.insert(std::make_pair("d0",   d0));
   varf.insert(std::make_pair("d0err",d0err));
   varf.insert(std::make_pair("z0",   z0));
   varf.insert(std::make_pair("chi2", chi2));
   vari.insert(std::make_pair("ndof", ndof));
   vari.insert(std::make_pair("n_hits_bl", n_hits_bl));
   vari.insert(std::make_pair("n_hits_pix",n_hits_pix));
   vari.insert(std::make_pair("n_hits_sct",n_hits_sct));

   if( prefix == "goodTrk" ) return;

   float d0_wrtVtx=0; float z0_wrtVtx=0; float d0err_wrtVtx=0; float z0err_wrtVtx=0;
   std::vector<float> v_vtx_z;
   get_fittrk_vtx(prefix, itMyData, d0_wrtVtx, z0_wrtVtx, d0err_wrtVtx, z0err_wrtVtx, v_vtx_z);
   varf.insert(std::make_pair("d0_wrtVtx", d0_wrtVtx));
   varf.insert(std::make_pair("z0_wrtVtx", z0_wrtVtx));

   int is_fail;
   itMyData->getDetail(prefix+"_is_fail", is_fail);
   vari.insert(std::make_pair("is_fail",  is_fail));

   // refit info
   get_fittrk_trk(prefix+"_refit",itMyData,p3,d0,d0err,z0,chi2,ndof,n_hits_bl,n_hits_pix,n_hits_sct);
   varf.insert(std::make_pair("refit_pt",   p3.Pt()));
   varf.insert(std::make_pair("refit_eta",  p3.Eta()));
   varf.insert(std::make_pair("refit_phi",  p3.Phi()));
   varf.insert(std::make_pair("refit_d0",   d0));
   varf.insert(std::make_pair("refit_d0err",d0err));
   varf.insert(std::make_pair("refit_z0",   z0));
   varf.insert(std::make_pair("refit_chi2", chi2));
   vari.insert(std::make_pair("refit_ndof", ndof));
   vari.insert(std::make_pair("refit_n_hits_bl", n_hits_bl));
   vari.insert(std::make_pair("refit_n_hits_pix",n_hits_pix));
   vari.insert(std::make_pair("refit_n_hits_sct",n_hits_sct));
   v_vtx_z.clear();
   get_fittrk_vtx(prefix+"_refit", itMyData, d0_wrtVtx, z0_wrtVtx, d0err_wrtVtx, z0err_wrtVtx, v_vtx_z);
   varf.insert(std::make_pair("refit_d0_wrtVtx", d0_wrtVtx));
   varf.insert(std::make_pair("refit_z0_wrtVtx", z0_wrtVtx));
}

// ------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------

void MyPackageAlg::get_fittrk_vtx(const std::string& prefix, const xAOD::TrigComposite* itMyData,
				  float& d0_wrtVtx, float& z0_wrtVtx, float& d0err_wrtVtx, float& z0err_wrtVtx, std::vector<float>& v_vtx_z)
{
   (itMyData)->getDetail(prefix+"_d0_wrtVtx",    d0_wrtVtx);
   (itMyData)->getDetail(prefix+"_z0_wrtVtx",    z0_wrtVtx);
   (itMyData)->getDetail(prefix+"_d0err_wrtVtx", d0err_wrtVtx);
   (itMyData)->getDetail(prefix+"_z0err_wrtVtx", z0err_wrtVtx);
   float vtx_z1, vtx_z2, vtx_z3;
   (itMyData)->getDetail(prefix+"_vtx_z1", vtx_z1);
   (itMyData)->getDetail(prefix+"_vtx_z2", vtx_z2);
   (itMyData)->getDetail(prefix+"_vtx_z3", vtx_z3);
   v_vtx_z.push_back(vtx_z1); v_vtx_z.push_back(vtx_z2); v_vtx_z.push_back(vtx_z3);
}

// ------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------

void MyPackageAlg::get_fittrk_brhits(const std::string& prefix, const xAOD::TrigComposite* itMyData,
				     std::vector<int>& v_nhits, std::vector<float>& v_chi2, std::vector<int>& v_ndof, std::vector<int>& v_nhits_good)
{
   int n_ibl, n_pix1, n_pix2, n_pix3;
   int n_sct1, n_sct2, n_sct3, n_sct4;
   (itMyData)->getDetail(prefix+"_n_brhits_ibl", n_ibl);
   (itMyData)->getDetail(prefix+"_n_brhits_pix1",n_pix1);
   (itMyData)->getDetail(prefix+"_n_brhits_pix2",n_pix2);
   (itMyData)->getDetail(prefix+"_n_brhits_pix3",n_pix3);
   (itMyData)->getDetail(prefix+"_n_brhits_sct1",n_sct1);
   (itMyData)->getDetail(prefix+"_n_brhits_sct2",n_sct2);
   (itMyData)->getDetail(prefix+"_n_brhits_sct3",n_sct3);
   (itMyData)->getDetail(prefix+"_n_brhits_sct4",n_sct4);
   v_nhits.push_back(n_ibl);  v_nhits.push_back(n_pix1); v_nhits.push_back(n_pix2); v_nhits.push_back(n_pix3);
   v_nhits.push_back(n_sct1); v_nhits.push_back(n_sct2); v_nhits.push_back(n_sct3); v_nhits.push_back(n_sct4);

   float chi2_ibl, chi2_pix1, chi2_pix2, chi2_pix3;
   float chi2_sct1, chi2_sct2, chi2_sct3, chi2_sct4;
   (itMyData)->getDetail(prefix+"_chi2sum_br_ibl", chi2_ibl);
   (itMyData)->getDetail(prefix+"_chi2sum_br_pix1",chi2_pix1);
   (itMyData)->getDetail(prefix+"_chi2sum_br_pix2",chi2_pix2);
   (itMyData)->getDetail(prefix+"_chi2sum_br_pix3",chi2_pix3);
   (itMyData)->getDetail(prefix+"_chi2sum_br_sct1",chi2_sct1);
   (itMyData)->getDetail(prefix+"_chi2sum_br_sct2",chi2_sct2);
   (itMyData)->getDetail(prefix+"_chi2sum_br_sct3",chi2_sct3);
   (itMyData)->getDetail(prefix+"_chi2sum_br_sct4",chi2_sct4);
   v_chi2.push_back(chi2_ibl);  v_chi2.push_back(chi2_pix1); v_chi2.push_back(chi2_pix2); v_chi2.push_back(chi2_pix3);
   v_chi2.push_back(chi2_sct1); v_chi2.push_back(chi2_sct2); v_chi2.push_back(chi2_sct3); v_chi2.push_back(chi2_sct4);

   int ndof_ibl, ndof_pix1, ndof_pix2, ndof_pix3;
   int ndof_sct1, ndof_sct2, ndof_sct3, ndof_sct4;
   (itMyData)->getDetail(prefix+"_ndofsum_br_ibl", ndof_ibl);
   (itMyData)->getDetail(prefix+"_ndofsum_br_pix1",ndof_pix1);
   (itMyData)->getDetail(prefix+"_ndofsum_br_pix2",ndof_pix2);
   (itMyData)->getDetail(prefix+"_ndofsum_br_pix3",ndof_pix3);
   (itMyData)->getDetail(prefix+"_ndofsum_br_sct1",ndof_sct1);
   (itMyData)->getDetail(prefix+"_ndofsum_br_sct2",ndof_sct2);
   (itMyData)->getDetail(prefix+"_ndofsum_br_sct3",ndof_sct3);
   (itMyData)->getDetail(prefix+"_ndofsum_br_sct4",ndof_sct4);
   v_ndof.push_back(ndof_ibl);  v_ndof.push_back(ndof_pix1); v_ndof.push_back(ndof_pix2); v_ndof.push_back(ndof_pix3);
   v_ndof.push_back(ndof_sct1); v_ndof.push_back(ndof_sct2); v_ndof.push_back(ndof_sct3); v_ndof.push_back(ndof_sct4);

   int n_good_ibl, n_good_pix1, n_good_pix2, n_good_pix3;
   int n_good_sct1, n_good_sct2, n_good_sct3, n_good_sct4;
   (itMyData)->getDetail(prefix+"_n_brhits_good_ibl", n_good_ibl);
   (itMyData)->getDetail(prefix+"_n_brhits_good_pix1",n_good_pix1);
   (itMyData)->getDetail(prefix+"_n_brhits_good_pix2",n_good_pix2);
   (itMyData)->getDetail(prefix+"_n_brhits_good_pix3",n_good_pix3);
   (itMyData)->getDetail(prefix+"_n_brhits_good_sct1",n_good_sct1);
   (itMyData)->getDetail(prefix+"_n_brhits_good_sct2",n_good_sct2);
   (itMyData)->getDetail(prefix+"_n_brhits_good_sct3",n_good_sct3);
   (itMyData)->getDetail(prefix+"_n_brhits_good_sct4",n_good_sct4);
   v_nhits_good.push_back(n_good_ibl);  v_nhits_good.push_back(n_good_pix1); v_nhits_good.push_back(n_good_pix2); v_nhits_good.push_back(n_good_pix3);
   v_nhits_good.push_back(n_good_sct1); v_nhits_good.push_back(n_good_sct2); v_nhits_good.push_back(n_good_sct3); v_nhits_good.push_back(n_good_sct4);

   /*
   std::cout << fname << "...trk br chi2 ibl  / pix1 / pix2 / pix3 = " << chi2_ibl  << " / " << chi2_pix1 << " / " << chi2_pix2 << " / " << chi2_pix3 << std::endl; 
   std::cout << fname << "...trk br ndof ibl  / pix1 / pix2 / pix3 = " << ndof_ibl  << " / " << ndof_pix1 << " / " << ndof_pix2 << " / " << ndof_pix3 << std::endl; 
   std::cout << fname << "...trk br chi2 sct1 / sct2 / sct3 / sct4 = " << chi2_sct1 << " / " << chi2_sct2 << " / " << chi2_sct3 << " / " << chi2_sct4 << std::endl; 
   std::cout << fname << "...trk br ndof sct1 / sct2 / sct3 / sct4 = " << ndof_sct1 << " / " << ndof_sct2 << " / " << ndof_sct3 << " / " << ndof_sct4 << std::endl; 
   std::cout << fname << "...trk br good nhits ibl  / pix1 / pix2 / pix3 = " << n_good_ibl  << " / " << n_good_pix1 << " / " << n_good_pix2 << " / " << n_good_pix3 << std::endl; 
   std::cout << fname << "...trk br good nhits sct1 / sct2 / sct3 / sct4 = " << n_good_sct1 << " / " << n_good_sct2 << " / " << n_good_sct3 << " / " << n_good_sct4 << std::endl; 
   */
}

// ------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------

void MyPackageAlg::print_fittrk(const std::string& header, std::map<std::string, float>& varf, std::map<std::string, int>& vari)
{
   std::cout << header << "trk pt / eta / phi / is_fail = " << varf["pt"]
	     << " / " << varf["eta"] << " / " << varf["phi"] << " / " << vari["is_fail"] << std::endl;
   std::cout << header << "trk d0_wrtVtx / z0_wrtVtx / chi2 / ndof = " << varf["d0_wrtVtx"] << " / " << varf["z0_wrtVtx"]
	     << " / " << varf["chi2"] << " / " << vari["ndof"] << std::endl;
   std::cout << header << "trk nbl / npix / nsct = " << vari["n_hits_bl"] << " / " << vari["n_hits_pix"] << " / " << vari["n_hits_sct"] << std::endl;
   std::cout << header << "REFIT pt / eta / phi = " << varf["refit_pt"]
	     << " / " << varf["refit_eta"] << " / " << varf["refit_phi"] << std::endl;
   std::cout << header << "REFIT chi2 / ndof = " << varf["refit_chi2"] << " / " << vari["refit_ndof"] << std::endl;
}

// ================================================================================================
// ================================================================================================

void MyPackageAlg::remove_duplicates(std::map<unsigned int, std::map<std::string, float> >& map_varf,
				     std::map<unsigned int, std::map<std::string, int> >& map_vari)
{
   const std::string fname = "remove_duplicates> ";

   std::vector<unsigned int>    v_idx;
   std::map<unsigned int, bool> map_idx_left;
   for(auto it=map_varf.begin(); it!=map_varf.end(); it++) {
      v_idx.push_back(it->first);
      map_idx_left.insert(std::make_pair(it->first,true));
   }
   
   for(int i=0; i<(int)v_idx.size()-1; i++) {
      for(int j=i+1; j<(int)v_idx.size(); j++) {
	 unsigned int i_idx=v_idx[i];
	 unsigned int j_idx=v_idx[j];
	 if( map_idx_left.find(i_idx) != map_idx_left.end() && map_idx_left.find(j_idx) != map_idx_left.end() && i_idx != j_idx ) { // check duplication
	    bool duplicated = true;
	    //
	    TVector3 p3_i, p3_j;
	    p3_i.SetPtEtaPhi((map_varf[i_idx])["pt"], (map_varf[i_idx])["eta"], (map_varf[i_idx])["phi"]);
	    p3_j.SetPtEtaPhi((map_varf[j_idx])["pt"], (map_varf[j_idx])["eta"], (map_varf[j_idx])["phi"]);
	    float dr = p3_i.DrEtaPhi(p3_j);
	    if( dr < 0.1 && m_is_debug_mode ) {
	       std::cout << fname << "dr = " << dr << ": pt 1/2 = " << (map_varf[i_idx])["pt"] << " / " << (map_varf[j_idx])["pt"] << std::endl;
	    }
	    //
	    if( fabs( (map_varf[i_idx])["pt"]-(map_varf[j_idx])["pt"] )  > 1e-2 ) duplicated = false;
	    if( dr > 1e-2 ) duplicated = false;
	    if( fabs( (map_varf[i_idx])["d0"]-(map_varf[j_idx])["d0"] )  > 1e-2 ) duplicated = false;
	    if( fabs( (map_varf[i_idx])["z0"]-(map_varf[j_idx])["z0"] )  > 1e-2 ) duplicated = false;
	    if( fabs( (map_varf[i_idx])["chi2"]-(map_varf[j_idx])["chi2"] )  > 1e-2 ) duplicated = false;
	    if( fabs( (map_varf[i_idx])["ndof"]-(map_varf[j_idx])["ndof"] )  > 1e-2 ) duplicated = false;
	    if( fabs( (map_vari[i_idx])["n_hits_bl"]-(map_vari[j_idx])["n_hits_bl"] )  > 1e-2 ) duplicated = false;
	    if( fabs( (map_vari[i_idx])["n_hits_pix"]-(map_vari[j_idx])["n_hits_pix"] )  > 1e-2 ) duplicated = false;
	    if( fabs( (map_vari[i_idx])["n_hits_sct"]-(map_vari[j_idx])["n_hits_sct"] )  > 1e-2 ) duplicated = false;
	    if( duplicated ) {
	       map_idx_left.erase(j_idx);
	    }
	 }
      }
   }

   for(unsigned int i=0; i<v_idx.size(); i++) {
      unsigned int idx = v_idx[i];
      if( map_idx_left.find(idx) == map_idx_left.end() ) {
	 map_varf.erase(idx); map_vari.erase(idx);
      }
   }
}

// ================================================================================================
// ================================================================================================

void MyPackageAlg::calc_jet_isolation(const TVector3& p3_ref,
				      const xAOD::JetContainer* jets,
				      float& ptsum_dr02, float& ptsum_dr04, float& ptsum_dr10)
{
   ptsum_dr02=0; ptsum_dr04=0; ptsum_dr10=0;
   std::vector<TVector3> v_p3_jets;
   for (auto jet : *jets) {
      if (!m_jetCleaning->keep (*jet)) continue;
      float jet_pt  = jet->pt() / 1000.0;
      float jet_eta = jet->eta();
      float jet_phi = jet->phi();
      TVector3 p3_jet(1,1,1); p3_jet.SetPtEtaPhi(jet_pt,jet_eta,jet_phi);
      v_p3_jets.push_back(p3_jet);
   }
   calc_isolation(p3_ref,v_p3_jets,ptsum_dr02,ptsum_dr04,ptsum_dr10);
}

// ------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------

void MyPackageAlg::calc_isolation(const TVector3& p3_ref,
				  const std::vector<TVector3>& v_p3,
				  float& ptsum_dr02, float& ptsum_dr04, float& ptsum_dr10)
{
   ptsum_dr02=0; ptsum_dr04=0; ptsum_dr10=0;
   for(unsigned int i=0; i<v_p3.size(); i++) {
      TVector3 p3 = v_p3[i];
      float pt = p3.Pt();
      float dr = p3_ref.DrEtaPhi(p3);
      if( dr<0.2 ) { ptsum_dr02 += pt; }
      if( dr<0.4 ) { ptsum_dr04 += pt; }
      if( dr<1.0 ) { ptsum_dr10 += pt; }
   }
}

// ------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------

void MyPackageAlg::calc_dr_min(const TVector3& p3, const std::vector<TVector3>& v_ref_p3, float& dr_min, int& idx_min) 
{
   std::cout << " truth p3 size = " << v_ref_p3.size() << "  ->" << "calc_dr_min start" << std::endl;
   dr_min=9999; idx_min=-1;
   for(unsigned int i=0; i<v_ref_p3.size(); i++) {
      TVector3 ref_p3 = v_ref_p3[i];
      float dr = p3.DrEtaPhi(ref_p3);
      if( dr<dr_min ) {
	 dr_min  = dr;
	 idx_min = i; 
      }
   }
   std::cout << " dr_to_chg = " << dr_min<< "  ->" << "calc_dr_min finish" << std::endl;
}

// ================================================================================================
// ================================================================================================

StatusCode MyPackageAlg::getTruthChargino(const xAOD::TruthEventContainer* xTruthEventContainer,
					  std::vector<TVector3>& v_chg_p3, std::vector<TVector3>& v_chg_prod, std::vector<TVector3>& v_chg_decay)
{
   const std::string fname = "getTruthChargino> ";

   v_chg_p3.clear(); v_chg_prod.clear(); v_chg_decay.clear(); 
   
   xAOD::TruthEventContainer::const_iterator itr;

   for (itr = xTruthEventContainer->begin(); itr!=xTruthEventContainer->end(); ++itr) {
      int nPart = (*itr)->truthParticleLinks().size();
      for(int iPart=0; iPart<nPart; iPart++) {
	 const xAOD::TruthParticle* particle = (*itr)->truthParticle(iPart);
	 if( particle == 0 ) continue;
	 int pdgId = particle->pdgId();
	 if( abs(pdgId) == 1000024 ) {
	    bool hasDecayVtx = particle->hasDecayVtx();
	    bool hasProdVtx = particle->hasProdVtx();
	    if( hasDecayVtx && hasProdVtx ) {
	       const xAOD::TruthVertex* prodVtx  = particle->prodVtx();
	       const xAOD::TruthVertex* decayVtx = particle->decayVtx();
	       float decayX = decayVtx->x(); float decayY = decayVtx->y(); float decayZ = decayVtx->z();
	       float prodX  = prodVtx->x();  float prodY  = prodVtx->y();  float prodZ = prodVtx->z();
	       float px = particle->px() / 1000.;
	       float py = particle->py() / 1000.;
	       float pz = particle->pz() / 1000.;
	       TVector3 chg_p3(px,py,pz);
	       if( fabs(prodX-decayX)>0.01 && fabs(prodY-decayY)>0.01 && fabs(prodZ-decayZ)>0.01 ) {
		  if( m_is_debug_mode ) {
		     std::cout << fname << "++ decayed: iPart / pdgId / hasProdVtx / hasDecayVtx = " << iPart << " / " << pdgId << " / " << hasProdVtx << " / " << hasDecayVtx << std::endl;
		     std::cout << fname << "... pt / eta / phi = " << chg_p3.Pt() << " / " << chg_p3.Eta() << " / " << chg_p3.Phi() << std::endl;
		     std::cout << fname << "... prod  vertex x / y / z  = " << prodX  << " / " << prodY  << " / " << prodZ << std::endl;
		     std::cout << fname << "... decay vertex x / y / z  = " << decayX << " / " << decayY << " / " << decayZ << std::endl;
		  }
		  v_chg_p3.push_back(chg_p3);
		  TVector3 chg_prod(prodX,prodY,prodZ);
		  TVector3 chg_decay(decayX,decayY,decayZ);
		  v_chg_prod.push_back(chg_prod); v_chg_decay.push_back(chg_decay);
		  for(unsigned int i_parent=0; i_parent < particle->nParents(); i_parent++) {
		     const xAOD::TruthParticle* parent = particle->parent(i_parent);
		     if( parent != 0 ) {
			float parent_px = parent->px() / 1000.;
			float parent_py = parent->py() / 1000.;
			float parent_pz = parent->pz() / 1000.;
			TVector3 parent_p3(parent_px,parent_py,parent_pz);
			if( m_is_debug_mode ) {
			   std::cout << fname << "... parent pt / eta / phi = " << parent_p3.Pt() << " / " << parent_p3.Eta() << " / " << parent_p3.Phi() << std::endl;
			}
		     }
		  }
		  if( v_chg_p3.size() >= 2 ) break;
	       }
	    }
	 }
      }
   }

   //
   return StatusCode::SUCCESS;
}
	 
// ================================================================================================
// ================================================================================================

StatusCode MyPackageAlg::initialize()
{
   ATH_MSG_INFO ("Initializing " << name() << "...");

   // GRL and PRW
   std::vector<std::string> vecStringGRL = {"/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data17_13TeV/20180619/data17_13TeV.periodAllYear_DetStatus-v99-pro22-01_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml"};

   std::vector<std::string> listOfLumicalcFiles = {"/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-010.root"};
   
   std::vector<std::string> vecStringPRW = {"prw.root"};

   // GRL selection tool
   CHECK (m_grlTool.setProperty("GoodRunsListVec", vecStringGRL));
   CHECK (m_grlTool.setProperty("PassThrough", false)); // if true (default) will ignore result of GRL and will just pass all events
   CHECK (m_grlTool.initialize());

   // Pileup weighting tool
   CHECK (m_pileupTool.setProperty("ConfigFiles",vecStringPRW));
   CHECK (m_pileupTool.setProperty("LumiCalcFiles",listOfLumicalcFiles));
   CHECK (m_pileupTool.initialize());
   
   // trig decision tool
   m_tdt.setTypeAndName("Trig::TrigDecisionTool/TrigDecisionTool");
   CHECK (m_tdt.initialize());

   // muon selector tool
   CHECK (m_muonSelection.setProperty("MaxEta", 2.5)); 
   CHECK (m_muonSelection.setProperty("MuQuality", xAOD::Muon::Medium));
   CHECK (m_muonSelection.initialize());

   // jet cleaning tool
   CHECK (m_jetCleaning.setProperty("CutLevel", "LooseBad"));
   CHECK (m_jetCleaning.setProperty("DoUgly", false));
   CHECK (m_jetCleaning.initialize());

   // jet energy resolution
   CHECK (m_JERTool.initialize());

   // We will create a histogram and a ttree and register them to the histsvc
   // Remember to configure the histsvc stream in the joboptions
   // m_myTree = new TTree("myTree","myTree");
   // CHECK( histSvc()->regTree("/MYSTREAM/SubDirectory/myTree", m_myTree) ); //registers tree to output stream inside a sub-directory

   // TMVA reader
   if( m_is_bdt_validation ) {
      m_tmva_pix4l_sct0_reader  = new TMVA::Reader( "!Color:!Silent" );
      m_tmva_pix4l_sct1p_reader = new TMVA::Reader( "!Color:!Silent" );
      m_tmva_pix3l_sct0_reader  = new TMVA::Reader( "!Color:!Silent" );
      m_tmva_pix3l_sct1p_reader = new TMVA::Reader( "!Color:!Silent" );
      // --- declare variables to the reader
      // pix4l_sct0
      m_tmva_pix4l_sct0_reader->AddVariable("pt",             &m_tmva_pix4l_sct0_pt);
      m_tmva_pix4l_sct0_reader->AddVariable("z0",             &m_tmva_pix4l_sct0_z0);
      m_tmva_pix4l_sct0_reader->AddVariable("d0",             &m_tmva_pix4l_sct0_d0);
      m_tmva_pix4l_sct0_reader->AddVariable("trkiso3_dr01",   &m_tmva_pix4l_sct0_trkiso3_dr01);
      m_tmva_pix4l_sct0_reader->AddVariable("trkiso3_dr0201", &m_tmva_pix4l_sct0_trkiso3_dr0201);
      m_tmva_pix4l_sct0_reader->AddVariable("chi2ndof",       &m_tmva_pix4l_sct0_chi2ndof);
      m_tmva_pix4l_sct0_reader->AddVariable("chi2ndof_pix",   &m_tmva_pix4l_sct0_chi2ndof_pix);
      m_tmva_pix4l_sct0_reader->AddVariable("refit_pt",      &m_tmva_pix4l_sct0_refit_pt);
      m_tmva_pix4l_sct0_reader->AddVariable("n_pix",         &m_tmva_pix4l_sct0_n_pix);
      m_tmva_pix4l_sct0_reader->AddVariable("refit_ptratio", &m_tmva_pix4l_sct0_refit_pt_ratio);
      m_tmva_pix4l_sct0_reader->AddVariable("refit_chi2ndof", &m_tmva_pix4l_sct0_refit_chi2ndof);
      m_tmva_pix4l_sct0_reader->AddVariable("n_bl",         &m_tmva_pix4l_sct0_n_bl);
      // pix4l_sct1p
      m_tmva_pix4l_sct1p_reader->AddVariable("pt",            &m_tmva_pix4l_sct1p_pt);
      //m_tmva_pix4l_sct1p_reader->AddVariable("z0",            &m_tmva_pix4l_sct1p_z0);
      //m_tmva_pix4l_sct1p_reader->AddVariable("d0",            &m_tmva_pix4l_sct1p_d0);
      m_tmva_pix4l_sct1p_reader->AddVariable("refit_pt",      &m_tmva_pix4l_sct1p_refit_pt);
      m_tmva_pix4l_sct1p_reader->AddVariable("refit_z0",      &m_tmva_pix4l_sct1p_refit_z0);
      m_tmva_pix4l_sct1p_reader->AddVariable("refit_d0",      &m_tmva_pix4l_sct1p_refit_d0);
      m_tmva_pix4l_sct1p_reader->AddVariable("n_sct",         &m_tmva_pix4l_sct1p_n_sct);
      m_tmva_pix4l_sct1p_reader->AddVariable("refit_ptratio", &m_tmva_pix4l_sct1p_refit_pt_ratio);
      m_tmva_pix4l_sct1p_reader->AddVariable("refit_chi2ndof_ratio", &m_tmva_pix4l_sct1p_refit_chi2ndof_ratio);
      m_tmva_pix4l_sct1p_reader->AddVariable("trkiso3_dr01",  &m_tmva_pix4l_sct1p_trkiso3_dr01);
      m_tmva_pix4l_sct1p_reader->AddVariable("trkiso3_dr0201",&m_tmva_pix4l_sct1p_trkiso3_dr0201);
      m_tmva_pix4l_sct1p_reader->AddVariable("is_fail",       &m_tmva_pix4l_sct1p_is_fail);
      m_tmva_pix4l_sct1p_reader->AddVariable("chi2ndof_pix",  &m_tmva_pix4l_sct1p_chi2ndof_pix);
      m_tmva_pix4l_sct1p_reader->AddVariable("n_pix",         &m_tmva_pix4l_sct1p_n_pix);
      // pix3l_sct0
      m_tmva_pix3l_sct0_reader->AddVariable("pt",             &m_tmva_pix3l_sct0_pt);
      m_tmva_pix3l_sct0_reader->AddVariable("z0",             &m_tmva_pix3l_sct0_z0);
      m_tmva_pix3l_sct0_reader->AddVariable("d0",             &m_tmva_pix3l_sct0_d0);
      m_tmva_pix3l_sct0_reader->AddVariable("chi2ndof",       &m_tmva_pix3l_sct0_chi2ndof);
      m_tmva_pix3l_sct0_reader->AddVariable("chi2ndof_pix",   &m_tmva_pix3l_sct0_chi2ndof_pix);
      m_tmva_pix3l_sct0_reader->AddVariable("trkiso3_dr01",  &m_tmva_pix3l_sct0_trkiso3_dr01);
      m_tmva_pix3l_sct0_reader->AddVariable("trkiso3_dr0201",&m_tmva_pix3l_sct0_trkiso3_dr0201);
      m_tmva_pix3l_sct0_reader->AddVariable("refit_pt",      &m_tmva_pix3l_sct0_refit_pt);
      m_tmva_pix3l_sct0_reader->AddVariable("refit_z0",      &m_tmva_pix3l_sct0_refit_z0);
      m_tmva_pix3l_sct0_reader->AddVariable("refit_d0",      &m_tmva_pix3l_sct0_refit_d0);
      m_tmva_pix3l_sct0_reader->AddVariable("n_pix",         &m_tmva_pix3l_sct0_n_pix);
      m_tmva_pix3l_sct0_reader->AddVariable("n_bl",          &m_tmva_pix3l_sct0_n_bl);
      // pix3l_sct1p
      m_tmva_pix3l_sct1p_reader->AddVariable("pt",            &m_tmva_pix3l_sct1p_pt);
      m_tmva_pix3l_sct1p_reader->AddVariable("z0",            &m_tmva_pix3l_sct1p_z0);
      m_tmva_pix3l_sct1p_reader->AddVariable("d0",            &m_tmva_pix3l_sct1p_d0);
      m_tmva_pix3l_sct1p_reader->AddVariable("refit_pt",      &m_tmva_pix3l_sct1p_refit_pt);
      m_tmva_pix3l_sct1p_reader->AddVariable("refit_z0",      &m_tmva_pix3l_sct1p_refit_z0);
      m_tmva_pix3l_sct1p_reader->AddVariable("refit_d0",      &m_tmva_pix3l_sct1p_refit_d0);
      m_tmva_pix3l_sct1p_reader->AddVariable("n_pix",         &m_tmva_pix3l_sct1p_n_pix);
      m_tmva_pix3l_sct1p_reader->AddVariable("n_sct",         &m_tmva_pix3l_sct1p_n_sct);
      m_tmva_pix3l_sct1p_reader->AddVariable("refit_ptratio", &m_tmva_pix3l_sct1p_refit_pt_ratio);
      m_tmva_pix3l_sct1p_reader->AddVariable("is_fail",       &m_tmva_pix3l_sct1p_is_fail);
      m_tmva_pix3l_sct1p_reader->AddVariable("n_bl",          &m_tmva_pix3l_sct1p_n_bl);
      m_tmva_pix3l_sct1p_reader->AddVariable("chi2ndof",      &m_tmva_pix3l_sct1p_chi2ndof);
      m_tmva_pix3l_sct1p_reader->AddVariable("trkiso3_dr01",  &m_tmva_pix3l_sct1p_trkiso3_dr01);
      m_tmva_pix3l_sct1p_reader->AddVariable("trkiso3_dr0201",&m_tmva_pix3l_sct1p_trkiso3_dr0201);
      m_tmva_pix3l_sct1p_reader->AddVariable("refit_chi2ndof", &m_tmva_pix3l_sct1p_refit_chi2ndof);
      // --- Book the MVA methods
      TString dir;
      TString weightfile;
      TString prefix = "TMVAClassification";
      TString methodName("BDT method");
      //
      dir = "../Dataset_pix4l_sct0/weights/";
      weightfile = dir + prefix + TString("_BDT.weights.xml");
      m_tmva_pix4l_sct0_reader->BookMVA(methodName, weightfile); 
      //
      dir = "../Dataset_pix4l_sct1p/weights/";
      weightfile = dir + prefix + TString("_BDT.weights.xml");
      m_tmva_pix4l_sct1p_reader->BookMVA(methodName, weightfile); 
      //
      dir = "../Dataset_pix3l_sct0/weights/";
      weightfile = dir + prefix + TString("_BDT.weights.xml");
      m_tmva_pix3l_sct0_reader->BookMVA(methodName, weightfile); 
      //
      dir = "../Dataset_pix3l_sct1p/weights/";
      weightfile = dir + prefix + TString("_BDT.weights.xml");
      m_tmva_pix3l_sct1p_reader->BookMVA(methodName, weightfile); 
   }

   // histogram booking
   CHECK( book_chargino_info_base() );
   CHECK( book_trigger_info_base() );
   CHECK( book_vertex_info() );
   // CHECK( book_n_triplets_info() );
   //
   CHECK( book_dr() );
   CHECK( book_isol_calc_info() );
   //
   CHECK( book_fittrk_n_info("disTrkFit") );
   CHECK( book_chargino_vs_fittrk_info("disTrkFit") );
   CHECK( book_fittrk_info_base("disTrkFit") );
   CHECK( book_fittrk_info_base("disTrkFit_match_to_chg") );
   CHECK( book_fittrk_info_base("disTrkFit_unmatched_to_chg") );
   //
   CHECK( book_fittrk_info_base("disTrkFit_presel") );
   CHECK( book_fittrk_info_base("disTrkFit_presel_match_to_chg") );
   CHECK( book_fittrk_info_base("disTrkFit_presel_unmatched_to_chg") );
   //
   CHECK( book_fittrk_info_base("disTrkFit_bdt") );
   CHECK( book_fittrk_info_base("disTrkFit_bdt_match_to_chg") );
   CHECK( book_fittrk_info_base("disTrkFit_bdt_unmatched_to_chg") );

   //
   //
   return StatusCode::SUCCESS;
}

// ================================================================================================
// ================================================================================================

StatusCode MyPackageAlg::finalize()
{
   ATH_MSG_INFO ("Finalizing " << name() << "...");

   ATH_MSG_INFO ("finalize(): Number of clean events = " << m_numCleanEvents);

   return StatusCode::SUCCESS;
}

// ================================================================================================
// ================================================================================================

StatusCode MyPackageAlg::do_Triplet(const Trig::FeatureContainer& features, const std::vector<TVector3>& v_chg_p3)
{ 
   const std::string fname = "MyPackageAlg::do_Triplet> ";

   // get TrigCompositeData
   decltype(features.containerFeature<xAOD::TrigCompositeContainer>()) featuresDisTrkTriplet;
   featuresDisTrkTriplet = features.containerFeature<xAOD::TrigCompositeContainer>("DisTrkTriplet");

   int n_triplets                  = 0;
   int n_triplets_allpix           = 0;
   int n_triplets_allpix_allbarrel = 0;
   int n_triplets_allpix_allbarrel_w_combTrk = 0;
   int n_triplets_w_combTrk        = 0;
   int n_triplets_allpix_w_combTrk = 0;

   int n_combTrks_pixbr = 0;
   int n_combTrks_pixbr_match_to_chg = 0;

   for (auto &myDataContainer : featuresDisTrkTriplet)  {
      if (myDataContainer.cptr())  {
	 for (auto itMyData : *myDataContainer.cptr())  {

	    bool is_allpix           = is_seed_allpix(itMyData);
	    bool is_allpix_allbarrel = is_seed_allpix_allbarrel(itMyData);
	    if( is_allpix )           n_triplets_allpix++;
	    if( is_allpix_allbarrel ) n_triplets_allpix_allbarrel++;

	    int n_combTrks = 0;
	    (itMyData)->getDetail("n_combTrks", n_combTrks);
	    if(n_combTrks > 0) {
	       n_triplets_w_combTrk++;
	       if( is_allpix )           n_triplets_allpix_w_combTrk++;
	       if( is_allpix_allbarrel ) {
		  n_triplets_allpix_allbarrel_w_combTrk++;
		  n_combTrks_pixbr++;
		  TVector3 trk_p3 = get_track_p3(itMyData);
		  float dr_to_chg=9999; int idx_of_chg=-1;
		  calc_dr_min(trk_p3, v_chg_p3, dr_to_chg, idx_of_chg);
		  bool is_match_to_chargino = ( dr_to_chg < DR_TO_CHG_CUT_VALUE );
		  if( is_match_to_chargino ) n_combTrks_pixbr_match_to_chg++;
	       }
	    }

	    // next triplet
	    n_triplets++;
	 }
      }
   }
   if( m_is_debug_mode ) {
      std::cout << fname << "Nr of triplets / with comb-track = " << n_triplets << " / " << n_triplets_w_combTrk << std::endl;
      std::cout << fname << "Nr of triplets allpix / with comb-track = " << n_triplets_allpix << " / " << n_triplets_allpix_w_combTrk << std::endl;
      std::cout << fname << "Nr of triplets allpix-allbarrel / with comb-track = " << n_triplets_allpix_allbarrel << " / " << n_triplets_allpix_allbarrel_w_combTrk << std::endl;
   }
   //
   fill_n_triplets_info(n_triplets, n_triplets_w_combTrk, n_triplets_allpix, n_triplets_allpix_w_combTrk, n_triplets_allpix_allbarrel, n_triplets_allpix_allbarrel_w_combTrk);

   //
   //
   return StatusCode::SUCCESS;
}

// ================================================================================================
// ================================================================================================

StatusCode MyPackageAlg::execute()
{ 
   const std::string fname = "MyPackageAlg::execute> ";
   
   ATH_MSG_DEBUG ("Executing " << name() << "...");
   setFilterPassed(false);
  
   // event info
   const xAOD::EventInfo *eventInfo = nullptr;
   CHECK( evtStore()->retrieve( eventInfo , "EventInfo" ) );
   auto actualMu = eventInfo->actualInteractionsPerCrossing(); 
   ATH_MSG_INFO("eventNumber = " << eventInfo->eventNumber() );
   bool isMC = eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION );
     
   // pileup reweight
   /*
   float pileupWeight = 1;
   if (isMC) {
      m_pileupTool->getCombinedWeight( *eventInfo );
      ATH_MSG_DEBUG("pileup weight = " << pileupWeight );
   }
   */
      
   // reject event if problematic data events
   if (!isMC) {
      if ((eventInfo->errorState(xAOD::EventInfo::LAr) == xAOD::EventInfo::Error) ||
	  (eventInfo->errorState(xAOD::EventInfo::Tile) == xAOD::EventInfo::Error) ||
	  (eventInfo->errorState(xAOD::EventInfo::SCT) == xAOD::EventInfo::Error) ||
	  (eventInfo->isEventFlagBitSet(xAOD::EventInfo::Core, 18))) {
	 return StatusCode::SUCCESS;
      }
   }
   m_numCleanEvents++;

   // GRL
   if (!isMC) {
      if (!m_grlTool->passRunLB(*eventInfo)) {
	 ATH_MSG_DEBUG ("drop event: GRL");
	 return StatusCode::SUCCESS;
      }
   }
   
   // get jet container of interest
   const xAOD::JetContainer* jets = 0;
   CHECK(evtStore()->retrieve( jets, "AntiKt4EMTopoJets" ));
   ATH_MSG_DEBUG ("number of jets = " << jets->size());

   // -----------------------------------------------------------------------------
   // trigger pre-selection
   // -----------------------------------------------------------------------------

   if( is_trigger_presel && ! m_tdt->isPassed(presel_trigger) ) return StatusCode::SUCCESS;

   // -----------------------------------------------------------------------------
   // truth signal info (+ possible pre-selection)
   // -----------------------------------------------------------------------------

   // truth MC container
   const xAOD::TruthEventContainer* xTruthEventContainer = NULL;
   CHECK( evtStore()->retrieve( xTruthEventContainer, "TruthEvents"));

   // get truth signal (chargino)
   std::vector<TVector3> v_chg_all_p3;
   std::vector<TVector3> v_chg_all_decay;
   std::vector<TVector3> v_chg_all_prod;
   if( m_is_signal_mc ) {
      if( ! getTruthChargino(xTruthEventContainer, v_chg_all_p3, v_chg_all_prod, v_chg_all_decay) ) {
	 ATH_MSG_ERROR("getTruthChargino returned error");
      }
   }
   fill_h1_each("chg_n_all",v_chg_all_decay.size());
   float chg_twosyst_pt = 0;
   if( v_chg_all_decay.size()==2 ) { chg_twosyst_pt = (v_chg_all_p3[0]+v_chg_all_p3[1]).Pt(); }
   if( m_is_debug_mode ) std::cout << fname << "pT of two chargino system = " << chg_twosyst_pt << std::endl;

   // limit chargino only in good decay range
   std::vector<TVector3> v_chg_p3;
   std::vector<TVector3> v_chg_decay;
   std::vector<TVector3> v_chg_prod;
   for(unsigned int i_chg=0; i_chg<v_chg_all_decay.size(); i_chg++) {
      TVector3 chg_decay = v_chg_all_decay[i_chg];
      float chg_decay_r = chg_decay.Perp();
      fill_h1_each("chg_decay_r_all",chg_decay_r);
      TVector3 chg_p3 = v_chg_all_p3[i_chg];
      fill_h1_each("chg_pt_all", hist_pt(chg_p3.Pt()));
      fill_h1_each("chg_eta_all",chg_p3.Eta());
      if( ! is_target_chg(chg_decay_r, chg_p3) ) continue;
      v_chg_p3.push_back(chg_p3);
      v_chg_decay.push_back(chg_decay);
      v_chg_prod.push_back(v_chg_all_prod[i_chg]);
   }
   fill_chargino_info_base(v_chg_p3,v_chg_decay,jets,chg_twosyst_pt);

   // no further process if no chargino in good decay range
   if( m_is_signal_mc && v_chg_p3.size() < 1 )  return StatusCode::SUCCESS;

   if( m_is_debug_mode ) std::cout << fname << "chargino cand: ===========================>" << std::endl;
   for(unsigned int i_chg=0; i_chg<v_chg_p3.size(); i_chg++) {
      TVector3 chg_p3 = v_chg_p3[i_chg];
      if( m_is_debug_mode ) std::cout << fname << "i_chg = " << i_chg << ": pt / eta / phi = " << chg_p3.Pt() << " / " << chg_p3.Eta() << " / " << chg_p3.Phi() << std::endl;
   }

   // -----------------------------------------------------------------------------
   // trigger and vertex
   // -----------------------------------------------------------------------------

   // L1 trigger
   m_map_l1_results.clear();
   if( m_is_debug_mode ) std::cout << fname << "L1 trigger items:" << std::endl;
   for(unsigned int i=0; i<m_v_l1_items.size(); i++) {
      std::string name = m_v_l1_items[i];
      bool is_passed = false;
      if( m_tdt->isPassed(("L1_"+name).c_str()) ) {
	 is_passed = true;
	 m_h1_l1_counts->Fill(i+0.1);
	 if( m_is_debug_mode ) std::cout << fname << "..." << name << " passed" << std::endl;
      }
      m_map_l1_results.insert(std::make_pair(name,is_passed));
   }

   // HLT MET container (needed for emulation)
   const std::string containerName = "HLT_xAOD__TrigMissingETContainer_TrigEFMissingET_topocl_PUC";
   const xAOD::TrigMissingETContainer* hltCont(0);
   CHECK( evtStore()->retrieve(hltCont, containerName) );
   float ex = hltCont->front()->ex();
   float ey = hltCont->front()->ey();
   float hltMet = sqrt(ex*ex + ey*ey);
   if( m_is_debug_mode ) std::cout << fname << "HLT ETmiss = " << hltMet/1000.0 << std::endl;

   // HLT decision (+emulation)
   m_map_hlt_results.clear();
   if( m_is_debug_mode ) std::cout << fname << "HLT trigger items:" << std::endl;
   for(unsigned int i=0; i<m_v_hlt_chains.size(); i++) {
      std::string name = m_v_hlt_chains[i];
      bool is_passed = false;
      float hltMetGeV = hltMet/1000.0;
      if     ( name ==  "xe60_pufit_L1XE35" ) { is_passed = (hltMetGeV >  60.0 && m_map_l1_results["XE35"]); }
      else if( name ==  "xe70_pufit_L1XE35" ) { is_passed = (hltMetGeV >  70.0 && m_map_l1_results["XE35"]); }
      else if( name ==  "xe80_pufit_L1XE35" ) { is_passed = (hltMetGeV >  80.0 && m_map_l1_results["XE35"]); }
      else if( name ==  "xe90_pufit_L1XE35" ) { is_passed = (hltMetGeV >  90.0 && m_map_l1_results["XE35"]); }
      else if( name == "xe100_pufit_L1XE35" ) { is_passed = (hltMetGeV > 100.0 && m_map_l1_results["XE35"]); }
      else if( name == "xe110_pufit_L1XE35" ) { is_passed = (hltMetGeV > 110.0 && m_map_l1_results["XE35"]); }
      //
      else if( name ==  "xe60_pufit_L1XE50" ) { is_passed = (hltMetGeV >  60.0 && m_map_l1_results["XE50"]); }
      else if( name ==  "xe70_pufit_L1XE50" ) { is_passed = (hltMetGeV >  70.0 && m_map_l1_results["XE50"]); }
      else if( name ==  "xe80_pufit_L1XE50" ) { is_passed = (hltMetGeV >  80.0 && m_map_l1_results["XE50"]); }
      else if( name ==  "xe90_pufit_L1XE50" ) { is_passed = (hltMetGeV >  90.0 && m_map_l1_results["XE50"]); }
      else if( name == "xe100_pufit_L1XE50" ) { is_passed = (hltMetGeV > 100.0 && m_map_l1_results["XE50"]); }
      else if( name == "xe110_pufit_L1XE50" ) { is_passed = (hltMetGeV > 110.0 && m_map_l1_results["XE50"]); }
      else {
	 if( m_tdt->isPassed(("HLT_"+name).c_str()) ) { is_passed = true; }
      }
      if( is_passed ) {
	 m_h1_hlt_counts->Fill(i+0.1);
	 if( m_is_debug_mode ) std::cout << fname << "..." << name << " passed" << std::endl;
      }
      m_map_hlt_results.insert(std::make_pair(name,is_passed));
   }

   // offline vertex
   const xAOD::VertexContainer* vertices = 0;
   CHECK( evtStore()->retrieve(vertices,"PrimaryVertices") );
   std::vector<TVector3> v_prim_vertices;
   for (auto vertex : *vertices) {
      if( vertex->vertexType() == xAOD::VxType::PriVtx ) {
	 TVector3 pvtx((vertex)->x(), (vertex)->y(), (vertex)->z());
	 v_prim_vertices.push_back(pvtx);
      }
   }
   float pvtx_x=-9.5; float pvtx_y=-9.5; float pvtx_z=-199.5;
   if( v_prim_vertices.size() >= 1 ) {
      pvtx_x = (v_prim_vertices[0]).x();
      pvtx_y = (v_prim_vertices[0]).y();
      pvtx_z = (v_prim_vertices[0]).z();
   }
   if( m_is_debug_mode ) std::cout << fname << "primary vertex x / y / z = " << pvtx_x << " / " << pvtx_y << " / " << pvtx_z << std::endl;
   if( v_prim_vertices.size() >= 2) {
      for(unsigned int i=1; i<v_prim_vertices.size(); i++) {
	 if( m_is_debug_mode ) std::cout << fname << "... other primary vertex (idx: " << (i+1) << ") : x / y / z = " << (v_prim_vertices[i]).x() << " / " << (v_prim_vertices[i]).y() << " / " << (v_prim_vertices[i]).z() << std::endl;
      }
   }
   fill_vertex_info(v_prim_vertices, v_chg_prod);
   
   // -----------------------------------------------------------------------------
   // relavant chain and features
   // -----------------------------------------------------------------------------

   const std::string chain = "HLT_mu20_ucnvtrk";

   auto features = m_tdt->features(chain,TrigDefs::allowResurrectedDecision);

   // -----------------------------------------------------------------------------
   // global info
   // -----------------------------------------------------------------------------

   decltype(features.containerFeature<xAOD::TrigCompositeContainer>()) featuresDisTrkGlobal;
   featuresDisTrkGlobal = features.containerFeature<xAOD::TrigCompositeContainer>("DisTrkGlobal");

   for (auto &myDataContainer : featuresDisTrkGlobal)  {
      if (myDataContainer.cptr())  {
	 for (auto itMyData : *myDataContainer.cptr())  {
	    // trigger vertex
	    fill_fittrk_vertex_info(itMyData,v_chg_prod);
	 }
      }
   }

   // -----------------------------------------------------------------------------
   // 1st loop: triplet and reconstructed track info
   // -----------------------------------------------------------------------------

   // CHECK( do_Triplet(features, v_chg_p3) );
   
   // -----------------------------------------------------------------------------
   // loop on goodTrk
   // -----------------------------------------------------------------------------

   std::string prefix = "goodTrk";
   
   decltype(features.containerFeature<xAOD::TrigCompositeContainer>()) featuresDisTrkGoodTrk;
   featuresDisTrkGoodTrk = features.containerFeature<xAOD::TrigCompositeContainer>("DisTrkGoodTrk");
   
   std::map<unsigned int, std::map<std::string, float> > map_goodTrk_varf;
   std::map<unsigned int, std::map<std::string, int> >   map_goodTrk_vari;

   unsigned int goodTrk_idx=0;
   for (auto &myDataContainer : featuresDisTrkGoodTrk)  {
      if (myDataContainer.cptr())  {
	 for (auto itMyData : *myDataContainer.cptr())  {
	    std::map<std::string, float> varf;
	    std::map<std::string, int>   vari;
	    get_fittrk(prefix,itMyData,varf,vari);
	    map_goodTrk_varf.insert(std::make_pair(goodTrk_idx, varf));
	    map_goodTrk_vari.insert(std::make_pair(goodTrk_idx, vari));
	    goodTrk_idx++;
	 }
      }
   }

   // -----------------------------------------------------------------------------
   // 1st loop on DisTrkCand
   // -----------------------------------------------------------------------------

   prefix = "disTrkFit";
   std::string match  = "_match_to_chg";

   decltype(features.containerFeature<xAOD::TrigCompositeContainer>()) featuresDisTrkFit;//featuresDisTrkFit
   featuresDisTrkFit = features.containerFeature<xAOD::TrigCompositeContainer>("DisTrkCandFit");//DisTrkCandFit
 
   std::vector<std::string> v_cat;
   v_cat.push_back("pix4l_sct0");v_cat.push_back("pix4l_sct1p");
   v_cat.push_back("pix3l_sct0");v_cat.push_back("pix3l_sct1p");
   
   // nr of disTrkFit tracks
   std::map<std::string, int> map_disTrkFit_n;
   map_disTrkFit_n.insert(std::make_pair(prefix+"_n",      0));
   map_disTrkFit_n.insert(std::make_pair(prefix+"_n"+match,0));
   map_disTrkFit_n.insert(std::make_pair(prefix+"_n_presel",      0));
   map_disTrkFit_n.insert(std::make_pair(prefix+"_n_presel"+match,0));
   if( m_is_bdt_validation ) {
      map_disTrkFit_n.insert(std::make_pair(prefix+"_n_bdt",      0));
      map_disTrkFit_n.insert(std::make_pair(prefix+"_n_bdt"+match,0));
   }
   for(auto it=v_cat.begin(); it!=v_cat.end(); it++) {
      map_disTrkFit_n.insert(std::make_pair(prefix+"_n_"+(*it),      0));
      map_disTrkFit_n.insert(std::make_pair(prefix+"_n_"+(*it)+match,0));
      map_disTrkFit_n.insert(std::make_pair(prefix+"_n_"+(*it)+"_presel",      0));
      map_disTrkFit_n.insert(std::make_pair(prefix+"_n_"+(*it)+"_presel"+match,0));
      if( m_is_bdt_validation ) {
	 map_disTrkFit_n.insert(std::make_pair(prefix+"_n_"+(*it)+"_bdt",         0));
	 map_disTrkFit_n.insert(std::make_pair(prefix+"_n_"+(*it)+"_bdt"+match,   0));
      }
   }

   //
   char buf[126];
   std::map<std::string, int> map_chg_n_match_disTrkFit;
   for(int i=0; i<(int)v_chg_p3.size(); i++) {
      sprintf(buf,"chg%i_",i);
      map_chg_n_match_disTrkFit.insert(std::make_pair(std::string(buf)+prefix+"_n",0));
      map_chg_n_match_disTrkFit.insert(std::make_pair(std::string(buf)+prefix+"_n_presel",0));
      if( m_is_bdt_validation ) {
	 map_chg_n_match_disTrkFit.insert(std::make_pair(std::string(buf)+prefix+"_n_bdt",0));
      }
   }

   //
   unsigned int disTrkFit_idx=0;
   std::map<unsigned int, std::map<std::string, float> > map_disTrkFit_varf;
   std::map<unsigned int, std::map<std::string, int> >   map_disTrkFit_vari;
   ATH_MSG_INFO("disTrkCand Container: size=" << featuresDisTrkFit.size());
   fill_h1_each("disTrkCand_n", featuresDisTrkFit.size());
   for (auto &myDataContainer : featuresDisTrkFit)  {//featuresDisTrkFit
      if (myDataContainer.cptr())  {
	 for (auto itMyData : *myDataContainer.cptr())  {

	    int cat = get_fittrk_category(prefix,itMyData);
	       
	    std::map<std::string, float> varf;
	    std::map<std::string, int>   vari;
	    get_fittrk(prefix,itMyData,varf,vari);

	    map_disTrkFit_n[prefix+"_n"]++;
	    if( cat!=0 ) map_disTrkFit_n[prefix+"_n_"+v_cat[cat-1]]++;

	    float bdt_score = fill_fittrk_info_base(prefix,prefix,itMyData,pvtx_z,varf,vari,map_goodTrk_varf,false,m_ofs_signal);
	    bool  bdt_pass = m_is_bdt_validation ? is_bdt_passed(cat,prefix,itMyData,bdt_score) : false;

	    float dr_to_chg = 999; int idx_of_chg = 999;
	    bool is_match_to_chargino = is_fittrk_match_to_chg(prefix, itMyData, v_chg_p3, dr_to_chg, idx_of_chg);
            std::cout << "dr_to_chargino = " << dr_to_chg << std::endl;
	    if( is_match_to_chargino ) {
	       map_disTrkFit_n[prefix+"_n"+match]++;
	       if( cat!=0 ) map_disTrkFit_n[prefix+"_n_"+v_cat[cat-1]+match]++;
	       sprintf(buf,"chg%i_",idx_of_chg); map_chg_n_match_disTrkFit[std::string(buf)+prefix+"_n"]++;
	       fill_fittrk_info_base(prefix,prefix+match,itMyData,pvtx_z,varf,vari,map_goodTrk_varf,false,m_ofs_signal);
	    }
	    else if( dr_to_chg > 3.0 ) {
	       fill_fittrk_info_base(prefix,prefix+"_unmatched_to_chg",itMyData,pvtx_z,varf,vari,map_goodTrk_varf,false,m_ofs_signal);
	    }

	    // presel
	    if( presel_fittrk(prefix,itMyData) ) {
               fill_h1_each("dr_to_chg_presel",dr_to_chg);
	       map_disTrkFit_varf.insert(std::make_pair(disTrkFit_idx, varf));
	       map_disTrkFit_vari.insert(std::make_pair(disTrkFit_idx, vari));

	       map_disTrkFit_n[prefix+"_n_presel"]++;
	       if( cat!=0 ) map_disTrkFit_n[prefix+"_n_"+v_cat[cat-1]+"_presel"]++;
	       bool doDump = false;
	       if( ! m_is_signal_mc && ! m_is_bdt_validation ) doDump = true;
	       fill_fittrk_info_base(prefix,prefix+"_presel",itMyData,pvtx_z,varf,vari,map_goodTrk_varf,doDump,m_ofs_background);

 	       if( is_match_to_chargino ) {
                  fill_h1_each("dr_match_to_chg_presel",dr_to_chg);
                  std::cout << "->" << "-> Pass Bool match_to_chg_presel:  category = " << cat  << std::endl;
		  map_disTrkFit_n[prefix+"_n_presel"+match]++;
		  if( cat!=0 ) map_disTrkFit_n[prefix+"_n_"+v_cat[cat-1]+"_presel"+match]++;
		  sprintf(buf,"chg%i_",idx_of_chg); map_chg_n_match_disTrkFit[std::string(buf)+prefix+"_n_presel"]++;
		  bool doDump = false;
		  if( m_is_signal_mc && ! m_is_bdt_validation ) doDump = true;
		  fill_fittrk_info_base(prefix,prefix+"_presel"+match,itMyData,pvtx_z,varf,vari,map_goodTrk_varf,doDump,m_ofs_signal);
		  if( m_is_debug_mode ) {
		     std::cout << fname << "disTrkFit presel (idx: " << disTrkFit_idx << ") reconstructed for chargino idx: " << idx_of_chg << std::endl;
		     print_fittrk(fname+"...",varf,vari);
		  }
	       }
	       else if( dr_to_chg > 3.0 ) {
		  bool doDump = false;
		  if( m_is_signal_mc && ! m_is_bdt_validation ) doDump = true;
		  fill_fittrk_info_base(prefix,prefix+"_presel_unmatched_to_chg",itMyData,pvtx_z,varf,vari,map_goodTrk_varf,doDump,m_ofs_background);
	       }

	       //
	       // presel+BDT if BDT validation
	       if( m_is_bdt_validation && bdt_pass ) {
		  map_disTrkFit_n[prefix+"_n_bdt"]++;
		  if( cat!=0 ) map_disTrkFit_n[prefix+"_n_"+v_cat[cat-1]+"_bdt"]++;
		  fill_fittrk_info_base(prefix,prefix+"_bdt",itMyData,pvtx_z,varf,vari,map_goodTrk_varf,false,m_ofs_background);
		  if( is_match_to_chargino ) {
		     map_disTrkFit_n[prefix+"_n_bdt"+match]++;
		     if( cat!=0 ) map_disTrkFit_n[prefix+"_n_"+v_cat[cat-1]+"_bdt"+match]++;
		     sprintf(buf,"chg%i_",idx_of_chg); map_chg_n_match_disTrkFit[std::string(buf)+prefix+"_n_bdt"]++;
		     fill_fittrk_info_base(prefix,prefix+"_bdt"+match,itMyData,pvtx_z,varf,vari,map_goodTrk_varf,false,m_ofs_signal);
		     if( m_is_debug_mode ) {
			std::cout << fname << "disTrkFit bdt (idx: " << disTrkFit_idx << ") reconstructed for chargino idx: " << idx_of_chg << std::endl;
			print_fittrk(fname+"...",varf,vari);
		     }
		  }
		  else if( dr_to_chg > 3.0 ) {
		     fill_fittrk_info_base(prefix,prefix+"_bdt_unmatched_to_chg",itMyData,pvtx_z,varf,vari,map_goodTrk_varf,false,m_ofs_background);
		  }
	       }
	       //
	       //

	    } // end of presel


	    // next
	    disTrkFit_idx++;
	 }
      }
   }

   //
   if( m_is_debug_mode ) {
      std::cout << fname << "-> Nr of disTrkFit                  : " << map_disTrkFit_n[prefix+"_n"] << std::endl;
      std::cout << fname << "     " << v_cat[0] << " / " << v_cat[1] << " / " << v_cat[2] << " / " << v_cat[3] << " = "
		<< map_disTrkFit_n[prefix+"_n_"+v_cat[0]] << " / " << map_disTrkFit_n[prefix+"_n_"+v_cat[1]] << " / "
		<< map_disTrkFit_n[prefix+"_n_"+v_cat[2]] << " / " << map_disTrkFit_n[prefix+"_n_"+v_cat[3]] << std::endl;
      //
      std::cout << fname << "-> Nr of disTrkFit + presel         : " << map_disTrkFit_n[prefix+"_n_presel"] << std::endl;
      std::cout << fname << "     " << v_cat[0] << " / " << v_cat[1] << " / " << v_cat[2] << " / " << v_cat[3] << " = "
		<< map_disTrkFit_n[prefix+"_n_"+v_cat[0]+"_presel"] << " / " << map_disTrkFit_n[prefix+"_n_"+v_cat[1]+"_presel"] << " / "
		<< map_disTrkFit_n[prefix+"_n_"+v_cat[2]+"_presel"] << " / " << map_disTrkFit_n[prefix+"_n_"+v_cat[3]+"_presel"] << std::endl;
      //
      std::cout << fname << "-> Nr of disTrkFit + bdt         : " << map_disTrkFit_n[prefix+"_n_bdt"] << std::endl;
      std::cout << fname << "     " << v_cat[0] << " / " << v_cat[1] << " / " << v_cat[2] << " / " << v_cat[3] << " = "
		<< map_disTrkFit_n[prefix+"_n_"+v_cat[0]+"_bdt"] << " / " << map_disTrkFit_n[prefix+"_n_"+v_cat[1]+"_bdt"] << " / "
		<< map_disTrkFit_n[prefix+"_n_"+v_cat[2]+"_bdt"] << " / " << map_disTrkFit_n[prefix+"_n_"+v_cat[3]+"_bdt"] << std::endl;
      //
      std::cout << fname << "-> Nr of matched disTrkFit          : " << map_disTrkFit_n[prefix+"_n"+match] << std::endl;
      std::cout << fname << "     " << v_cat[0] << " / " << v_cat[1] << " / " << v_cat[2] << " / " << v_cat[3] << " = "
		<< map_disTrkFit_n[prefix+"_n_"+v_cat[0]+match] << " / " << map_disTrkFit_n[prefix+"_n_"+v_cat[1]+match] << " / "
		<< map_disTrkFit_n[prefix+"_n_"+v_cat[2]+match] << " / " << map_disTrkFit_n[prefix+"_n_"+v_cat[3]+match] << std::endl;
      //
      std::cout << fname << "-> Nr of matched disTrkFit + presel : " << map_disTrkFit_n[prefix+"_n_presel"+match] << std::endl;
      std::cout << fname << "     " << v_cat[0] << " / " << v_cat[1] << " / " << v_cat[2] << " / " << v_cat[3] << " = "
		<< map_disTrkFit_n[prefix+"_n_"+v_cat[0]+"_presel"+match] << " / " << map_disTrkFit_n[prefix+"_n_"+v_cat[1]+"_presel"+match] << " / "
		<< map_disTrkFit_n[prefix+"_n_"+v_cat[2]+"_presel"+match] << " / " << map_disTrkFit_n[prefix+"_n_"+v_cat[3]+"_presel"+match] << std::endl;
      //
      std::cout << fname << "-> Nr of matched disTrkFit + bdt : " << map_disTrkFit_n[prefix+"_n_bdt"+match] << std::endl;
      std::cout << fname << "     " << v_cat[0] << " / " << v_cat[1] << " / " << v_cat[2] << " / " << v_cat[3] << " = "
		<< map_disTrkFit_n[prefix+"_n_"+v_cat[0]+"_bdt"+match] << " / " << map_disTrkFit_n[prefix+"_n_"+v_cat[1]+"_bdt"+match] << " / "
		<< map_disTrkFit_n[prefix+"_n_"+v_cat[2]+"_bdt"+match] << " / " << map_disTrkFit_n[prefix+"_n_"+v_cat[3]+"_bdt"+match] << std::endl;
   }

   fill_fittrk_n_info(prefix,map_disTrkFit_n);

   if( m_is_debug_mode ) {
      for(unsigned int i=0; i<v_chg_p3.size(); i++) {
	 sprintf(buf,"chg%i_",i); 
	 std::cout << fname << "Nr of matched disTrkFit for chargino (idx: " << i << ") : " << map_chg_n_match_disTrkFit[std::string(buf)+prefix+"_n"];
	 std::cout << " -> " << map_chg_n_match_disTrkFit[std::string(buf)+prefix+"_n_presel"] << " (presel)";
	 std::cout << " -> " << map_chg_n_match_disTrkFit[std::string(buf)+prefix+"_n_bdt"] << " (bdt)" << std::endl;
      }
   }
   
   fill_chargino_vs_fittrk_info(prefix,v_chg_p3,v_chg_decay,actualMu,map_chg_n_match_disTrkFit);

   // -----------------------------------------------------------------------------
   // trigger info with BDT
   // -----------------------------------------------------------------------------

   int n_bdt = map_disTrkFit_n[prefix+"_n_bdt"];

   if( m_is_debug_mode ) std::cout << fname << "L1 trigger items:" << std::endl;
   for(unsigned int i=0; i<m_v_l1_items.size(); i++) {
      std::string name = m_v_l1_items[i];
      bool is_passed = m_map_l1_results[name];
      if( is_passed && n_bdt > 0 ) {
	 m_h1_l1_bdt_counts->Fill(i+0.1);
	 if( m_is_debug_mode ) std::cout << fname << "..." << name << " BDT passed" << std::endl;
      }
   }

   if( m_is_debug_mode ) std::cout << fname << "HLT trigger items:" << std::endl;
   for(unsigned int i=0; i<m_v_hlt_chains.size(); i++) {
      std::string name = m_v_hlt_chains[i];
      bool is_passed = m_map_hlt_results[name];
      if( is_passed && n_bdt > 0 ) {
	 m_h1_hlt_bdt_counts->Fill(i+0.1);
	 if( m_is_debug_mode ) std::cout << fname << "..." << name << " BDT passed" << std::endl;
      }
   }

   // -----------------------------------------------------------------------------
   // trigger as a function of chargino twosystem mass
   // -----------------------------------------------------------------------------

   if( m_map_l1_results["XE35"] ) { fill_h1_each("chg_twosyst_pt_L1XE35", chg_twosyst_pt); }
   if( m_map_l1_results["XE40"] ) { fill_h1_each("chg_twosyst_pt_L1XE40", chg_twosyst_pt); }
   if( m_map_l1_results["XE50"] ) { fill_h1_each("chg_twosyst_pt_L1XE50", chg_twosyst_pt); }
   if( m_map_hlt_results["xe110_pufit_L1XE50"] ) { fill_h1_each("chg_twosyst_pt_xe110", chg_twosyst_pt); }
   if( m_map_hlt_results["xe110_pufit_L1XE35"] ) { fill_h1_each("chg_twosyst_pt_L1XE35_xe110", chg_twosyst_pt); }
   if( m_map_hlt_results["xe80_pufit_L1XE50"] ) { fill_h1_each("chg_twosyst_pt_xe80", chg_twosyst_pt); }
   if( m_map_hlt_results["xe80_pufit_L1XE35"] ) { fill_h1_each("chg_twosyst_pt_L1XE35_xe80", chg_twosyst_pt); }

   if( m_is_bdt_validation ) {
      if(  m_map_l1_results["XE35"] && n_bdt > 0 ) { fill_h1_each("chg_twosyst_pt_L1XE35bdt", chg_twosyst_pt); }
      if( (m_map_l1_results["XE35"] && n_bdt > 0) || m_map_hlt_results["xe110_pufit_L1XE50"] ) {
	 fill_h1_each("chg_twosyst_pt_xe110_or_L1XE35bdt", chg_twosyst_pt);
      }
      if(  m_map_l1_results["XE50"] && n_bdt > 0 ) { fill_h1_each("chg_twosyst_pt_L1XE50bdt", chg_twosyst_pt); }
      if(  m_map_hlt_results["xe80_pufit_L1XE50"] && n_bdt > 0 ) { fill_h1_each("chg_twosyst_pt_xe80bdt", chg_twosyst_pt); }
      if( m_map_hlt_results["xe110_pufit_L1XE50"] || (m_map_hlt_results["xe80_pufit_L1XE50"] && n_bdt > 0)  ) {
	 fill_h1_each("chg_twosyst_pt_xe110_or_xe80bdt", chg_twosyst_pt);
      }
      if( m_map_hlt_results["xe110_pufit_L1XE50"] && (m_map_hlt_results["xe80_pufit_L1XE50"] && n_bdt > 0)  ) {
         fill_h1_each("chg_twosyst_pt_xe110_and_xe80bdt", chg_twosyst_pt);
      }
      if( m_map_hlt_results["xe110_pufit_L1XE50"] ^ (m_map_hlt_results["xe80_pufit_L1XE50"] && n_bdt > 0)  ) {
         fill_h1_each("chg_twosyst_pt_xe110_xor_xe80bdt", chg_twosyst_pt);
      }
      if( !m_map_hlt_results["xe110_pufit_L1XE50"] && (m_map_hlt_results["xe80_pufit_L1XE50"] && n_bdt > 0)  ) {
         fill_h1_each("chg_twosyst_pt_notxe110_and_xe80bdt", chg_twosyst_pt);
      }
      if( m_map_hlt_results["xe110_pufit_L1XE50"] && !(m_map_hlt_results["xe80_pufit_L1XE50"] && n_bdt > 0)  ) {
         fill_h1_each("chg_twosyst_pt_xe110_and_notxe80bdt", chg_twosyst_pt);
      }
      if( m_map_hlt_results["xe110_pufit_L1XE50"] || (m_map_hlt_results["xe60_pufit_L1XE35"] && n_bdt > 0)  ) {
	 fill_h1_each("chg_twosyst_pt_xe110_or_xe60bdt", chg_twosyst_pt);
      }
   }

   // -----------------------------------------------------------------------------
   //
   // -----------------------------------------------------------------------------

   // muon
   const xAOD::MuonContainer* muons = 0;
   CHECK( evtStore()->retrieve(muons,"CalibratedMuons") );
   for (auto muon : *muons) {
      float pt  = (muon)->pt() * 0.001;
      float eta = (muon)->eta();
      float phi = (muon)->phi();
      ATH_MSG_DEBUG ("calibrated muon pt/eta/phi = " << pt << " GeV / " << eta << " / " << phi);
      if ( !m_muonSelection->accept(*muon) ) {
	 ATH_MSG_DEBUG ("--> muonSelector accepted");
      }
   }

   // raw muons
   CHECK( evtStore()->retrieve(muons,"Muons") );
   for (auto muon : *muons) {
      ATH_MSG_DEBUG ("raw muon pt = " << ((muon)->pt() * 0.001) << " GeV");
      if ( !m_muonSelection->accept(*muon) ) {
	 ATH_MSG_DEBUG ("--> muonSelector accepted");
      }
   }

   // electron
   const xAOD::ElectronContainer* electrons = 0;
   CHECK( evtStore()->retrieve(electrons,"CalibratedElectrons") );
   for (auto electron : *electrons) {
      ATH_MSG_DEBUG ("calibrated electron pt = " << ((electron)->pt() * 0.001) << " GeV");
   }

   // photon
   const xAOD::PhotonContainer* photons = 0;
   CHECK( evtStore()->retrieve(photons,"CalibratedPhotons") );
   // for (auto photon : *photons) {
   // ATH_MSG_INFO ("calibrated photon pt = " << ((photon)->pt() * 0.001) << " GeV");
   // }
  
   // loop over the jets in the container
   unsigned numGoodJets = 0;
   for (auto jet : *jets) {
      ATH_MSG_DEBUG ("jet pt = " << (jet->pt() * 0.001) << " GeV"); // just to print out something
      // JER and uncert
      if(isMC){
	 // Get the MC resolution
	 double mcRes = m_JERTool->getRelResolutionMC(jet);
	 // Get the resolution uncertainty
	 double uncert = m_JERTool->getUncertainty(jet); // you can provide a second argument specify which nuisance parameter, the default is all 
	 ATH_MSG_DEBUG ("--> jet mcRes = " << mcRes << " , uncert = " << uncert);
      } // end if MC
      
      // jet cleaning
      if (!m_jetCleaning->keep (*jet)) continue;
      ++ numGoodJets;
   }
   ATH_MSG_DEBUG ("number of good jets = " << numGoodJets);
   
   //
   setFilterPassed(true); //if got here, assume that means algorithm passed
   return StatusCode::SUCCESS;
}

// ================================================================================================
// ================================================================================================

StatusCode MyPackageAlg::beginInputFile() { 
  //
  //This method is called at the start of each input file, even if
  //the input file contains no events. Accumulate metadata information here
  //

  //example of retrieval of CutBookkeepers: (remember you will need to include the necessary header files and use statements in requirements file)
  // const xAOD::CutBookkeeperContainer* bks = 0;
  // CHECK( inputMetaStore()->retrieve(bks, "CutBookkeepers") );

  //example of IOVMetaData retrieval (see https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/AthAnalysisBase#How_to_access_file_metadata_in_C)
  //float beamEnergy(0); CHECK( retrieveMetadata("/TagInfo","beam_energy",beamEnergy) );
  //std::vector<float> bunchPattern; CHECK( retrieveMetadata("/Digitiation/Parameters","BeamIntensityPattern",bunchPattern) );



  return StatusCode::SUCCESS;
}

// ================================================================================================
// ================================================================================================
