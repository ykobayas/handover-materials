# Handover materials

The technical handover materials of disappearing track trigger from 2020/04 - 2022/03
※私が主に作業していた作業空間はgpfs/fs7001/ykobayas/gitMC/distrk_aodanalysisで、そこのathena/MyPackageに研究に使用していた最新のMyPackageAlg.cxxがあります。
本来gitで管理するところですが、もし困ったら直接コピーするか私にご一報ください。（あるいはこの引継ぎgitの中にも最新のMyPackageAlg.cxxと.hだけはあるのでそれを使っても同じです）
またgpfs/fs7001/ykobayas/EB、及びgpfs/fs7001/ykobayas/EB2以下にあるファイルも同様です。

※R22に関しての作業空間は
- RDOtoAODはhome/ykobayas/distrk_dev/distrk_rdo2aod_R22
- retracking RDOtoAODはhome/ykobayas/distrk_dev/distrk_rdo2aod4
- AOD analysisはhome/ykobayas/distrk_dev/distrk_aodanalysis_R22  (sourceディレクトリ以下が重要)
- AOD analysis for retrackingはhome/ykobayas/distrk_dev/distrk_aodanalysis2

 retrackingしたZMUMUAODは以下にあります。（消えていたら小林にお声掛けください）
- home/ykobayas/distrk_dev/distrk_aodanalysis2/datafiles_ZMUMU

## distrk_aodanalysis for R21 (memo.txtにも同様の内容が書いてあります)
Plz refer to git of [distrk_aodanalysis](https://gitlab.cern.ch/ykobayas/distrk_aodanalysis) if you want to understand only basical points.
### athena/MyPackage
MyPackage is put in distrk_aodanalysis/athena/MyPackage
MyPackage govern the a lot of points.

###MyPackage/src/MyPackageAlg.cxx
- A lot of variables for analysis are daclared in MyPackageAlg.cxx and MyPackageAlg.h.
- book and fill alot of histgrams which is necessary to analyse.
- By looping event in container(AOD data), this program calculates some important things of each event and track.
    - e.g.)
    - For signal(chargino MC), judging whether each track matches truth(MC object) or not, and if not, finishing loop and go next event.(Maybe lines 1800--2000?)
    - For all tracks in each event, some variables are retrieved or calculated, for example, jet isolation, impact parameter, and category (pix4lsct0 -- pix3lsct1p)
    - For all tracks, they are sorted with preselection and BDT selection (only validation phase). (if you want to see, command ""find . -type f -exec grep -Hi -n "preselect" {} \;"")
    - For all tracks in validation phase, we calculete BDT score and orgnise selection information (Nr of tracks passed to each selection and in each category and so on).

If you want to do advanced analysis in R21, you will need to modify MyPackage, e.g.,in the case if you change BDT input variables at MVAtuning or want to make new histgram or calculate new variables ,and so on.


※もしBDT input variableをMyPackage内で変えたかったら
- MyPackageAlg.h ->・bdt_eval_pix....関数の中身(float, float,int...)　->pix4lsct_tmva_variable の定義
- MyPackageAlg.cxx ->・bdt_eval_pix_sct関数
　　　　　　　　　    ・tmva_関数  (TMVA_readerからの読み込み)
　　　　　　　　　    ・bdt_score関数
の４つくらいを変える必要があります。面倒くさいのでいい方法があればそちらを。

※distrk_aodanalysis (R21)の私のgitは最新版に更新してあります。すなわち、BDTのinput変数などは昔のgitのものより多くそれに応じて、MyPackageAlg.cxxも変えなければなりません。
それゆえ、もし20220330以降にこのgitをforkあるいはcloneしたもので解析する場合は、MyPacakgeの中身を最新版に更新する必要があります。既存のものを更新してもいいし、既存ファイルをMyPackageAlg.cxx~みたいな回避名にして、新たに最新版をここから（あるいは私の作業空間から）コピーしてもよいです。自分で１から書き換えてもよいです。

## R22での作業について
基本的にR22で行っていた作業は２つあり、１つがRDO→AOD変換(MCの場合、データはbsrdo→AOD)でこれは/home/ykobayas/distrk_dev/distrk_rdo2aod_R22もしくは/home/ykobayas/distrk_dev/distrk_rdo2aod4で行っていました。前者は基本的にMCの変換を、後者はretrackingしたMCやZMUMU dataなどを作るためのものです。
R22でのbsrdo→aodはテクニカル的に困ったところもあるので、もしまだ私のファイルが存在したら、distrk_dev/distrk_rdo2aod4/scripts以下のZMUMU関連あるいはscript_analy関連のファイルを保持しておくとよいです。
R22のRDO→AOD工程では既に実装した消失飛跡トリガーが再構成からBDT selectionまで行っています。
再構成は、athena/Trigger/TrigAlgrithms/TrigFastTrackFinder/src/TrigFastTrackFinder.cxx　というFTFファイルで行っています。再構成の手順は私の修論やスライドを見ると分かります。
そこで2種類のAOD Container(HLT_DisTrkCandとHLT_DisTrkBDTSel)を作っています。前者は再構成+preselectionまでかけたもので、後者は後段トリガ-+BDT selectionまで
かけ終わったものです。この２つあるいは1つを分析することで、再構成や変数の様子BDTの様子などをモニタリングできます。
BDT tuningをして変化をみたければR21のローカルで変数やパラメータを動かして、という形になります。


そしてもう１つの作業がAOD analysisです。R21の時と違ってreconstructだけでなく、BDTselectionまでの工程がトリガー内（rdoにtirggerを掛ける段階）ですべて終わってるので、基本的にtruth matchingやmake histgramなどに使っています。

## distrk_rdo2aod4 (retraking)
retracking用のTrigFastTrackFinder.cxxはこのgitにあります。以下は補足説明です。（長野さんもある程度分かります）
再構成を行う際にSCTヒットの情報を消すことをretrackingと呼びますが、要するにFTF内でＳＣＴ情報を持ってくるところを持ってこないようにしています。
###space point conversionにおけるSCT veto
Trigger/TrigTools/TrigOnlineSpacePointTool/src/TrigSpacePointConversionTool.cxxというファイルを参照していますが、ここでSCT spacepointを持ってくる箇所があるのでコメントアウトする。
###Combinatrial trackingにおけるSCT veto
track_makerにSCTオプションがあるのでそれをfalseにする。ただしこれだと上手くvertexが再構成されないため、本筋とは別個にSCTオプションをfalseにしたtrack(変数名をInitialTrack2とか適当な名前にしてます)を作って、vertex計算は本筋(SCTありのtrack)を使って、再構成するそのものはSCT vetoしたものを使います。
###Is_fail変数が使えない場合（長野さんに相談）
R22環境下ではなぜかcomb trackingにfailして捨てられるtrackを再取得するのがうまくいっていないみたいです。
そのためInnerDetector/InDetRecEvent/SiSPSeededTrackFinderData/src/SiCombinatorialTrackFinderData_xk.cxx　内における、fail trackを再取得するオプションを常にオンの状態にする必要があります。もしかしたら20220401以降は解決してるかもしれません。長野さんに聞いてください。

## distrk_aodanalysis for R22
Plz refer to git of [distrk_aodanalysis_r22](https://gitlab.cern.ch/ykobayas/distrk_aodanalysis_r22) if you want to understand only basical points.

This project has 2 branches (master and retracking), and basically you will use the master branch for R22 analysis, but if you try to analyse retracking AOD, you should use the retracking branch.
However, retracking source code for analysis is imcomplete now(20220331), so they will output alot of errors if not change.

## Root file
signal(chargino MC)のBDT validation rootファイルです。BDT selectionをどれだけ強めたかでTight ~ VeryLooseまで用意してあります。
中身をみることで、現状のBDTでの信号効率やトリガー効率、BDT distributionなどを見れます。
