// UTTAnalysis includes

#include <string>
#include <map>
#include <vector>

#include "TString.h"

#include "UTTAnalysis/UTTAnalysisAlg.h"

#include "xAODEventInfo/EventInfo.h"
#include "xAODTrigger/TrigCompositeContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODTrigMissingET/TrigMissingETContainer.h"

// ================================================================================================
// ================================================================================================

UTTAnalysisAlg::UTTAnalysisAlg( const std::string& name, ISvcLocator* pSvcLocator ) :
   AthAlgorithm( name, pSvcLocator ),
   m_grlTool ("GoodRunsListSelectionTool/grl", this),
   m_configSvc( "TrigConf::TrigConfigSvc/TrigConfigSvc", name )
   //m_ofs_signal("signal.ascii"),
   //m_ofs_background("background.ascii")

{
   /** signal mc mode */
   declareProperty("IsSignalMC", m_is_signal_mc = true);

   /** EB data mode */
   declareProperty("IsEBData",    m_is_eb_data = false);

   /** debug mode */
   declareProperty("IsDebugMode", m_is_debug_mode = true);
}

UTTAnalysisAlg::~UTTAnalysisAlg() {}

// ================================================================================================
// ================================================================================================

StatusCode UTTAnalysisAlg::initialize()
{
   ATH_MSG_INFO ("Initializing " << name() << "...");

   m_isMC = true;
   if( m_is_eb_data ) m_isMC = false;

   // GRL Tools
   ATH_CHECK( m_grlTool.retrieve() );

   // Trigger Tools
   ATH_CHECK(m_configSvc.retrieve());
   ATH_CHECK(m_trigDecTool.retrieve());
   if(m_trigDecTool->getNavigationFormat() == "TrigComposite") m_run3 = true;
   ATH_MSG_INFO("TrigDecTool->getNavigationFormat() == " << m_trigDecTool->getNavigationFormat());
   //m_trigDecTool->ExperimentalAndExpertMethods()->enable();

   // EventInfo
   ATH_CHECK( m_EventInfoKey.initialize() );
   ATH_CHECK( m_TruthEventKey.initialize() );
   // Vertex
   // ATH_CHECK( m_vertexKey.initialize() );
   // DisTrkCand keys
   ATH_CHECK( m_disTrkCandKey.initialize() );
   // DisTrkBDTSel keys
   ATH_CHECK( m_disTrkBDTSelKey.initialize() );

   // Histogram
   ATH_CHECK( m_histMgr.initialize("hist.root") );
   //
   if( m_isMC ) ATH_CHECK( m_histMgr.book_chargino_info_base() );
   ATH_CHECK( m_histMgr.book_distrk_info_base("disTrkCand_presel") );
   if( m_isMC ) ATH_CHECK( m_histMgr.book_distrk_info_base("disTrkCand_presel_match_to_chg") );
   if( m_isMC ) ATH_CHECK( m_histMgr.book_distrk_info_base("disTrkCand_presel_unmatched_to_chg") );
   if( m_isMC ) ATH_CHECK( m_histMgr.book_distrk_info_base("disTrkCand_bdt_match_to_chg") );
   ATH_CHECK( m_histMgr.book_distrk_n_info("disTrkCand") );
   //
   return StatusCode::SUCCESS;
}

// ================================================================================================
// ================================================================================================

StatusCode UTTAnalysisAlg::finalize()
{
   ATH_MSG_INFO ("Finalizing " << name() << "...");
   ATH_MSG_INFO ("finalize(): Number of clean events = " << m_numCleanEvents);

   // Histogram
   ATH_CHECK( m_histMgr.finalize() );

   return StatusCode::SUCCESS;
}

// ================================================================================================
// ================================================================================================

StatusCode UTTAnalysisAlg::execute()
{ 
   const std::string fname = "UTTAnalysisAlg::execute> ";

   ATH_MSG_DEBUG ("Executing " << name() << "...");
   setFilterPassed(false);

   const EventContext& ctx = getContext();
   ATH_MSG_INFO("Get event context << " << ctx );

   // event info
   SG::ReadHandle<xAOD::EventInfo> eventInfo(m_EventInfoKey, ctx);
   uint32_t runNumber = eventInfo->runNumber();
   unsigned long long eventNumber = eventInfo->eventNumber();
   int lumiBlock = eventInfo-> lumiBlock();
   double averageInteractionsPerCrossing =  eventInfo -> averageInteractionsPerCrossing();
   ATH_CHECK(m_histMgr.fill_h1_each("aveIntPerCross", averageInteractionsPerCrossing));

   ATH_MSG_INFO("===================================================");
   ATH_MSG_INFO("eventNumber==========#" << eventNumber << "========" );
   ATH_MSG_INFO("===================================================");
   ATH_MSG_INFO("Run = " << runNumber << " : LB = " << lumiBlock << " : mu = " << averageInteractionsPerCrossing );
   m_isMC = eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION );
   ATH_MSG_INFO("isMC = " << m_isMC );

   // reject event if problematic data events
   if (!m_isMC) {
      if ((eventInfo->errorState(xAOD::EventInfo::LAr)  == xAOD::EventInfo::Error) ||
	  (eventInfo->errorState(xAOD::EventInfo::Tile) == xAOD::EventInfo::Error) ||
	  (eventInfo->errorState(xAOD::EventInfo::SCT)  == xAOD::EventInfo::Error) ||
	  (eventInfo->isEventFlagBitSet(xAOD::EventInfo::Core, 18))) {
	 ATH_MSG_INFO("Rejected as LAr/Tile/SCT problematic event");
	 return StatusCode::SUCCESS;
      }
   }
   m_numCleanEvents++;

   // GRL
   if ( !m_isMC && !m_is_eb_data ) {
      if (!m_grlTool->passRunLB(*eventInfo)) {
	 ATH_MSG_INFO("Rejected due to GRL");
	 return StatusCode::SUCCESS;
      }
   }

   // TrigDecTool
   auto cgs = m_trigDecTool->getChainGroup("HLT_.*unconv.*");

   for( auto &trig : cgs->getListOfTriggers() ) {
      auto cg = m_trigDecTool->getChainGroup(trig);
      bool isPassedCurrent = cg->isPassed( TrigDefs::eventAccepted );
      ATH_MSG_INFO(trig << " is passed ===> " << isPassedCurrent);
    }
   // DisTrkCand EDMs
   const xAOD::TrigCompositeContainer* Container = 0;
   SG::ReadHandle<xAOD::TrigCompositeContainer> disTrkCand(m_disTrkCandKey, ctx);
   ATH_CHECK( disTrkCand.isValid() );
   Container = disTrkCand.get();
   ATH_MSG_INFO("disTrkCand Container: size=" << Container->size());
   ATH_CHECK(m_histMgr.fill_h1_each("disTrkCand_n", Container->size()));
   for ( const xAOD::TrigComposite* ct : *Container ) {
      ATH_MSG_INFO("disTrkCand_eta=" << ct->getDetail<float>("disTrkCand_eta"));
      ATH_MSG_INFO("disTrkCand_pt=" << ct->getDetail<float>("disTrkCand_pt"));
      ATH_MSG_INFO("disTrkCand_refit_pt=" << ct->getDetail<float>("disTrkCand_refit_pt"));
      ATH_CHECK(m_histMgr.fill_h1_each("disTrkCand_category",   (double)ct->getDetail<int>("disTrkCand_category")));
      ATH_CHECK(m_histMgr.fill_h1_each("disTrkCand_eta",        (double)ct->getDetail<float>("disTrkCand_eta")));
      ATH_CHECK(m_histMgr.fill_h1_each("disTrkCand_phi",        (double)ct->getDetail<float>("disTrkCand_phi")));
      ATH_CHECK(m_histMgr.fill_h1_each("disTrkCand_pt",         (double)ct->getDetail<float>("disTrkCand_pt")/1000.0));
      ATH_CHECK(m_histMgr.fill_h1_each("disTrkCand_refit_pt",   (double)ct->getDetail<float>("disTrkCand_refit_pt")/1000.0));
      ATH_CHECK(m_histMgr.fill_h1_each("disTrkCand_n_hits_pix", (double)ct->getDetail<int>("disTrkCand_n_hits_pix")));
      ATH_CHECK(m_histMgr.fill_h1_each("disTrkCand_n_hits_sct", (double)ct->getDetail<int>("disTrkCand_n_hits_sct")));
      ATH_CHECK(m_histMgr.fill_h1_each("disTrkCand_is_fail",    (double)ct->getDetail<int>("disTrkCand_is_fail")));
      ATH_CHECK(m_histMgr.fill_h1_each("disTrkCand_d0_wrtVtx",        (double)ct->getDetail<float>("disTrkCand_d0_wrtVtx")));
      ATH_CHECK(m_histMgr.fill_h1_each("disTrkCand_z0_wrtVtx",        (double)ct->getDetail<float>("disTrkCand_z0_wrtVtx")));
      ATH_CHECK(m_histMgr.fill_h1_each("disTrkCand_refit_d0_wrtVtx",        (double)ct->getDetail<float>("disTrkCand_refit_d0_wrtVtx")));
      ATH_CHECK(m_histMgr.fill_h1_each("disTrkCand_refit_z0_wrtVtx",        (double)ct->getDetail<float>("disTrkCand_refit_z0_wrtVtx")));
      ATH_CHECK(m_histMgr.fill_h1_each("disTrkCand_n_brhits_sct1", (double)ct->getDetail<int>("disTrkCand_n_brhits_sct1")));
      ATH_CHECK(m_histMgr.fill_h1_each("disTrkCand_n_brhits_sct2", (double)ct->getDetail<int>("disTrkCand_n_brhits_sct2")));
      ATH_CHECK(m_histMgr.fill_h1_each("disTrkCand_n_brhits_sct3", (double)ct->getDetail<int>("disTrkCand_n_brhits_sct3")));
      ATH_CHECK(m_histMgr.fill_h1_each("disTrkCand_n_brhits_sct4", (double)ct->getDetail<int>("disTrkCand_n_brhits_sct4")));
      ATH_CHECK(m_histMgr.fill_h1_each("disTrkCand_iso3_dr01", (double)ct->getDetail<float>("disTrkCand_iso3_dr01")));
      ATH_CHECK(m_histMgr.fill_h1_each("disTrkCand_iso3_dr02", (double)ct->getDetail<float>("disTrkCand_iso3_dr02")));
      //ATH_CHECK(m_histMgr.fill_h1_each("disTrkCand_chi2", (double)ct->getDetail<float>("disTrkCand_chi2")));
      int sct_1 = (double)ct->getDetail<int>("disTrkCand_n_brhits_sct1");
      int sct_2 = (double)ct->getDetail<int>("disTrkCand_n_brhits_sct2");
      int sct_3 = (double)ct->getDetail<int>("disTrkCand_n_brhits_sct3");
      int sct_4 = (double)ct->getDetail<int>("disTrkCand_n_brhits_sct4");
      int sct_all = sct_1 + sct_2 + sct_3 + sct_4;
      ATH_CHECK(m_histMgr.fill_h1_each("disTrkCand_n_brhits_sct_all",sct_all));
   }
   // DisTrkBDTSel EDMs
   const xAOD::TrigCompositeContainer* compContainer = 0;
   
   SG::ReadHandle<xAOD::TrigCompositeContainer> disTrkBDTSel(m_disTrkBDTSelKey, ctx);
   ATH_CHECK( disTrkBDTSel.isValid() );
   compContainer = disTrkBDTSel.get();
   ATH_MSG_INFO("disTrkBDTSel Container: size=" << compContainer->size());
   ATH_CHECK(m_histMgr.fill_h1_each("disTrkBDT_n", compContainer->size()));
   for ( const xAOD::TrigComposite* tc : *compContainer ) {
      ATH_MSG_INFO("disTrkBDT_eta=" << tc->getDetail<float>("disTrk_eta"));
      ATH_MSG_INFO("disTrkBDT_pt=" << tc->getDetail<float>("disTrk_pt"));
      ATH_MSG_INFO("disTrkBDT_refit_pt=" << tc->getDetail<float>("disTrk_refit_pt"));
      //ATH_MSG_INFO("disTrk_is_fail=" << tc->getDetail<int>("disTrk_is_fail"));
      //ATH_MSG_INFO("disTrk_bdtscore=" << tc->getDetail<int>("disTrk_bdtscore"));
      ATH_CHECK(m_histMgr.fill_h1_each("disTrkBDT_category",  tc->getDetail<int>("disTrk_category")));
      ATH_CHECK(m_histMgr.fill_h1_each("disTrkBDT_eta",        (double)tc->getDetail<float>("disTrk_eta")));
      ATH_CHECK(m_histMgr.fill_h1_each("disTrkBDT_phi",        (double)tc->getDetail<float>("disTrk_phi")));
      ATH_CHECK(m_histMgr.fill_h1_each("disTrkBDT_pt",         (double)tc->getDetail<float>("disTrk_pt")/1000.0));
      ATH_CHECK(m_histMgr.fill_h1_each("disTrkBDT_refit_pt",   (double)tc->getDetail<float>("disTrk_refit_pt")/1000.0)); 
      ATH_CHECK(m_histMgr.fill_h1_each("disTrkBDT_is_fail",   (double)tc->getDetail<int>("disTrk_is_fail")));
      ATH_CHECK(m_histMgr.fill_h1_each("disTrkBDT_bdtscore",   (double)tc->getDetail<float>("disTrk_bdtscore")));
      ATH_CHECK(m_histMgr.fill_h1_each("disTrkBDT_n_hits_pix",   (double)tc->getDetail<int>("disTrk_n_hits_pix")));
      ATH_CHECK(m_histMgr.fill_h1_each("disTrkBDT_n_hits_sct",   (double)tc->getDetail<int>("disTrk_n_hits_sct")));
      ATH_CHECK(m_histMgr.fill_h1_each("disTrkBDT_d0_wrtVtx",   (double)tc->getDetail<float>("disTrk_d0_wrtVtx")));
      ATH_CHECK(m_histMgr.fill_h1_each("disTrkBDT_z0_wrtVtx",   (double)tc->getDetail<float>("disTrk_z0_wrtVtx")));
      ATH_CHECK(m_histMgr.fill_h1_each("disTrkBDT_iso3_dr01", (double)tc->getDetail<float>("disTrk_iso3_dr01")));
      ATH_CHECK(m_histMgr.fill_h1_each("disTrkBDT_iso3_dr02", (double)tc->getDetail<float>("disTrk_iso3_dr02")));
  }

   std::cout << fname << "Truth processing start." << std::endl;
   //Truth 
   const xAOD::TruthEventContainer* xTruthEventContainer = NULL;
   if( m_isMC ) CHECK( evtStore()->retrieve( xTruthEventContainer, "TruthEvents"));
 
   std::vector<TVector3> v_chg_all_p3;
   std::vector<TVector3> v_chg_all_decay;
   std::vector<TVector3> v_chg_all_prod;
   if( m_isMC && m_is_signal_mc ) {
      if(! m_histMgr.getTruthChargino(xTruthEventContainer, v_chg_all_p3, v_chg_all_prod, v_chg_all_decay)) {
         ATH_MSG_ERROR("getTruthChargino returned error");
      }
   }

   if( m_isMC ) {
      ATH_CHECK(m_histMgr.fill_h1_each("chg_n_all",v_chg_all_decay.size()));
   }
   float chg_twosyst_pt = 0;
   if( v_chg_all_decay.size()==2 ) { chg_twosyst_pt = (v_chg_all_p3[0]+v_chg_all_p3[1]).Pt(); }
   if( m_is_debug_mode ) std::cout << fname << "pT of two chargino system = " << chg_twosyst_pt << std::endl;
   std::vector<TVector3> v_chg_p3;
   std::vector<TVector3> v_chg_decay;
   std::vector<TVector3> v_chg_prod;
   for(unsigned int i_chg=0; i_chg<v_chg_all_decay.size(); i_chg++) {
      TVector3 chg_decay = v_chg_all_decay[i_chg];
      float chg_decay_r = chg_decay.Perp();
      ATH_CHECK(m_histMgr.fill_h1_each("chg_decay_r_all",chg_decay_r));
      TVector3 chg_p3 = v_chg_all_p3[i_chg];
      ATH_CHECK(m_histMgr.fill_h1_each("chg_pt_all", hist_pt(chg_p3.Pt())));
      ATH_CHECK(m_histMgr.fill_h1_each("chg_eta_all",chg_p3.Eta()));
      if( ! m_histMgr.is_target_chg(chg_decay_r, chg_p3) ) continue;
      v_chg_p3.push_back(chg_p3);
      v_chg_decay.push_back(chg_decay);
      v_chg_prod.push_back(v_chg_all_prod[i_chg]);
   }
   if( m_isMC ) {
      fill_chargino_info_base(v_chg_p3,v_chg_decay,chg_twosyst_pt);
   }
   if( m_isMC && m_is_signal_mc && v_chg_p3.size() < 1 )  return StatusCode::SUCCESS;

   //if( m_isMC && m_is_signal_mc && m_is_debug_mode ) std::cout << fname << "chargino cand: ===========================>" << std::endl;
   for(unsigned int i_chg=0; i_chg<v_chg_p3.size(); i_chg++) {
      TVector3 chg_p3 = v_chg_p3[i_chg];
      if( m_is_debug_mode ) std::cout << fname << "i_chg = " << i_chg << ": pt / eta / phi = " << chg_p3.Pt() << " / " << chg_p3.Eta() << " / " << chg_p3.Phi() << std::endl;
   }

   
/*
   if( !m_isMC && m_is_eb_data ) {
      if( m_is_debug_mode ) std::cout << fname << "EB data : Event selection with L1" << std::endl;
      bool fire_xe50 = m_map_l1_results["XE50"];
      if( m_is_debug_mode ) std::cout << fname << "... XE50 / J100 / 4J15 = " << fire_xe50 << " / " << fire_j100 << " / " << fire_4j15 << std::endl;
      if( !fire_xe50 ) {
	 if( m_is_debug_mode ) std::cout << fname << "==> No further process" << std::endl;
	 return StatusCode::SUCCESS;
      }
   }
   // HLT MET container (needed for emulation)   
   const std::string containerName = "HLT_xAOD__TrigMissingETContainer_TrigEFMissingET_topocl_PUC";
   const xAOD::TrigMissingETContainer* hltCont(0);
   CHECK( evtStore()->retrieve(hltCont, containerName) );
   float ex = hltCont->front()->ex();
   float ey = hltCont->front()->ey();
   float hltMet = sqrt(ex*ex + ey*ey);
   if( m_is_debug_mode ) std::cout << fname << "HLT ETmiss = " << hltMet/1000.0 << std::endl;
   // HLT decision (+emulation) 
   m_map_hlt_results.clear();
   if( m_is_debug_mode ) std::cout << fname << "HLT trigger items:" << std::endl;
   for(unsigned int i=0; i<m_v_hlt_chains.size(); i++) {
      std::string name = m_v_hlt_chains[i];
      bool is_passed = false;
      float hltMetGeV = hltMet/1000.0;
      if     ( name ==  "xe60_pufit_L1XE35" ) { is_passed = (hltMetGeV >  60.0 && m_map_l1_results["XE35"]); }
      else if( name ==  "xe70_pufit_L1XE35" ) { is_passed = (hltMetGeV >  70.0 && m_map_l1_results["XE35"]); }
      else if( name ==  "xe80_pufit_L1XE35" ) { is_passed = (hltMetGeV >  80.0 && m_map_l1_results["XE35"]); }
      else if( name ==  "xe90_pufit_L1XE35" ) { is_passed = (hltMetGeV >  90.0 && m_map_l1_results["XE35"]); }
      else if( name == "xe100_pufit_L1XE35" ) { is_passed = (hltMetGeV > 100.0 && m_map_l1_results["XE35"]); }
      else if( name == "xe110_pufit_L1XE35" ) { is_passed = (hltMetGeV > 110.0 && m_map_l1_results["XE35"]); }
      
      else if( name ==  "xe60_pufit_L1XE50" ) { is_passed = (hltMetGeV >  60.0 && m_map_l1_results["XE50"]); }
      else if( name ==  "xe70_pufit_L1XE50" ) { is_passed = (hltMetGeV >  70.0 && m_map_l1_results["XE50"]); }
      else if( name ==  "xe80_pufit_L1XE50" ) { is_passed = (hltMetGeV >  80.0 && m_map_l1_results["XE50"]); }
      else if( name ==  "xe90_pufit_L1XE50" ) { is_passed = (hltMetGeV >  90.0 && m_map_l1_results["XE50"]); }
      else if( name == "xe100_pufit_L1XE50" ) { is_passed = (hltMetGeV > 100.0 && m_map_l1_results["XE50"]); }
      else if( name == "xe110_pufit_L1XE50" ) { is_passed = (hltMetGeV > 110.0 && m_map_l1_results["XE50"]); }
      else {
           if( m_tdt->isPassed(("HLT_"+name).c_str()) ) { is_passed = true; }
    }
      if( is_passed ) {
           m_h1_hlt_counts->Fill(i+0.1);
           if( m_is_debug_mode ) std::cout << fname << "..." << name << " passed" << std::endl;
      }
      m_map_hlt_results.insert(std::make_pair(name,is_passed));
    }

   // trigger info with BDT
   int n_bdt = map_disTrkCand_n[prefix+"_n_bdt"];
   if( m_is_debug_mode ) std::cout << fname << "L1 trigger items:" << std::endl;
   for(unsigned int i=0; i<m_v_l1_items.size(); i++) {
      std::string name = m_v_l1_items[i];
      bool is_passed = m_map_l1_results[name];
      if( is_passed && n_bdt > 0 ) {
	 m_h1_l1_bdt_counts->Fill(i+0.1);
	 if( m_is_debug_mode ) std::cout << fname << "..." << name << " BDT passed" << std::endl;
      }
   }

   if( m_is_debug_mode ) std::cout << fname << "HLT trigger items:" << std::endl;
   for(unsigned int i=0; i<m_v_hlt_chains.size(); i++) {
      std::string name = m_v_hlt_chains[i];
      bool is_passed = m_map_hlt_results[name];
      if( is_passed && n_bdt > 0 ) {
	 m_h1_hlt_bdt_counts->Fill(i+0.1);
	 if( m_is_debug_mode ) std::cout << fname << "..." << name << " BDT passed" << std::endl;
      }
   }   
   // trigger as a function of chargino twosystem mass
   if( m_isMC ) {
      //if( m_map_l1_results["XE35"] ) { fill_h1_each("chg_twosyst_pt_L1XE35", chg_twosyst_pt); }
      //if( m_map_l1_results["XE40"] ) { fill_h1_each("chg_twosyst_pt_L1XE40", chg_twosyst_pt); }
      if( m_map_l1_results["XE50"] ) { ATH_CHECK(m_histMgr.fill_h1_each("chg_twosyst_pt_L1XE50", chg_twosyst_pt)); }
      if( m_map_hlt_results["xe110_pufit_L1XE50"] ) { ATH_CHECK(m_histMgr.fill_h1_each("chg_twosyst_pt_xe110", chg_twosyst_pt)); }
      if( m_map_hlt_results["xe110_pufit_L1XE50"] || (m_map_hlt_results["xe80_pufit_L1XE50"] && n_bdt > 0)  ) {
	    ATH_CHECK(m_histMgr.fill_h1_each("chg_twosyst_pt_xe110_or_xe80bdt", chg_twosyst_pt));
	 }
      if( m_map_hlt_results["xe110_pufit_L1XE50"] || (m_map_hlt_results["xe60_pufit_L1XE35"] && n_bdt > 0)  ) {
	    ATH_CHECK(m_histMgr.fill_h1_each("chg_twosyst_pt_xe110_or_xe60bdt", chg_twosyst_pt));
	 }
      }
   }
*/
   //-------
   //-------1st loop on DisTrkCand

   std::string prefix = "disTrkCand";
   std::string prefix_bdt = "disTrk";
   std::string match  = "_match_to_chg";

   std::vector<std::string> v_cat;
   v_cat.push_back("pix4l_sct0");v_cat.push_back("pix4l_sct1p");
   v_cat.push_back("pix3l_sct0");v_cat.push_back("pix3l_sct1p");
   
   // nr of disTrkCand tracks
   std::map<std::string, int> map_disTrkCand_n;
   map_disTrkCand_n.insert(std::make_pair(prefix+"_n",      0));
   map_disTrkCand_n.insert(std::make_pair(prefix+"_n"+match,0));
   map_disTrkCand_n.insert(std::make_pair(prefix+"_n_presel",      0));
   map_disTrkCand_n.insert(std::make_pair(prefix+"_n_presel"+match,0));
   map_disTrkCand_n.insert(std::make_pair(prefix+"_n_bdt",      0));
   map_disTrkCand_n.insert(std::make_pair(prefix+"_n_bdt"+match,0));
   for(auto it=v_cat.begin(); it!=v_cat.end(); it++) {
      map_disTrkCand_n.insert(std::make_pair(prefix+"_n_"+(*it),      0));
      map_disTrkCand_n.insert(std::make_pair(prefix+"_n_"+(*it)+match,0));
      map_disTrkCand_n.insert(std::make_pair(prefix+"_n_"+(*it)+"_presel",      0));
      map_disTrkCand_n.insert(std::make_pair(prefix+"_n_"+(*it)+"_presel"+match,0));
      map_disTrkCand_n.insert(std::make_pair(prefix+"_n_"+(*it)+"_bdt",         0));
      map_disTrkCand_n.insert(std::make_pair(prefix+"_n_"+(*it)+"_bdt"+match,   0));
   }

   //
   char buf[126];
   std::map<std::string, int> map_chg_n_match_disTrkCand;
   for(int i=0; i<(int)v_chg_p3.size(); i++) {
      sprintf(buf,"chg%i_",i);
      map_chg_n_match_disTrkCand.insert(std::make_pair(std::string(buf)+prefix+"_n",0));
      map_chg_n_match_disTrkCand.insert(std::make_pair(std::string(buf)+prefix+"_n_presel",0));
      map_chg_n_match_disTrkCand.insert(std::make_pair(std::string(buf)+prefix+"_n_bdt",0));
   }
   //
   // DisTrkCand 

   unsigned int disTrkCand_idx=0;
   unsigned int disTrkBDT_idx=0;
   std::map<unsigned int, std::map<std::string, float> > map_disTrkCand_varf;
   std::map<unsigned int, std::map<std::string, int> >   map_disTrkCand_vari;
   //
   //Only Truth
   xAOD::TruthEventContainer::const_iterator itr;
   for (itr = xTruthEventContainer->begin(); itr!=xTruthEventContainer->end(); ++itr) {
      std::map<std::string, float> varf;
      std::map<std::string, int>   vari;
      int idx_of_chg = 999;
      map_disTrkCand_n[prefix+"_n"]++;
      //if( v_chg_all_decay.size()!=0 )
      map_disTrkCand_n[prefix+"_n"+match]++;
      sprintf(buf,"chg%i_",idx_of_chg);map_chg_n_match_disTrkCand[std::string(buf)+prefix+"_n"]++;
      //fill_distrk_info_base(prefix,prefix+match,ct,pvtx_z,varf,vari,map_goodTrk_varf,false,m_ofs_signal);
      m_histMgr.fill_h1_each("truth_chg",map_chg_n_match_disTrkCand[std::string(buf)+prefix+"_n"]);   
      std::cout << " -> " << "Truth tracks mapping finish!\n";
   }
   
   //DisTrkCand track(after Preselection)
   for ( const xAOD::TrigComposite* ct : *Container ) {	       
      //std::map<std::string, float> varf;
      //std::map<std::string, int>   vari;
      //get_distrk(prefix,ct,varf,vari);
      std::cout << " -> " << "DisTrkCand tracks mapping start\n";
      //int cat=0;
      //ct->getDetail(prefix+"_category",cat);
      int cat = ct->getDetail<int>(prefix+"_category");
      map_disTrkCand_n[prefix+"_n_presel"]++;
      std::cout << " cat = "<< cat << "  ->" << "DisTrkCand tracks mapping in the middle\n";
      if( cat!=0 ) map_disTrkCand_n[prefix+"_n_"+v_cat[cat-1]+"_presel"]++;
      float dr_to_chg = 999; int idx_of_chg = 999;
      bool is_match_to_chargino = m_histMgr.is_distrk_match_to_chg(prefix, ct, v_chg_p3, dr_to_chg, idx_of_chg);
      m_histMgr.fill_h1_each("dr_to_chg",dr_to_chg);
      fill_distrk_info_base(prefix,prefix+"_presel",ct);
      if( ! is_match_to_chargino ) {std::cout << " -> " << dr_to_chg << "  ->" << "DisTrkCand do not match T^T\n";}
      if( is_match_to_chargino ) {
         std::cout << " -> " << "Pass bool of is_match_to_chargino ^o^\n";
         map_disTrkCand_n[prefix+"_n_presel"+match]++;
         if( cat!=0 ) map_disTrkCand_n[prefix+"_n_"+v_cat[cat-1]+"_presel"+match]++;
	 sprintf(buf,"chg%i_",idx_of_chg); map_chg_n_match_disTrkCand[std::string(buf)+prefix+"_n_presel"]++;
	 fill_distrk_info_base(prefix,prefix+"_presel"+match,ct);
       }
       else if( dr_to_chg > 3.0 ) {
         fill_distrk_info_base(prefix,prefix+"_presel_unmatched_to_chg",ct);
       }
       disTrkCand_idx++;
       std::cout << "  -> " << "DisTrkCand tracks mapping finish!\n";
   }

   //DisTrkBDTSel tracks
   for ( const xAOD::TrigComposite* tc : *compContainer ) {
      int cat = tc->getDetail<int>(prefix_bdt+"_category");
      map_disTrkCand_n[prefix+"_n_bdt"]++;
      std::cout << " cat = "<< cat << "  ->" << "DisTrkBDT tracks mapping in the middle\n";
      if( cat!=0 ) map_disTrkCand_n[prefix+"_n_"+v_cat[cat-1]+"_bdt"]++;
      float dr_to_chg = 999; int idx_of_chg = 999;
      bool is_match_to_chargino = m_histMgr.is_distrk_match_to_chg(prefix_bdt, tc, v_chg_p3, dr_to_chg, idx_of_chg);
      m_histMgr.fill_h1_each("dr_to_chg_bdt",dr_to_chg);
      if( is_match_to_chargino ) {
         std::cout << " -> " << "BDT Pass bool of is_match_to_chargino ^o^\n";
         map_disTrkCand_n[prefix+"_n_bdt"+match]++;
         if( cat!=0 ) map_disTrkCand_n[prefix+"_n_"+v_cat[cat-1]+"_bdt"+match]++;
         sprintf(buf,"chg%i_",idx_of_chg); map_chg_n_match_disTrkCand[std::string(buf)+prefix+"_n_bdt"]++;
         fill_distrk_info_base("disTrk",prefix+"_bdt"+match,tc);
       }
       //else if( dr_to_chg > 3.0 ) {
         //fill_distrk_info_base(prefix,prefix+"_presel_unmatched_to_chg",tc);
       //}
       disTrkBDT_idx++;
   }

   //
   //

   fill_distrk_n_info(prefix,map_disTrkCand_n);
   std::cout << "  -> " << "disTrkCand_idx = " << disTrkCand_idx << "  disTrkBDT_idx = " << disTrkBDT_idx << "\n";
   if( m_isMC ) {
      for(unsigned int i=0; i<v_chg_p3.size(); i++) {
	 sprintf(buf,"chg%i_",i); 
	 std::cout << fname << "Nr of matched disTrkCand for chargino (idx: " << i << ") : " << map_chg_n_match_disTrkCand[std::string(buf)+prefix+"_n"];
	 std::cout << " -> " << map_chg_n_match_disTrkCand[std::string(buf)+prefix+"_n_presel"] << " (presel)";
      }
   }
   if( m_isMC ) fill_chargino_vs_distrk_info(prefix,v_chg_p3,v_chg_decay,map_chg_n_match_disTrkCand);

   //---------------------
   //---------------------
   std::cout << " ============ " << "Excution finish! =============\n\n";
   setFilterPassed(true); //if got here, assume that means algorithm passed
   return StatusCode::SUCCESS;
}
//---------------------------
//---------------------------
//---------------------------
void UTTAnalysisAlg::fill_chargino_info_base(const std::vector<TVector3>& v_chg_p3,
					   const std::vector<TVector3>& v_chg_decay,
					   float chg2_pt)
{
   m_histMgr.fill_h1_each("chg_n",          v_chg_p3.size());
   m_histMgr.fill_h1_each("chg_twosyst_pt", chg2_pt);

   for(unsigned int i_chg=0; i_chg<v_chg_p3.size(); i_chg++) {
      TVector3 decay_chg = v_chg_decay[i_chg];
      float chg_decay_r = decay_chg.Perp();
      m_histMgr.fill_h1_each("chg_decay_r", chg_decay_r);
      //m_histMgr.fill_h1_each("chg_decay_x", decay_chg.X());
      //m_histMgr.fill_h1_each("chg_decay_y", decay_chg.Y());
      //m_histMgr.fill_h1_each("chg_decay_z", decay_chg.Z());
      
      TVector3 p3_chg = v_chg_p3[i_chg];
      m_histMgr.fill_h1_each("chg_pt",  hist_pt(p3_chg.Pt()));
      m_histMgr.fill_h1_each("chg_eta", p3_chg.Eta());
      m_histMgr.fill_h1_each("chg_phi", p3_chg.Phi());
   }
}
//-------
//-------
float UTTAnalysisAlg::hist_pt(float pt) {
   if( pt > HIST_PT_MAX ) return HIST_PT_MAX;
   return pt;
}

float UTTAnalysisAlg::hist_chi2(float chi2) {
   if( chi2 > HIST_CHI2_MAX ) return HIST_CHI2_MAX;
   return chi2;
}

float UTTAnalysisAlg::hist_chi2ndof(float chi2ndof) {
   if( chi2ndof > HIST_CHI2NDOF_MAX ) return HIST_CHI2NDOF_MAX;
   return chi2ndof;
}

float UTTAnalysisAlg::hist_refit_ratio(float ratio) {
   if( ratio > HIST_REFIT_RATIO_MAX ) return HIST_REFIT_RATIO_MAX;
   return ratio;
}
float UTTAnalysisAlg::get_distrk_n_hist(int n) 
{
   if( n > HIST_N_DISTRK_MAX ) return float(HIST_N_DISTRK_MAX);
   return float(n);
}


//---------
//---------

void UTTAnalysisAlg::fill_chargino_vs_distrk_info(const std::string& prefix, 
						const std::vector<TVector3>& v_chg_p3, const std::vector<TVector3>& v_chg_decay,
						std::map<std::string, int>& map_chg_n_match_disTrkCand)
{
   std::string prefix_hist = "chg_vs_" + prefix;

   std::vector<std::string> v_names;
   v_names.push_back("_all");
   v_names.push_back("_clean");
   v_names.push_back("_presel");
   v_names.push_back("_bdt");

   for(unsigned int i_chg=0; i_chg<v_chg_p3.size(); i_chg++) {

      TVector3 decay_chg = v_chg_decay[i_chg];
      float chg_decay_r = decay_chg.Perp();
      TVector3 p3_chg = v_chg_p3[i_chg];
      char buf[64];
      sprintf(buf,"chg%i_",i_chg); 

      std::vector<int> v_n;
      v_n.push_back(1);
      for(unsigned int j=1; j<v_names.size(); j++) {
	 std::string name = v_names[j];
	 int n = 0;
	 if( name == "_clean" ) { n = map_chg_n_match_disTrkCand[std::string(buf)+prefix+"_n"]; }
	 else {
	    n = map_chg_n_match_disTrkCand[std::string(buf)+prefix+"_n"+name];
	 }
	 v_n.push_back(n);
      }

      for(unsigned int j=0; j<v_names.size(); j++) {
	 std::string name = v_names[j];
	 if( v_n[j] > 0 ) {
	    m_histMgr.fill_h1_each(prefix_hist+"_chg_decay_r"+name, chg_decay_r);
	    m_histMgr.fill_h1_each(prefix_hist+"_chg_pt"+name,  hist_pt(p3_chg.Pt()));
	    m_histMgr.fill_h1_each(prefix_hist+"_chg_eta"+name, p3_chg.Eta());
	    m_histMgr.fill_h1_each(prefix_hist+"_chg_phi"+name, p3_chg.Phi());
	 }
      }
   }
}

//-------------
//------------

void UTTAnalysisAlg::fill_distrk_n_info(const std::string& prefix, std::map<std::string,int>& map_vari)
{
   std::string match = "_match_to_chg";
   std::vector<std::string> v_names;
   v_names.push_back("_n");
   v_names.push_back("_n_presel");
   v_names.push_back("_n_bdt");
   v_names.push_back("_n"+match);
   v_names.push_back("_n_presel"+match);
   v_names.push_back("_n_bdt"+match);
   for(auto it=v_names.begin(); it!=v_names.end(); it++) {
      std::string name = prefix + (*it);
      m_histMgr.fill_h1_each(name, get_distrk_n_hist(map_vari[name]));
   }

   //
   m_histMgr.fill_h1_each(prefix+"_n_vs_selection",1);           // nevents
   m_histMgr.fill_h1_each(prefix+"_n1_vs_selection",1);          // nevents
   if( m_isMC ) m_histMgr.fill_h1_each(prefix+"_n"+match+"_vs_selection",1);  // nevents
   if( m_isMC ) m_histMgr.fill_h1_each(prefix+"_n1"+match+"_vs_selection",1); // nevents
   for(unsigned int j=0; j<v_names.size()/2; j++) {
      std::string name  = prefix + v_names[j];
      m_histMgr.m_map_h1[prefix+"_n_vs_selection"]->Fill(j+2,map_vari[name]);
      if( map_vari[name] > 0 ) m_histMgr.fill_h1_each(prefix+"_n1_vs_selection", j+2);
      name = prefix + v_names[j+v_names.size()/2];
      if( m_isMC ) m_histMgr.m_map_h1[prefix+"_n"+match+"_vs_selection"]->Fill(j+2,map_vari[name]);
      if( m_isMC && map_vari[name] > 0 ) m_histMgr.fill_h1_each(prefix+"_n1"+match+"_vs_selection", j+2);
   }
}
//-------------
//-------------

float UTTAnalysisAlg::fill_distrk_info_base(const std::string& prefix_aod, const std::string& prefix_hist,const xAOD::TrigComposite* ct)
{
   double chi2_ndof=0;
   float trk_chi2=0; float trk_ndof=0;
   (double)ct->getDetail(prefix_aod+"_chi2",      trk_chi2);
   (double)ct->getDetail(prefix_aod+"_ndof",      trk_ndof);
   chi2_ndof = trk_chi2/trk_ndof;
   m_histMgr.fill_h1_each(prefix_hist+"_pt",            (double)ct->getDetail<float>(prefix_aod+"_pt")/1000.0);
   m_histMgr.fill_h1_each(prefix_hist+"_refit_pt",            (double)ct->getDetail<float>(prefix_aod+"_refit_pt")/1000.0);
   m_histMgr.fill_h1_each(prefix_hist+"_eta",           (double)ct->getDetail<float>(prefix_aod+"_eta"));
   m_histMgr.fill_h1_each(prefix_hist+"_phi",           (double)ct->getDetail<float>(prefix_aod+"_phi"));
   //m_histMgr.fill_h1_each(prefix_hist+"_d0",            (double)ct->getDetail<float>(prefix_aod+"_d0"));
   //m_histMgr.fill_h1_each(prefix_hist+"_z0",            (double)ct->getDetail<float>(prefix_aod+"_z0"));
   m_histMgr.fill_h1_each(prefix_hist+"_d0_wrtVtx",     (double)ct->getDetail<float>(prefix_aod+"_d0_wrtVtx"));
   m_histMgr.fill_h1_each(prefix_hist+"_z0_wrtVtx",     (double)ct->getDetail<float>(prefix_aod+"_z0_wrtVtx"));
   m_histMgr.fill_h1_each(prefix_hist+"_chi2",          hist_chi2(trk_chi2));
   m_histMgr.fill_h1_each(prefix_hist+"_chi2_ndof",     hist_chi2ndof(chi2_ndof));
   //m_histMgr.fill_h1_each(prefix_hist+"_n_hits_inner",  (double)ct->getDetail<int>(prefix_aod+"_n_hits_inner"));
   m_histMgr.fill_h1_each(prefix_hist+"_n_hits_pix",    (double)ct->getDetail<int>(prefix_aod+"_n_hits_pix"));
   m_histMgr.fill_h1_each(prefix_hist+"_n_hits_sct",    (double)ct->getDetail<int>(prefix_aod+"_n_hits_sct"));
   m_histMgr.fill_h1_each(prefix_hist+"_is_fail",       (double)ct->getDetail<int>(prefix_aod+"_is_fail"));
   m_histMgr.fill_h1_each(prefix_hist+"_iso3_dr01",            (double)ct->getDetail<float>(prefix_aod+"_iso3_dr01"));   
   m_histMgr.fill_h1_each(prefix_hist+"_iso3_dr02",            (double)ct->getDetail<float>(prefix_aod+"_iso3_dr02"));
   std::cout << " -> " << "Pass bool of is_match_to_chargino\n"; 
   //category
   int cat=0;
   (double)ct->getDetail(prefix_aod+"_category",cat);
   m_histMgr.fill_h1_each(prefix_hist+"_category",(float)cat);
   // divide into pix layer 4 and 3
   std::vector<std::string> ppfix;
   if( cat==1 ) { ppfix.push_back("_pix4l_sct0"); }
   else if( cat==2 ) { ppfix.push_back("_pix4l_sct1p"); }
   else if( cat==3 ) { ppfix.push_back("_pix3l_sct0"); }
   else              { ppfix.push_back("_pix3l_sct1p"); }
   for(unsigned int j=0; j<ppfix.size(); j++) {
      m_histMgr.fill_h1_each(prefix_hist+ppfix[j]+"_pt",            (double)ct->getDetail<float>(prefix_aod+"_pt")/1000.0);
      m_histMgr.fill_h1_each(prefix_hist+ppfix[j]+"_refit_pt",            (double)ct->getDetail<float>(prefix_aod+"_refit_pt")/1000.0);
      m_histMgr.fill_h1_each(prefix_hist+ppfix[j]+"_eta",           (double)ct->getDetail<float>(prefix_aod+"_eta"));
      m_histMgr.fill_h1_each(prefix_hist+ppfix[j]+"_phi",           (double)ct->getDetail<float>(prefix_aod+"_phi"));
      //m_histMgr.fill_h1_each(prefix_hist+ppfix[j]+"_d0",            (itMyData)->getDetail<float>(prefix_aod+"_d0"));
      //m_histMgr.fill_h1_each(prefix_hist+ppfix[j]+"_z0",            (itMyData)->getDetail<float>(prefix_aod+"_z0"));
      m_histMgr.fill_h1_each(prefix_hist+ppfix[j]+"_d0_wrtVtx",     (double)ct->getDetail<float>(prefix_aod+"_d0_wrtVtx"));
      m_histMgr.fill_h1_each(prefix_hist+ppfix[j]+"_z0_wrtVtx",     (double)ct->getDetail<float>(prefix_aod+"_z0_wrtVtx"));
      m_histMgr.fill_h1_each(prefix_hist+ppfix[j]+"_refit_d0_wrtVtx",     (double)ct->getDetail<float>(prefix_aod+"_refit_d0_wrtVtx"));
      m_histMgr.fill_h1_each(prefix_hist+ppfix[j]+"_refit_z0_wrtVtx",     (double)ct->getDetail<float>(prefix_aod+"_refit_z0_wrtVtx"));
      m_histMgr.fill_h1_each(prefix_hist+ppfix[j]+"_chi2_ndof",     hist_chi2ndof(chi2_ndof));
      //m_histMgr.fill_h1_each(prefix_hist+ppfix[j]+"_n_hits_inner",  (double)ct->getDetail<int>(prefix_aod+"_n_hits_inner"));
      m_histMgr.fill_h1_each(prefix_hist+ppfix[j]+"_n_hits_pix",    (double)ct->getDetail<int>(prefix_aod+"_n_hits_pix"));
      m_histMgr.fill_h1_each(prefix_hist+ppfix[j]+"_n_hits_sct",    (double)ct->getDetail<int>(prefix_aod+"_n_hits_sct"));
      m_histMgr.fill_h1_each(prefix_hist+ppfix[j]+"_is_fail",       (double)ct->getDetail<int>(prefix_aod+"_is_fail"));
      m_histMgr.fill_h1_each(prefix_hist+ppfix[j]+"_iso3_dr01",            (double)ct->getDetail<float>(prefix_aod+"_iso3_dr01"));
      m_histMgr.fill_h1_each(prefix_hist+ppfix[j]+"_iso3_dr02",            (double)ct->getDetail<float>(prefix_aod+"_iso3_dr02"));
   }
   // return
   return cat;

   //return chi2_ndof;   
}

// ================================================================================================
// ================================================================================================

StatusCode UTTAnalysisAlg::beginInputFile() { 
  //example of retrieval of CutBookkeepers: (remember you will need to include the necessary header files and use statements in requirements file)
  //const xAOD::CutBookkeeperContainer* bks = 0;
  //CHECK( inputMetaStore()->retrieve(bks, "CutBookkeepers") );
  //
  //example of IOVMetaData retrieval (see https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/AthAnalysisBase#How_to_access_file_metadata_in_C)
  //float beamEnergy(0); CHECK( retrieveMetadata("/TagInfo","beam_energy",beamEnergy) );
  //std::vector<float> bunchPattern; CHECK( retrieveMetadata("/Digitiation/Parameters","BeamIntensityPattern",bunchPattern) );
  return StatusCode::SUCCESS;
}

// ================================================================================================
// ================================================================================================
