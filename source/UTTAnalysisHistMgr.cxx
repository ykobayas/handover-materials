#include "TTree.h"
#include "TH1.h"
#include "TH2.h"
#include <map>
#include <string>
#include <vector>
#include "TVector3.h"
#include "UTTAnalysis/UTTAnalysisHistMgr.h"
#include "xAODTrigger/TrigCompositeContainer.h"
// ================================================================================================
// ================================================================================================

UTTAnalysisHistMgr::UTTAnalysisHistMgr() {
}

UTTAnalysisHistMgr::~UTTAnalysisHistMgr() {
}

// ================================================================================================
// ================================================================================================

StatusCode UTTAnalysisHistMgr::initialize( std::string histname ) {
   TString etname = histname.c_str();
   m_FILE = new TFile( etname, "recreate" );
/*
   // 
   ATH_CHECK( book_h1_each("aveIntPerCross",105,-0.5,100) );
   ATH_CHECK( book_h1_each("dr_to_chg",102,-0.1,5) );
   ATH_CHECK( book_h1_each("truth_chg",10,0,10) );
   // disTrkCand
   ATH_CHECK( book_h1_each("disTrkCand_n",          20, 0, 20) );
   ATH_CHECK( book_h1_each("disTrkCand_category",    5, 0, 5) );
   ATH_CHECK( book_h1_each("disTrkCand_pt",        100, 0, 200) );
   ATH_CHECK( book_h1_each("disTrkCand_eta",        80,-4.0, 4.0) );
   ATH_CHECK( book_h1_each("disTrkCand_phi",        160,-4.0, 4.0) );
   ATH_CHECK( book_h1_each("disTrkCand_refit_pt",  100, 0, 200) );
   ATH_CHECK( book_h1_each("disTrkCand_n_hits_pix",  5, 1, 6) );
   ATH_CHECK( book_h1_each("disTrkCand_n_hits_sct",  10, 0, 10) );
   ATH_CHECK( book_h1_each("disTrkCand_is_fail",  31, 0, 3.1) );
   ATH_CHECK( book_h1_each("disTrkCand_d0_wrtVtx",  100, -1, 1) );
   ATH_CHECK( book_h1_each("disTrkCand_z0_wrtVtx",  200, -50, 50) );
   ATH_CHECK( book_h1_each("disTrkCand_n_brhits_sct1",  5, 0, 5) );
   ATH_CHECK( book_h1_each("disTrkCand_n_brhits_sct2",  5, 0, 5) );
   ATH_CHECK( book_h1_each("disTrkCand_n_brhits_sct3",  5, 0, 5) );
   ATH_CHECK( book_h1_each("disTrkCand_n_brhits_sct4",  5, 0, 5) );
   ATH_CHECK( book_h1_each("disTrkCand_n_brhits_sct_all",  10, 0, 10) );
   ATH_CHECK( book_h1_each("disTrkCand_iso3_dr01",    10, 0, 5) );
*/
   // disTrkBDTSel
   ATH_CHECK( book_h1_each("disTrkBDT_n",          20, 0, 20) );
   ATH_CHECK( book_h1_each("disTrkBDT_category",    5, 0, 5) );
   ATH_CHECK( book_h1_each("disTrkBDT_pt",        100, 0, 200) );
   ATH_CHECK( book_h1_each("disTrkBDT_eta",        160,-4.0, 4.0) );
   ATH_CHECK( book_h1_each("disTrkBDT_phi",        128,-3.2, 3.2) );
   ATH_CHECK( book_h1_each("disTrkBDT_refit_pt",  100, 0, 200) );
   ATH_CHECK( book_h1_each("disTrkBDT_is_fail",  2, -0.5, 1.5) );
   ATH_CHECK( book_h1_each("disTrkBDT_bdtscore",  10, -1, 1) );
   ATH_CHECK( book_h1_each("disTrkBDT_n_hits_pix",  11, 0, 11) );
   ATH_CHECK( book_h1_each("disTrkBDT_n_hits_sct",  11, 0, 11) );
   ATH_CHECK( book_h1_each("disTrkBDT_d0_wrtVtx",  100, -1, 1) );
   ATH_CHECK( book_h1_each("disTrkBDT_z0_wrtVtx",  200, -10, 10) );
   ATH_CHECK( book_h1_each("disTrkBDT_iso3_dr01",    110, -0.5, 10.5) );
   ATH_CHECK( book_h1_each("disTrkBDT_iso3_dr02",    110, -0.5, 10.5) ); 
   //
   //
   ATH_CHECK( book_basic() );
   //ATH_CHECK( book_chargino_info_base() );
   //ATH_CHECK( book_chargino_vs_distrk_info("disTrkCand") );
   ATH_CHECK( book_distrk_n_info("disTrkCand") );
   ATH_CHECK( book_distrk_info_base("disTrkCand_presel") );
   ATH_CHECK( book_distrk_info_base("disTrkCand_presel_match_to_chg") );
   //ATH_CHECK( book_distrk_info_base("disTrkCand_presel_unmatched_to_chg") );
   return StatusCode::SUCCESS;
}

StatusCode UTTAnalysisHistMgr::finalize() {
  m_FILE->Write();
  return StatusCode::SUCCESS;
}

// ================================================================================================
// ================================================================================================

StatusCode UTTAnalysisHistMgr::book_h1_each(const std::string& name, int nbins, double xmin, double xmax)
{
   const std::string fname = "book_h1_each> ";
   if( m_map_h1.find(name) != m_map_h1.end() ) {
      std::cout << fname << "ERROR: defining hist two times" << std::endl;
      return StatusCode::FAILURE;
   }
   TH1D* h1 = new TH1D(("h1_"+name).c_str(),name.c_str(),nbins,xmin,xmax);
   m_map_h1[name] = h1;
   return StatusCode::SUCCESS;
}

// ------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------

StatusCode UTTAnalysisHistMgr::fill_h1_each(const std::string& name, double value)
{
   const std::string fname = "fill_h1_each> ";
   if( m_map_h1.find(name) == m_map_h1.end() ) {
      std::cout << fname << " ERROR no histgram = " << name << std::endl;
      return StatusCode::FAILURE;
   }
   m_map_h1[name]->Fill(value);
   return StatusCode::SUCCESS;
}
// ====================
// ====================
StatusCode UTTAnalysisHistMgr::book_basic()
{
   ATH_CHECK( book_h1_each("aveIntPerCross",105,-0.5,100) );
   ATH_CHECK( book_h1_each("dr_to_chg",401,-0.01,4) );
   ATH_CHECK( book_h1_each("dr_to_chg_bdt",401,-0.01,4) );
   ATH_CHECK( book_h1_each("truth_chg",10,0,10) );
   ATH_CHECK( book_h1_each("disTrkCand_n",          20, 0, 20) );
   ATH_CHECK( book_h1_each("disTrkCand_category",    5, 0, 5) );
   ATH_CHECK( book_h1_each("disTrkCand_pt",        100, 0, 200) );
   ATH_CHECK( book_h1_each("disTrkCand_eta",        160,-4.0, 4.0) );
   ATH_CHECK( book_h1_each("disTrkCand_phi",        128,-3.2, 3.2) );
   ATH_CHECK( book_h1_each("disTrkCand_refit_pt",  100, 0, 200) );
   ATH_CHECK( book_h1_each("disTrkCand_n_hits_pix",  11, 0, 11) );
   ATH_CHECK( book_h1_each("disTrkCand_n_hits_sct",  11, 0, 11) );
   ATH_CHECK( book_h1_each("disTrkCand_is_fail",  2, -0.5, 1.5) );
   ATH_CHECK( book_h1_each("disTrkCand_d0_wrtVtx",  100, -1, 1) );
   ATH_CHECK( book_h1_each("disTrkCand_z0_wrtVtx",  200, -10, 10) );
   ATH_CHECK( book_h1_each("disTrkCand_refit_d0_wrtVtx",  100, -1, 1) );
   ATH_CHECK( book_h1_each("disTrkCand_refit_z0_wrtVtx",  200, -10, 10) );
   ATH_CHECK( book_h1_each("disTrkCand_n_brhits_sct1",  5, 0, 5) );
   ATH_CHECK( book_h1_each("disTrkCand_n_brhits_sct2",  5, 0, 5) );
   ATH_CHECK( book_h1_each("disTrkCand_n_brhits_sct3",  5, 0, 5) );
   ATH_CHECK( book_h1_each("disTrkCand_n_brhits_sct4",  5, 0, 5) );
   ATH_CHECK( book_h1_each("disTrkCand_n_brhits_sct_all",  11, 0, 11) );
   ATH_CHECK( book_h1_each("disTrkCand_iso3_dr01",    110, -0.5, 54.5) );
   ATH_CHECK( book_h1_each("disTrkCand_iso3_dr02",    110, -0.5, 54.5) );
   return StatusCode::SUCCESS;
}
// ================================================================================================
// ================================================================================================

StatusCode UTTAnalysisHistMgr::book_chargino_vs_distrk_info(const std::string& prefix)
{
   std::string prefix_hist = "chg_vs_" + prefix;   
   std::vector<std::string> v_names;
   v_names.push_back("_all");
   v_names.push_back("_clean");
   v_names.push_back("_presel");
   v_names.push_back("_bdt");

   for(auto it=v_names.begin(); it!=v_names.end(); it++) {
      book_h1_each(prefix_hist+"_chg_decay_r"+(*it),200,0,400);
      book_h1_each(prefix_hist+"_chg_pt"+(*it),     408,-1,101);
      book_h1_each(prefix_hist+"_chg_eta"+(*it),    160,-4,4);
      book_h1_each(prefix_hist+"_chg_phi"+(*it),    128,-3.2,3.2);
   }

   return StatusCode::SUCCESS;
}

//-------
//-------
StatusCode UTTAnalysisHistMgr::book_distrk_info_base(const std::string& prefix)
{
   book_h1_each(prefix+"_pt",         200,0,100);
   book_h1_each(prefix+"_refit_pt",         200,0,100);
   //book_h1_each(prefix+"_d0",         100,-1,1); 
   book_h1_each(prefix+"_d0_wrtVtx",  100,-1,1);
   //book_h1_each(prefix+"_z0",           300,-75,75);
   book_h1_each(prefix+"_z0_wrtVtx",    200,-10,10);
   book_h1_each(prefix+"_eta",        160,-4,4);
   book_h1_each(prefix+"_phi",        128,-3.2,3.2);
   book_h1_each(prefix+"_chi2",       210,-0.5,20.5);
   book_h1_each(prefix+"_chi2_ndof",  220,-0.5,10.5);
   book_h1_each(prefix+"_n_hits_inner",  11,0,11);
   book_h1_each(prefix+"_n_hits_pix", 11,0,11);
   book_h1_each(prefix+"_n_hits_sct", 11,0,11);
   book_h1_each(prefix+"_is_fail",    2,-0.5,1.5);
   book_h1_each(prefix+"_iso3_dr01", 110,-0.5,10.5);
   book_h1_each(prefix+"_iso3_dr02", 110,-0.5,10.5);
   book_h1_each(prefix+"_category",4,0.5,4.5);
   m_map_h1[prefix+"_category"]->GetXaxis()->SetBinLabel(1,"pix4l_sct0");
   m_map_h1[prefix+"_category"]->GetXaxis()->SetBinLabel(2,"pix4l_sct1p");
   m_map_h1[prefix+"_category"]->GetXaxis()->SetBinLabel(3,"pix3l_sct0");
   m_map_h1[prefix+"_category"]->GetXaxis()->SetBinLabel(4,"pix3l_sct1p");
   m_map_h1[prefix+"_category"]->GetXaxis()->SetLabelSize(0.06);
   std::vector<std::string> vpre;
   vpre.push_back("_pix4l");
   vpre.push_back("_pix3l");
   for(unsigned int j=0; j<vpre.size(); j++) {
      std::string pre = vpre[j];
      std::vector<std::string> ppfix;
      ppfix.push_back("_sct0");
      ppfix.push_back("_sct1p");
      for(unsigned int k=0; k<ppfix.size(); k++) {
	 book_h1_each(prefix+pre+ppfix[k]+"_pt",         200,0,100);
         book_h1_each(prefix+pre+ppfix[k]+"_refit_pt",         200,0,100);
	 book_h1_each(prefix+pre+ppfix[k]+"_eta",        160,-4,4);
	 book_h1_each(prefix+pre+ppfix[k]+"_phi",        128,-3.2,3.2);
	 book_h1_each(prefix+pre+ppfix[k]+"_d0_wrtVtx",  100,-1,1);
	 book_h1_each(prefix+pre+ppfix[k]+"_z0_wrtVtx",  200,-10,10);
         book_h1_each(prefix+pre+ppfix[k]+"_refit_d0_wrtVtx",  100,-1,1);
         book_h1_each(prefix+pre+ppfix[k]+"_refit_z0_wrtVtx",  200,-10,10);
         book_h1_each(prefix+pre+ppfix[k]+"_chi2_ndof",  220,-0.5,10.5);
         book_h1_each(prefix+pre+ppfix[k]+"_n_hits_inner",  11,0,11);
	 book_h1_each(prefix+pre+ppfix[k]+"_n_hits_pix", 11,0,11);
	 book_h1_each(prefix+pre+ppfix[k]+"_n_hits_sct", 11,0,11);
         book_h1_each(prefix+pre+ppfix[k]+"_is_fail",             2,-0.5,1.5);
         book_h1_each(prefix+pre+ppfix[k]+"_iso3_dr01",             110,-0.5,10.5);
         book_h1_each(prefix+pre+ppfix[k]+"_iso3_dr02",             110,-0.5,10.5);
      }
   }   
   return StatusCode::SUCCESS;
}   
//---------
//---------
StatusCode UTTAnalysisHistMgr::getTruthChargino(const xAOD::TruthEventContainer* xTruthEventContainer,
					  std::vector<TVector3>& v_chg_p3, std::vector<TVector3>& v_chg_prod, std::vector<TVector3>& v_chg_decay)
{
   const std::string fname = "getTruthChargino> ";

   v_chg_p3.clear(); v_chg_prod.clear(); v_chg_decay.clear(); 
   
   xAOD::TruthEventContainer::const_iterator itr;

   for (itr = xTruthEventContainer->begin(); itr!=xTruthEventContainer->end(); ++itr) {
      int nPart = (*itr)->truthParticleLinks().size();
      for(int iPart=0; iPart<nPart; iPart++) {
	 const xAOD::TruthParticle* particle = (*itr)->truthParticle(iPart);
	 if( particle == 0 ) continue;
	 int pdgId = particle->pdgId();
	 if( abs(pdgId) == 1000024 ) {
	    bool hasDecayVtx = particle->hasDecayVtx();
	    bool hasProdVtx = particle->hasProdVtx();
	    if( hasDecayVtx && hasProdVtx ) {
	       const xAOD::TruthVertex* prodVtx  = particle->prodVtx();
	       const xAOD::TruthVertex* decayVtx = particle->decayVtx();
	       float decayX = decayVtx->x(); float decayY = decayVtx->y(); float decayZ = decayVtx->z();
	       float prodX  = prodVtx->x();  float prodY  = prodVtx->y();  float prodZ = prodVtx->z();
	       float px = particle->px() / 1000.;
	       float py = particle->py() / 1000.;
	       float pz = particle->pz() / 1000.;
	       TVector3 chg_p3(px,py,pz);
	       if( fabs(prodX-decayX)>0.01 && fabs(prodY-decayY)>0.01 && fabs(prodZ-decayZ)>0.01 ) {
		  
		  std::cout << fname << "++ decayed: iPart / pdgId / hasProdVtx / hasDecayVtx = " << iPart << " / " << pdgId << " / " << hasProdVtx << " / " << hasDecayVtx << std::endl;
		  std::cout << fname << "... pt / eta / phi = " << chg_p3.Pt() << " / " << chg_p3.Eta() << " / " << chg_p3.Phi() << std::endl;
		     
		  v_chg_p3.push_back(chg_p3);
		  TVector3 chg_prod(prodX,prodY,prodZ);
		  TVector3 chg_decay(decayX,decayY,decayZ);
		  v_chg_prod.push_back(chg_prod); v_chg_decay.push_back(chg_decay);
		  for(unsigned int i_parent=0; i_parent < particle->nParents(); i_parent++) {
		     const xAOD::TruthParticle* parent = particle->parent(i_parent);
		     if( parent != 0 ) {
			float parent_px = parent->px() / 1000.;
			float parent_py = parent->py() / 1000.;
			float parent_pz = parent->pz() / 1000.;
			TVector3 parent_p3(parent_px,parent_py,parent_pz);
			std::cout << fname << "... parent pt / eta / phi = " << parent_p3.Pt() << " / " << parent_p3.Eta() << " / " << parent_p3.Phi() << std::endl;
		     }
		  }
		  if( v_chg_p3.size() >= 2 ) break;
	       }
	    }
	 }
      }
   }

   //
   return StatusCode::SUCCESS;
}

//--------
//--------

StatusCode UTTAnalysisHistMgr::book_chargino_info_base()
{
   std::string prefix = "chg";
   int nbins;
   double xmin, xmax;
   
   book_h1_each(prefix+"_n_all",10,0,10);
   book_h1_each(prefix+"_n",    10,0,10);
   book_h1_each(prefix+"_decay_r_all",200,0,400);
   book_h1_each(prefix+"_pt_all", 408,-1,101);
   book_h1_each(prefix+"_eta_all",160,-4,4);
   book_h1_each(prefix+"_decay_r",200,0,400);
   //book_h1_each(prefix+"_decay_x",200,-200,200);
   //book_h1_each(prefix+"_decay_y",200,-200,200);
   //book_h1_each(prefix+"_decay_z",300,-900,900);
   
   
   book_h1_each(prefix+"_pt", 408,-1,101);
   book_h1_each(prefix+"_eta",160,-4,4);
   book_h1_each(prefix+"_phi",128,-3.2,3.2);
   
   nbins=30; xmin=0.0; xmax=600.0;
   book_h1_each(prefix+"_twosyst_pt",nbins,xmin,xmax);
/*
   book_h1_each(prefix+"_twosyst_pt_L1XE35",nbins,xmin,xmax);
   book_h1_each(prefix+"_twosyst_pt_L1XE50",nbins,xmin,xmax);
   book_h1_each(prefix+"_twosyst_pt_xe110",nbins,xmin,xmax);
   book_h1_each(prefix+"_twosyst_pt_L1XE35bdt",nbins,xmin,xmax);
   book_h1_each(prefix+"_twosyst_pt_xe110_or_L1XE35bdt",nbins,xmin,xmax);
   book_h1_each(prefix+"_twosyst_pt_xe110_or_xe80bdt",nbins,xmin,xmax);
   book_h1_each(prefix+"_twosyst_pt_xe110_or_xe60bdt",nbins,xmin,xmax);
*/
   
   return StatusCode::SUCCESS;
}
//----------
//----------
/*
void UTTAnalysisHistMgr::get_distrk_trk(const std::string& prefix, const xAOD::TrigComposite* itMyData, TVector3& trk_p3, float& trk_d0, float& trk_d0err, float& trk_z0,
				  float& trk_chi2, float& trk_ndof, int& trk_n_hits_bl, int& trk_n_hits_pix, int& trk_n_hits_sct)
{
   get_distrk_p3(prefix,itMyData,trk_p3);
   (itMyData)->getDetail(prefix+"_d0",        trk_d0);
   //(itMyData)->getDetail(prefix+"_d0err",     trk_d0err);
   (itMyData)->getDetail(prefix+"_z0",        trk_z0);
   //(itMyData)->getDetail(prefix+"_chi2",      trk_chi2);
   //(itMyData)->getDetail(prefix+"_ndof",      trk_ndof);
   (itMyData)->getDetail(prefix+"_n_hits_bl", trk_n_hits_bl);
   (itMyData)->getDetail(prefix+"_n_hits_pix",trk_n_hits_pix);
   (itMyData)->getDetail(prefix+"_n_hits_sct",trk_n_hits_sct);
}

//----------
//----------
void UTTAnalysisHistMgr::get_distrk(const std::string& prefix, const xAOD::TrigComposite* itMyData,std::map<std::string, float>& varf, std::map<std::string, int>& vari)
{
   TVector3 p3;
   float d0, d0err, z0, chi2, ndof;
   int  n_hits_bl, n_hits_pix, n_hits_sct;
   get_distrk_trk(prefix,itMyData,p3,d0,d0err,z0,chi2,ndof,n_hits_bl,n_hits_pix,n_hits_sct);
   varf.insert(std::make_pair("pt",   p3.Pt()));
   varf.insert(std::make_pair("eta",  p3.Eta()));
   varf.insert(std::make_pair("phi",  p3.Phi()));
   varf.insert(std::make_pair("d0",   d0));
   varf.insert(std::make_pair("d0err",d0err));
   varf.insert(std::make_pair("z0",   z0));
   varf.insert(std::make_pair("chi2", chi2));
   vari.insert(std::make_pair("ndof", ndof));
   vari.insert(std::make_pair("n_hits_bl", n_hits_bl));
   vari.insert(std::make_pair("n_hits_pix",n_hits_pix));
   vari.insert(std::make_pair("n_hits_sct",n_hits_sct));
   
}
*/
//---------
//---------
void UTTAnalysisHistMgr::get_distrk_p3(const std::string& prefix, const xAOD::TrigComposite* itMyData, TVector3& trk_p3)
{
   float trk_pt=0; float trk_eta=0; float trk_phi=0;
   (itMyData)->getDetail(prefix+"_pt",    trk_pt);
   (itMyData)->getDetail(prefix+"_eta", trk_eta);
   (itMyData)->getDetail(prefix+"_phi",   trk_phi);
   trk_pt /= 1000.;
   trk_p3.SetPtEtaPhi(trk_pt,trk_eta,trk_phi);//SetPtThetaPhi
   std::cout << " pt = "<< trk_pt << "  ->"<< " eta = "<< trk_eta << "  ->"  << " phi = "<< trk_phi << "  ->" << "get_distrk_p3 finish!\n";  
}
//--------------
//--------------
void UTTAnalysisHistMgr::calc_dr_min(const TVector3& p3, const std::vector<TVector3>& v_ref_p3, float& dr_min, int& idx_min) 
{
   dr_min=9999; idx_min=-1;
   std::cout << " truth p3 size = "<< v_ref_p3.size() << "  ->" << "calc_dr_min start!\n";
   for(unsigned int i=0; i<v_ref_p3.size(); i++) {
      TVector3 ref_p3 = v_ref_p3[i];
      float dr = p3.DrEtaPhi(ref_p3);
      std::cout << " dr = "<< dr << "  ->"  << "calc_dr_min in the middle!\n";
      if( dr<dr_min ) {
	 dr_min  = dr;
	 idx_min = i; 
      }
   }
   std::cout << " dr_to_chg = "<< dr_min << "  ->" << "calc_dr_min finish!\n";
}

//---------
//---------
bool UTTAnalysisHistMgr::is_distrk_match_to_chg(const std::string& prefix, const xAOD::TrigComposite* itMyData,
					  const std::vector<TVector3>& v_chg_p3, float& dr_to_chg, int& idx_of_chg)
{
   dr_to_chg = 9999; idx_of_chg = 9999;
   //std::cout << " dr_to_chg = "<< dr_to_chg << "  ->" << "match_to_chg judgement start!\n"; 
   TVector3 trk_p3;
   get_distrk_p3(prefix,itMyData,trk_p3);
   calc_dr_min(trk_p3, v_chg_p3, dr_to_chg, idx_of_chg);
   //fill_h1_each("dr_to_chg",dr_to_chg);
   std::cout << " dr_to_chg = "<< dr_to_chg << "  ->" << "match_to_chg judgement finish!\n";
   return (dr_to_chg < DR_TO_CHG_CUT_VALUE);
}
//--------
//--------
bool UTTAnalysisHistMgr::is_target_chg(float chg_decay_r, const TVector3& chg_p3)
{
   if( chg_decay_r < CHG_DECAY_R_MIN_VALUE || chg_decay_r > CHG_DECAY_R_MAX_VALUE ) return false;
   if( chg_p3.Pt() < CHG_PT_CUT_VALUE ) return false;
   if( abs(chg_p3.Eta()) > CHG_ETA_CUT_VALUE ) return false;

   // all ok
   return true;
}
//-----------
//-----------

StatusCode UTTAnalysisHistMgr::book_distrk_n_info(const std::string& prefix)
{
   std::string match = "_match_to_chg";

   book_h1_each(prefix+"_n",             21,0,21);
   book_h1_each(prefix+"_n_presel",      21,0,21);
   book_h1_each(prefix+"_n_bdt",         21,0,21);
   book_h1_each(prefix+"_n"+match,       21,0,21);
   book_h1_each(prefix+"_n_presel"+match,21,0,21);
   book_h1_each(prefix+"_n_bdt"+match,   21,0,21);

   book_h1_each(prefix+"_n_vs_selection",4,0.5,4.5);
   m_map_h1[prefix+"_n_vs_selection"]->GetXaxis()->SetBinLabel(1,"N events");
   m_map_h1[prefix+"_n_vs_selection"]->GetXaxis()->SetBinLabel(2,"Cleaning");
   m_map_h1[prefix+"_n_vs_selection"]->GetXaxis()->SetBinLabel(3,"Presel");
   m_map_h1[prefix+"_n_vs_selection"]->GetXaxis()->SetBinLabel(4,"BDT");

   book_h1_each(prefix+"_n1_vs_selection",4,0.5,4.5);
   m_map_h1[prefix+"_n1_vs_selection"]->GetXaxis()->SetBinLabel(1,"N events");
   m_map_h1[prefix+"_n1_vs_selection"]->GetXaxis()->SetBinLabel(2,"Cleaning");
   m_map_h1[prefix+"_n1_vs_selection"]->GetXaxis()->SetBinLabel(3,"Presel");
   m_map_h1[prefix+"_n1_vs_selection"]->GetXaxis()->SetBinLabel(4,"BDT");

   book_h1_each(prefix+"_n"+match+"_vs_selection",4,0.5,4.5);
   m_map_h1[prefix+"_n"+match+"_vs_selection"]->GetXaxis()->SetBinLabel(1,"N events");
   m_map_h1[prefix+"_n"+match+"_vs_selection"]->GetXaxis()->SetBinLabel(2,"Cleaning");
   m_map_h1[prefix+"_n"+match+"_vs_selection"]->GetXaxis()->SetBinLabel(3,"Presel");
   m_map_h1[prefix+"_n"+match+"_vs_selection"]->GetXaxis()->SetBinLabel(4,"BDT");

   book_h1_each(prefix+"_n1"+match+"_vs_selection",4,0.5,4.5);
   m_map_h1[prefix+"_n1"+match+"_vs_selection"]->GetXaxis()->SetBinLabel(1,"N events");
   m_map_h1[prefix+"_n1"+match+"_vs_selection"]->GetXaxis()->SetBinLabel(2,"Cleaning");
   m_map_h1[prefix+"_n1"+match+"_vs_selection"]->GetXaxis()->SetBinLabel(3,"Presel");
   m_map_h1[prefix+"_n1"+match+"_vs_selection"]->GetXaxis()->SetBinLabel(4,"BDT");

   return StatusCode::SUCCESS;
}

// ------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------
//
//
